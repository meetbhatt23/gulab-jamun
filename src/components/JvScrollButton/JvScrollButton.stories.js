import React from "react";
import { JvScrollButton } from "./index";

export default {
    title: "JvScrollButton"
};

export const Default = () => {
    const [state, setState] = React.useState("scroll");

    return (
        <>
            <button
                onClick={() =>
                    state === "scroll" ? setState("trivia") : setState("scroll")
                }
            >
                toggle
            </button>
            <JvScrollButton state={state} />
        </>
    );
};
