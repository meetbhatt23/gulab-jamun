import styled from "@emotion/styled";
import { pxToRem } from "../../utils/px-to-rem";
import { IGridColumn } from "./types";

export const PAGE_GRID_GAP = 24;

export const StyledGridRow = styled.div(({}) => ({
    display: "grid",
    gridGap: pxToRem(PAGE_GRID_GAP),
    gridTemplateColumns: "repeat(12, 1fr)"
}));

export const StyledGridColumn = styled.div<IGridColumn>(({ theme, span, xs, sm, md, lg, xl }) => ({
    ...(xs && {
        [theme.breakpoints.xs]: {
            gridColumn: `auto / span ${xs}`
        }
    }),
    ...(sm && {
        [theme.breakpoints.sm]: {
            gridColumn: `auto / span ${sm}`
        }
    }),
    ...(md && {
        [theme.breakpoints.md]: {
            gridColumn: `auto / span ${md}`
        }
    }),
    ...(lg && {
        [theme.breakpoints.lg]: {
            gridColumn: `auto / span ${lg}`
        }
    }),
    ...(xl && {
        [theme.breakpoints.xl]: {
            gridColumn: `auto / span ${xl}`
        }
    }),
    gridColumn: `auto / span ${span}`
}));
