import {StyledGridColumn, StyledGridRow} from "./styled";

export const GridRow = StyledGridRow;
export const GridColumn = StyledGridColumn;
