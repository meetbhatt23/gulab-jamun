import React from "react";
import {BigBang as BigBangComp} from "./index";
// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
    title: "BigBang",
    parameters: {
        layout: "fullscreen"
    }
    // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
};


export const BigBang = () => <BigBangComp />;

