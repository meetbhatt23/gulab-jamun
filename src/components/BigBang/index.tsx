import { useLottieScrollTrigger } from "../../utils/lottie-scroll";
import React from "react";
import styled from "@emotion/styled";
import { Text } from "../Text";

export const BigBang = () => {
    const { loaded } = useLottieScrollTrigger({
        target: "#animationWindow",
        path: "/mshk-image-to-lottie.json",
        speed: "medium",
        renderer: "svg",
        scrub: 1 // seconds it takes for the playhead to "catch up"
    });
    return <div id="animationWindow" css={{
        width: "100vw",
        height: "100vh",
        position: "relative",
        color: "white",
        ".jpg": {
            width: "100vw",
            height: "100vh",
        },
        "> div": {
            width: "100vw",
            height: "100vh",
        },
        ".loading-text": {
            position: "absolute",
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
        }
    }}>
        {!loaded && <div className="loading-text"><Text variant="caption">Loading..</Text></div>}
    </div>;
};
