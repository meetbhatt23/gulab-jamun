import React from "react";
import { FITB } from "./index";
// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
    title: "FunFacts/FillInTheBlanks"
    // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
};

export const FillInTheBlanks = () => (
    <>
        <FITB onComplete={() => console.log("completed")} />
    </>
);
