import React from "react";
import { CreditAnimations } from "./index";
import { useWindowSize } from "usehooks-ts";

export default {
    title: "CreditAnimations",
    parameters: {
        layout: "fullscreen"
    }
};

export const Default = () => {
    const { width } = useWindowSize();
    return (
        <>
            <CreditAnimations marqueeItemWidth={width} />
        </>
    );
};
