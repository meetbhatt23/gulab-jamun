import React from "react";
import { FfMapItem } from "./index";
import { FF_MAP_ITEMS } from "../data";

export default {
    title: "FunFacts/MapItem"
};

export const Default = () => (
    <>
        {FF_MAP_ITEMS.map((item) => (
            <FfMapItem {...item} />
        ))}
    </>
);
