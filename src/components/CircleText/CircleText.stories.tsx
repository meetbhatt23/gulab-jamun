import React, { useMemo } from "react";
import { CircleText } from "./index";
import { ILetters } from "../PilowlavaText";
import { SpaceChar } from "../PilowlavaText/helpers";

const LETTERS = [
    "0-G-gulabjamun.svg",
    "1-u-gulabjamun.svg",
    "2-l-gulabjamun.svg",
    "3-a-gulabjamun.svg",
    "4-b-gulabjamun.svg",
    "break",
    "6-J-gulabjamun.svg",
    "7-a-gulabjamun.svg",
    "8-m-gulabjamun.svg",
    "9-u-gulabjamun.svg",
    "10-n-gulabjamun.svg"
];

const WHITE_LETTERS = [
    "0-G-gulabjamun-white.svg",
    "1-u-gulabjamun-white.svg",
    "2-l-gulabjamun-white.svg",
    "3-a-gulabjamun-white.svg",
    "4-b-gulabjamun-white.svg",
    "break",
    "6-J-gulabjamun-white.svg",
    "7-a-gulabjamun-white.svg",
    "8-m-gulabjamun-white.svg",
    "9-u-gulabjamun-white.svg",
    "10-n-gulabjamun-white.svg"
];

export default {
    title: "CircleText"
};

export const Default = () => {
    const letters = useMemo<ILetters>(
        () =>
            LETTERS.map((url) => {
                let retVal;
                if (url === "space") {
                    retVal = {
                        el: <SpaceChar />,
                        type: "space"
                    };
                } else if (url === "break") {
                    retVal = {
                        el: <SpaceChar />,
                        type: "break"
                    };
                } else {
                    retVal = {
                        el: <img src={`/images/letters/${url}`} />,
                        type: "letter"
                    };
                }
                return retVal;
            }),
        []
    );

    const whiteLetters = useMemo<ILetters>(
        () =>
            WHITE_LETTERS.map((url) => {
                let retVal;
                if (url === "space") {
                    retVal = {
                        el: <SpaceChar />,
                        type: "space"
                    };
                } else if (url === "break") {
                    retVal = {
                        el: <SpaceChar />,
                        type: "break"
                    };
                } else {
                    retVal = {
                        el: <img src={`/images/letters/${url}`} />,
                        type: "letter"
                    };
                }
                return retVal;
            }),
        []
    );
    return (
        <>
            <CircleText letters={letters} whiteLetters={whiteLetters} />
        </>
    );
};
