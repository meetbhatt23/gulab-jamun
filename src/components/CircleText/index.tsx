import React, { useCallback, useLayoutEffect, useRef } from "react";
import { gsap } from "gsap";
import { pxToRem } from "../../utils/px-to-rem";
import { lowerFirst, shuffle, split } from "lodash";
import { MotionPathPlugin } from "gsap/dist/MotionPathPlugin";
import { Flip } from "gsap/dist/Flip";
import { EncNavPath } from "../../assets/misc-svgs/components";

type ILetters = {
    el: React.ReactElement;
    type: "letter" | "space" | "break";
}[];
interface ICircleText {
    letters: ILetters;
    whiteLetters: ILetters;
}

export const CircleText = ({ letters, whiteLetters }: ICircleText) => {
    const mainRef = useRef(null);
    const initials =
        useRef<
            { el: HTMLDivElement; x: number | string; y: number | string }[]
        >(null);
    const lastRecordedVal =
        useRef<
            { el: HTMLDivElement; x: number | string; y: number | string }[]
        >(null);
    const tween = useRef<GSAPTween>(null);
    const mainTl = useRef<GSAPTimeline>(null);
    const flipAnim = useRef<GSAPTimeline>(null);

    const splitArrayShuffled = useRef<HTMLDivElement[]>([]);
    const splitArray = useRef<HTMLDivElement[]>([]);
    const isTlReversed = useRef(false);

    useLayoutEffect(() => {
        const ctx = gsap.context(() => {
            gsap.registerPlugin(MotionPathPlugin, Flip);
            splitArray.current = gsap.utils.toArray<HTMLDivElement>(
                ".letter-type-wrapper"
            );
            initials.current = splitArray.current.map((el) => {
                return {
                    el: el,
                    x: gsap.getProperty(el, "offsetLeft"),
                    y: gsap.getProperty(el, "offsetTop")
                };
            });
            splitArrayShuffled.current = shuffle(splitArray.current);

            const TOTAL_DURATION = 40;

            tween.current = gsap.to(splitArrayShuffled.current, {
                paused: true,
                // reversed: direction === "anticlockwise",
                // immediateRender: true,
                motionPath: {
                    path: "svg path",
                    align: "svg path",
                    alignOrigin: [0.5, 0.5]
                    // autoRotate: true
                },
                stagger: {
                    each: TOTAL_DURATION / splitArrayShuffled.current.length,
                    repeat: -1,
                    ease: "none"
                },
                // transformOrigin: "50% 50%",
                duration: TOTAL_DURATION,
                ease: "none"
            });
            tween.current.seek(TOTAL_DURATION);

            const initialState = Flip.getState(".letter-type-wrapper");

            gsap.set(".letter-type-wrapper", {
                clearProps: "all"
            });

            flipAnim.current = Flip.from(initialState, {
                paused: true,
                ease: "power1.inOut",
            });

            mainTl.current = gsap
                .timeline({
                    paused: true
                })
                .add(flipAnim.current.play());
        }, mainRef);

        return () => ctx.revert();
    }, []);

    const lineBreakIndex = letters.findIndex(
        (letter) => letter.type === "break"
    );

    let letterLines, whiteLetterLines;

    if (lineBreakIndex > -1) {
        letterLines = [
            letters.slice(0, lineBreakIndex),
            letters.slice(lineBreakIndex + 1, letters.length)
        ];
        whiteLetterLines = [
            whiteLetters.slice(0, lineBreakIndex),
            whiteLetters.slice(lineBreakIndex + 1, whiteLetters.length)
        ];
    } else {
        letterLines = [letters];
        whiteLetterLines = [whiteLetters];
    }

    const onClick = useCallback(() => {
        if (mainTl.current.time() !== mainTl.current.totalDuration()) {
            mainTl.current.play();
        } else {
            mainTl.current.reverse().then(() => {});
        }
    }, [mainTl.current]);

    return (
        <div ref={mainRef} css={{
            width: "500px",
            height: "500px",
            position: "relative",
        }}>
            <button css={{
                position: "absolute",
                top: -100,
                left: -100,
            }} onClick={onClick}>toggle</button>
            <EncNavPath
                css={{
                    position: "absolute",
                    top: 0,
                    left: 0,
                    right: 0,
                    bottom: 0,
                    width: "100%"
                }}
            />
            {letterLines.map((line, lineIndex) => (
                <div className="line-container" css={{}}>
                    <div
                        css={{
                            display: "inline-grid",
                            gridAutoFlow: "column",
                            gridGap: pxToRem(6),
                            svg: {
                                height: pxToRem(60)
                            },
                            img: {
                                height: pxToRem(60)
                            },
                            ".space-letter": {
                                width: pxToRem(20)
                            },
                            ".letter-wrapper": {
                                position: "relative"
                                // opacity: 0
                            },
                            ".white-letter": {
                                opacity: 0
                            },
                            ".colored-letter": {
                                opacity: 1,
                                position: "absolute",
                                left: 0,
                                right: 0,
                                bottom: 0,
                                top: 0
                            }
                        }}
                        className={`line line-${lineIndex}`}
                    >
                        <>
                            {line.map((letter, index) => {
                                return (
                                    <div
                                        css={{
                                            willChange: "transform"
                                        }}
                                        className={`letter-wrapper letter-${
                                            index + 1
                                        } ${letter.type}-type-wrapper`}
                                    >
                                        <div
                                            css={{
                                                willChange: "transform"
                                            }}
                                            className="colored-letter"
                                        >
                                            {letter.el}
                                        </div>
                                        <div
                                            css={{
                                                willChange: "transform"
                                            }}
                                            className="white-letter"
                                        >
                                            {
                                                whiteLetterLines[lineIndex][
                                                    index
                                                ].el
                                            }
                                        </div>
                                    </div>
                                );
                            })}
                        </>
                    </div>
                </div>
            ))}
        </div>
    );
};
