import React, { useLayoutEffect, useRef } from "react";
import { gsap } from "gsap";
import { Text } from "../Text";
import { pxToRem } from "../../utils/px-to-rem";

interface ILetter {
}

export const Letter = ({}: ILetter) => {
    const mainRef = useRef(null);

    let mainTl = null;
    useLayoutEffect(() => {
        const ctx = gsap.context(() => {
            mainTl = gsap.timeline({});
        }, mainRef);

        return () => ctx.revert();
    }, []);

    return <div ref={mainRef} css={{
    }}>
        <div css={{
            color: "transparent",
            fontFamily: "Pilowlava",
            fontSize: pxToRem(40),
            backgroundClip: "text",
            "-webkit-background-clip": "text",
            textFillColor: "transparent",
            "-webkit-text-fill-color": "transparent",
            // boxShadow: [
            //     "0px -2px 10px 0px #05D7D7 inset",
            //     "3px 10px 9px 0px #911229 inset",
            //     "-4px 0px 5px 0px #FF8B38 inset"
            // ].join(", ")
        }}>G</div>
    </div>;
};

