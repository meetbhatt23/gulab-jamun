import React, { useMemo } from "react";
import { ILetters, IPilowlavaText, PilowlavaText } from "./index";

const LETTERS = [
    "/images/letters/l.png",
    "/images/letters/e.png",
    "/images/letters/d.png",
    "/images/letters/i.png",
    "/images/letters/k.png",
    "/images/letters/e.png",
    "/images/letters/n.png",
    "/images/letters/i.png"
];

const WHITE_LETTERS = [
    "/images/letters/white-l.png",
    "/images/letters/white-e.png",
    "/images/letters/white-d.png",
    "/images/letters/white-i.png",
    "/images/letters/white-k.png",
    "/images/letters/white-e.png",
    "/images/letters/white-n.png",
    "/images/letters/white-i.png"
];

export const LedikeniText = ({
                               ...props
                           }: Omit<IPilowlavaText, "name" | "letters" | "whiteLetters">) => {
    const letters = useMemo<ILetters>(
        () =>
            LETTERS.map((url) => ({
                el: <img src={`${url}`} />,
                type: "letter"
            })),
        []
    );

    const whiteLetters = useMemo<ILetters>(
        () =>
            WHITE_LETTERS.map((url) => ({
                el: <img src={`${url}`} />,
                type: "letter"
            })),
        []
    );
    return (
        <PilowlavaText
            name="ledikeni"
            letters={letters}
            whiteLetters={whiteLetters}
            {...props}
        />
    );
};
