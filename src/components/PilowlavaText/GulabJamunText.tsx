import React, { useMemo } from "react";
import { ILetters, IPilowlavaText, PilowlavaText } from "./index";
import { SpaceChar } from "./helpers";

const LETTERS = [
    "/images/letters/g.png",
    "/images/letters/u.png",
    "/images/letters/l.png",

    "/images/letters/a.png",

    "/images/letters/b.png",

    "break",
    "/images/letters/j.png",

    "/images/letters/a.png",

    "/images/letters/m.png",

    "/images/letters/u.png",

    "/images/letters/n.png"
];

const WHITE_LETTERS = [
    "/images/letters/white-g.png",
    "/images/letters/white-u.png",
    "/images/letters/white-l.png",

    "/images/letters/white-a.png",

    "/images/letters/white-b.png",

    "break",
    "/images/letters/white-j.png",

    "/images/letters/white-a.png",

    "/images/letters/white-m.png",

    "/images/letters/white-u.png",

    "/images/letters/white-n.png"
];

export const GulabJamunText = ({
    ...props
}: Omit<IPilowlavaText, "name" | "letters" | "whiteLetters">) => {
    const letters = useMemo<ILetters>(
        () =>
            LETTERS.map((url) => {
                let retVal;
                if (url === "space") {
                    retVal = {
                        el: <SpaceChar />,
                        type: "space"
                    };
                } else if (url === "break") {
                    retVal = {
                        el: <SpaceChar />,
                        type: "break"
                    };
                } else {
                    retVal = {
                        el: <img src={`${url}`} />,
                        type: "letter"
                    };
                }
                return retVal;
            }),
        []
    );

    const whiteLetters = useMemo<ILetters>(
        () =>
            WHITE_LETTERS.map((url) => {
                let retVal;
                if (url === "space") {
                    retVal = {
                        el: <SpaceChar />,
                        type: "space"
                    };
                } else if (url === "break") {
                    retVal = {
                        el: <SpaceChar />,
                        type: "break"
                    };
                } else {
                    retVal = {
                        el: <img src={`${url}`} />,
                        type: "letter"
                    };
                }
                return retVal;
            }),
        []
    );
    return (
        <PilowlavaText
            name="gulabjamun"
            letters={letters}
            whiteLetters={whiteLetters}
            {...props}
        />
    );
};
