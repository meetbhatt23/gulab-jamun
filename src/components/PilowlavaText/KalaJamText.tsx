import React, { useMemo } from "react";
import { ILetters, IPilowlavaText, PilowlavaText } from "./index";

const LETTERS = [
    "/images/letters/k.png",
    "/images/letters/a.png",
    "/images/letters/l.png",
    "/images/letters/a.png",
    "/images/letters/j.png",
    "/images/letters/a.png",
    "/images/letters/m.png"
];

const WHITE_LETTERS = [
    "/images/letters/white-k.png",
    "/images/letters/white-a.png",
    "/images/letters/white-l.png",
    "/images/letters/white-a.png",
    "/images/letters/white-j.png",
    "/images/letters/white-a.png",
    "/images/letters/white-m.png"
];

export const KalaJamText = ({
                               ...props
                           }: Omit<IPilowlavaText, "name" | "letters" | "whiteLetters">) => {
    const letters = useMemo<ILetters>(
        () =>
            LETTERS.map((url) => ({
                el: <img src={`${url}`} />,
                type: "letter"
            })),
        []
    );

    const whiteLetters = useMemo<ILetters>(
        () =>
            WHITE_LETTERS.map((url) => ({
                el: <img src={`${url}`} />,
                type: "letter"
            })),
        []
    );
    return (
        <PilowlavaText
            name="kalajam"
            letters={letters}
            whiteLetters={whiteLetters}
            {...props}
        />
    );
};
