import React from "react";
import { PantuaText } from "./PantuaText";
import { JhurreKaRasgullaText } from "./JhurraText";
import { KalaJamText } from "./KalaJamText";
import { LedikeniText } from "./LedikeniText";
import { GulabJamunText } from "./GulabJamunText";
import { TextWithHotspot } from "./index";
import { RasgullaText } from "./RasgullaText";
// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
    title: "GJText"
    // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
};

const CONTENT = [
    {
        body1:
            "Ledikeni (Bengali: লেডিকেনি) or Lady Kenny is a popular\n" +
            "                        Indian sweet originated in West Bengal, India and also\n" +
            "                        consumed in Bangladesh.",
        body2: "It is a light fried reddish-brown sweet ball made of Chhena and flour, soaked in sugar syrup. "
    }
];

export const Pantua = () => {
    return (
        <TextWithHotspot
            top={500}
            left={500}
            hotspotPositionOnly={{
                top: 0.5,
                left: 0.5,
            }}
            children={({
                progress,
                initialHotspotY,
                initialHotspotX
            }) => (
                <PantuaText
                    setActiveTl={() => {}}
                    initialHotspotX={initialHotspotX}
                    initialHotspotY={initialHotspotY}
                    body1={CONTENT[0].body1}
                    body2={CONTENT[0].body2}
                    showProgress={progress}
                />
            )}
        />
    );
};
