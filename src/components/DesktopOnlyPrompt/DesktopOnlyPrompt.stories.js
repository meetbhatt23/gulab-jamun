import React from "react";
import { DesktopOnlyPrompt } from "./index";

export default {
    title: "DesktopOnlyPrompt"
};

export const Default = () => <DesktopOnlyPrompt />;
