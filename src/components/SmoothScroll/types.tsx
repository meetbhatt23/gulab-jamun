export interface ISmoothScroll {
    smooth?: boolean | number;
    enabled?: boolean;
}
