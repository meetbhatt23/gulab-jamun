import React, { useLayoutEffect, useRef } from "react";
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";
import { ScrollSmoother } from "gsap/dist/ScrollSmoother";
import { gsap } from "gsap";
import { ISmoothScroll } from "./types";

export const SmoothScroll = ({
    smooth = 1.5
}: ISmoothScroll) => {
    const ref = useRef<ScrollSmoother>(null);

    useLayoutEffect(() => {
        gsap.registerPlugin(ScrollTrigger, ScrollSmoother);

        // create the scrollSmoother before your scrollTriggers
        ref.current = ScrollSmoother.create({
            smooth, // how long (in seconds) it takes to "catch up" to the native scroll position
            effects: true, // looks for data-speed and data-lag attributes on elements
            smoothTouch: 0.05, // much shorter smoothing time on touch devices (default is NO smoothing on touch devices),'            normalizeScroll: true
            normalizeScroll: true
        });
    }, []);
    // eslint-disable-next-line react/jsx-no-useless-fragment
    return <></>;
};
