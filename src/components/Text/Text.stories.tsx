import React from "react";
import { Text } from "./index";

import { theme } from "../../themes";
import { pxToRem } from "../../utils/px-to-rem";
// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
    title: "Text"
    // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
};

export const Default = () => {
    const typographyKeys = Object.keys(theme.typography);

    return (
        <div
            css={{
                display: "grid",
                gridGap: pxToRem(20)
            }}
        >
            {typographyKeys.map((key) => (
                <Text variant={key} color="white">
                    {key}
                </Text>
            ))}
        </div>
    );
};
