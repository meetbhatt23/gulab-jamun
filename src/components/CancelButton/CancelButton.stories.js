import React from "react";
import { CancelButton } from "./index";
import { NextButton } from "../NextButton";
import { pxToRem } from "../../utils/px-to-rem";

export default {
    title: "CancelButton"
};

export const Default = () => (
    <div
        css={{
            display: "grid",
            gridAutoFlow: "column",
            gridGap: pxToRem(32),
            alignItems: "center"
        }}
    >
        <CancelButton />
    </div>
);
