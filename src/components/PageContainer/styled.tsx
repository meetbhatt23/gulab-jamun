import styled from "@emotion/styled";
import {IPageContainer} from "./types";

import {PAGE_MARGIN} from "../../utils/constants";

export const StyledPageContainer = styled.div<IPageContainer>(({fullWidth}) => ({
    width: fullWidth ? "100%" : `calc(100vw - ${2 * PAGE_MARGIN}px)`,
    margin: `0 ${PAGE_MARGIN}px`,
}));
