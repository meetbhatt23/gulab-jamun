import React from "react";
import { FfTwitterPoll } from "./index";

export default {
    title: "FunFacts/TwitterPoll",
    parameters: {
        layout: "fullscreen"
    }
};

export const Default = () => (
    <>
        <FfTwitterPoll />
    </>
);
