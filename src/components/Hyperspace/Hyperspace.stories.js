import React from "react";
import { Hyperspace } from "./index";

export default {
    title: "FunFacts/Hyperspace",
    parameters: {
        layout: "fullscreen"
    }
};

export const Lines1000 = () => <>
    <Hyperspace count={1000} />
</>;

export const Lines3000 = () => <>
    <Hyperspace count={3000} />
</>;


