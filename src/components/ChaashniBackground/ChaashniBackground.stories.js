import React from "react";
import { ChaashniBackground } from "./index";
import { Shader } from "./Shader";

export default {
    title: "ChaashniBackground"
};

export const Default = () => (
    <>
        <ChaashniBackground />
    </>
);

export const Shader2 = () => (
    <>
        <Shader />
    </>
);
