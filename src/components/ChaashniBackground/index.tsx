import React, { useLayoutEffect, useRef, useState } from "react";
import { gsap } from "gsap";
import { random, sample } from "lodash";
import { StaticImage } from "gatsby-plugin-image";

interface IChaashniBackground {
    zIndex?: number;
}

const sShapedPolygonPoints = {
    p1: [0, 0],
    p2: [55, 0],
    p3: [100, 33],
    p4: [60, 67],
    p5: [100, 100],
    p6: [30, 100],
    p7: [0, 67],
    p8: [45, 33]
};

const cShapedPolygonPoints = {
    p1: [0, 0],
    p2: [50, 0],
    p3: [100, 50],
    p4: [50, 100],
    p5: [0, 100],
    p6: [50, 50]
};

const getRangeForNumber = (n: number) => {
    const range = 15;
    if (n === 0) {
        return random(0, 2 * range, true);
    }
    if (n === 100) {
        return random(100 - 2 * range, 100, true);
    }
    return random(n - range, n + range, true);
};

const getSShapedPolygon = () => {
    return (
        "polygon(" +
        Object.keys(sShapedPolygonPoints)
            .map((key) => {
                const p = sShapedPolygonPoints[key];
                const [x, y] = p;
                return `${getRangeForNumber(x)}% ${y}%`;
            })
            .join(",") +
        ")"
    );
};

const getCShapedPolygon = () => {
    return (
        "polygon(" +
        Object.keys(cShapedPolygonPoints)
            .map((key) => {
                const p = cShapedPolygonPoints[key];
                const [x, y] = p;
                return `${getRangeForNumber(x)}% ${y}%`;
            })
            .join(",") +
        ")"
    );
};

interface IBlob {
    shape?: "c" | "s";
    rotate?: number;
}

const Blob = React.memo(({ shape = "c", rotate = 1, ...props }: IBlob) => {
    const mainRef = useRef(null);
    return (
        <div
            {...props}
            css={{
                filter: `blur(5vw)`,
                width: "30vw"
            }}
            ref={mainRef}
        >
            <div
                className="blob-1"
                css={{
                    width: "100%",
                    // height: "100%",
                    overflow: "hidden",
                    height: 0,
                    paddingTop: "100%",
                    // left: "40.38px",
                    // top: "20.99px",
                    clipPath:
                        shape === "c"
                            ? getCShapedPolygon()
                            : getSShapedPolygon(),
                    position: "relative"

                    // transform: "rotate(-3.88deg)"
                }}
            >
                <div
                    css={{
                        transform: "scale(1.5)",

                        position: "absolute",
                        top: 0,
                        right: 0,
                        left: 0,
                        bottom: 0,
                        background: sample([
                            "linear-gradient(90deg, rgba(255, 139, 56, 0.4) 33.71%, rgba(193, 84, 35, 0.4) 51.54%, rgba(25, 19, 14, 0.4) 65.24%)",
                            "linear-gradient(270deg, rgba(255, 139, 56, 0.4) 10.89%, rgba(193, 84, 35, 0.4) 32.17%, rgba(145, 41, 18, 0.4) 49.2%, rgba(21, 16, 20, 0.4) 69.27%)"
                        ])
                    }}
                />
            </div>
        </div>
    );
});

const polygon1 = getSShapedPolygon();
const polygon2 = getSShapedPolygon();
const polygon3 = getSShapedPolygon();
const polygon4 = getSShapedPolygon();
const polygon5 = getSShapedPolygon();

export const ChaashniBackground = React.memo(
    ({ zIndex }: IChaashniBackground) => {
        const mainRef = useRef(null);
        const [imgLoaded, setImgLoaded] = useState(false);

        let mainTl = null;

        useLayoutEffect(() => {
            const ctx = gsap.context(() => {
                const singleDuration = 4;
                gsap.timeline({
                    repeat: -1,
                    // paused: true,
                    defaults: { duration: singleDuration, ease: "linear" },
                    yoyo: true
                })
                    .to(
                        ".blob-1",
                        {
                            clipPath: polygon1,
                            scale: random(0.9, 1.25, true)
                        },
                        "="
                    )
                    .to(".blob-1", {
                        clipPath: polygon2,
                        scale: random(0.9, 1.25, true)
                    })
                    .to(".blob-1", {
                        clipPath: polygon3,
                        scale: random(0.9, 1.25, true)
                    })
                    .to(".blob-1", {
                        clipPath: polygon4,
                        scale: random(0.9, 1.25, true)
                    })
                    .to(".blob-1", {
                        clipPath: polygon5,
                        scale: random(0.9, 1.25, true)
                    });

                gsap.to(".blob-1", {
                    repeat: -1,
                    // paused: true,
                    yoyo: true,
                    // rotate: `${Math.sign(rotate) * random(180, 360)}deg`,
                    rotate: `${Math.sign(random(1, -1)) * 360}`,
                    duration: singleDuration * 30,
                    ease: "none"
                });
            }, mainRef);

            return () => {
                // document.removeEventListener("mousemove", onMouseMove, false);
                ctx.revert();
            };
        }, [imgLoaded]);
        return (
            <div ref={mainRef} id="chaashni-background" css={{ zIndex }}>
                <div
                    css={{
                        position: "fixed",
                        top: 0,
                        left: 0,
                        right: 0,
                        bottom: 0
                    }}
                >
                    <div
                        css={{
                            position: "absolute",
                            top: 0,
                            left: 0,
                            right: 0,
                            bottom: 0,
                            transform: "scale(1.4)",
                            opacity: 0.7
                        }}
                    >
                        <StaticImage
                            quality={70}
                            onLoad={() => {
                                setImgLoaded(true);
                            }}
                            className="static-image"
                            objectFit="cover"
                            objectPosition="center"
                            css={{
                                width: "100%",
                                height: "100%"
                            }}
                            src="../../images/chaashni-static.jpg"
                            alt=""
                        />
                    </div>
                    {/*<Blob*/}
                    {/*    rotate={-1}*/}
                    {/*    shape="c"*/}
                    {/*    css={{*/}
                    {/*        position: "fixed",*/}
                    {/*        left: "0vw",*/}
                    {/*        bottom: "0vh",*/}
                    {/*        maxHeight: "90vh",*/}
                    {/*        transform: "rotate(180deg)"*/}
                    {/*    }}*/}
                    {/*/>*/}
                    <Blob
                        shape="s"
                        css={{
                            position: "fixed",
                            left: "50vw",
                            top: "50vh",
                            transform: "translate(-50%, -50%)",
                            maxHeight: "70vh"
                        }}
                    />
                </div>
            </div>
        );
    }
);
