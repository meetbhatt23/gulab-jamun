#define MAIN_COLOR vec3(36,14,2) / vec3(255.0) / vec3(1.5)
uniform vec3 iResolution;           // viewport resolution (in pixels)
uniform float iTime;                 // shader playback time (in seconds)
uniform vec2 iMouse;                // mouse pixel coords. xy: current (if MLB down), zw: click

void main() {
    vec2 p = (1.5 * gl_FragCoord.xy - iResolution.xy) / max(iResolution.x, iResolution.y);
    float computedTime = iTime * 0.15;
    for(int i = 1; i < 20; i++) {
        vec2 newp = p;
		//newp.x+=0.6/float(i)*sin(float(i)*p.y+computedTime+0.3*float(i))+1.0;
        newp.x += 0.9 / float(i) * sin(float(i) * p.y + computedTime + 1.0) + 6.0;
		//newp.y+=0.6/float(i)*sin(float(i)*p.x+computedTime+0.3*float(i+10))-1.4;
        newp.y += 0.2 / float(i) * sin(float(i) * p.x + computedTime + 0.3) - 1.4;
        p = newp;
    }

    float edge0 = 0.3;
    float edge1 = 0.6;
    float preStepColor = smoothstep(edge0, edge1, 0.7 - abs(sin(p.x)));
    vec3 col = vec3(preStepColor) * MAIN_COLOR;

    gl_FragColor = vec4(vec3(col.x, col.y, col.z), 1.0);
    // gl_FragColor = vec4(vec3(smoothColorX, smoothColorY, smoothColorZ), 1.0);
    // gl_FragColor = linearToOutputTexel(gl_FragColor);

}