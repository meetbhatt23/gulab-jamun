import React from "react";
import { EncyclopediaNav, EncyclopediaNavItemCircle, NAV_ITEMS } from "./index";
// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
    title: "EncyclopediaNav"
    // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
};

export const FullNav = () => <>
    <EncyclopediaNav
        item1={NAV_ITEMS[0]}
        item2={NAV_ITEMS[1]}
        item3={NAV_ITEMS[2]}
        item4={NAV_ITEMS[3]}
        item5={NAV_ITEMS[4]}
        item6={NAV_ITEMS[5]}
    />
</>;


