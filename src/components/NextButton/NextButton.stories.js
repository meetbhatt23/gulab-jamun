import React from "react";
import { NextButton } from "./index";
import { pxToRem } from "../../utils/px-to-rem";
import { PrevButton } from "../PrevButton";

export default {
    title: "NextButton"
};

export const Default = () => (
    <>
        <div
            css={{
                display: "grid",
                gridAutoFlow: "column",
                gridGap: pxToRem(32),
                alignItems: "center"
            }}
        >
            <PrevButton />
            <NextButton />
        </div>
    </>
);
