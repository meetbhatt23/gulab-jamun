import React from "react";
import { RoundPlanet } from "./3DPlanet";
import { pxToRem } from "../../utils/px-to-rem";
import { AllPlanetsWithJamunverse } from "./AllPlanetsWithJamunverse";
// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
    title: "3DPlanets",
    parameters: {
        layout: "fullscreen"
    }
    // More on argTypes: https://storybook.js.org/docs/react/api/argtypes,
};
export const AllRoundPlanets = () => <AllPlanetsWithJamunverse />;
