import { IRoundPlanetID } from "./3DPlanet";

export type IAnyPlanet = IRoundPlanetID | "ledikeni";

export interface IHotspotPosition {
    left?: number;
    right?: number;
    top?: number;
    bottom?: number;
}

export interface IHotspotMapItem {
    key: IAnyPlanet;
    setter: (IHotspotPosition) => void;
    setterHotspot: (IHotspotPosition) => void;
    relativePosition: "topLeft" | "topRight" | "bottomLeft" | "bottomRight";
}
