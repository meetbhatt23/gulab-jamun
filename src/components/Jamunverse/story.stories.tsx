import React from "react";
import { JamunVerse as JamunVerseComp } from "./JamunVerse";
import { PlanetHotspot as PlanetHotspotComp } from "./PlanetHotspot";

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
    title: "Jamunverse"
    // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
};


export const JamunVerse = () => <JamunVerseComp />;

export const PlanetHotspot = () => <><PlanetHotspotComp /> <PlanetHotspotComp /> <PlanetHotspotComp /></>;
