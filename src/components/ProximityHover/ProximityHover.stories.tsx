import React, { useLayoutEffect, useRef } from "react";
import { ProximityHover } from "./index";
import { Text } from "../Text";
import { gsap } from "gsap";
import { useTheme } from "@emotion/react";
import { rgba } from "polished";

export default {
    title: "ProximityHover",
    parameters: {
        layout: "fullscreen"
    }
};

export const Default = () => {
    const mainRef = useRef(null);
    const theme = useTheme();
    const startingGradient = `radial-gradient(circle at bottom left, ${rgba(
        theme.colors.obNeutral20,
        0
    )} -10%, ${rgba(theme.colors.obNeutral20, 1)} 0%)`;
    const endingGradient = `radial-gradient(circle at bottom left, ${rgba(
        theme.colors.obNeutral20,
        0
    )} 8.5%, ${rgba(theme.colors.obNeutral20, 1)} 100%)`;

    let mainTl = null;
    useLayoutEffect(() => {
        const ctx = gsap.context(() => {
            mainTl = gsap.timeline({ paused: true }).to(".map-gradient", {
                backgroundImage: endingGradient,
                duration: 5,
                ease: "none"
            });
        }, mainRef);

        return () => {
            ctx.revert();
        };
    }, []);
    return (
        <div
            css={{
                position: "fixed",
                top: 0,
                left: 0,
                right: 0,
                bottom: 0,
                backgroundImage: "url(/images/contact-page-map.png)",
                backgroundRepeat: "no-repeat",
                backgroundPosition: "bottom 20% left 20%",
                backgroundSize: "cover"
            }}
            ref={mainRef}
        >
            <div
                css={{
                    position: "absolute",
                    top: "0%",
                    left: "0%",
                    width: "100%",
                    height: "100%",
                    zIndex: 0
                }}
            >
                <div
                    className="map-image"
                    css={{
                        width: "100%",
                        zIndex: 0
                    }}
                />
            </div>
            <div
                className="map-gradient"
                css={{
                    backgroundImage: startingGradient,
                    // transform: "matrix(-1, 0, 0, 1, 0, 0)",
                    position: "absolute",
                    top: 0,
                    left: 0,
                    right: 0,
                    bottom: 0,
                    zIndex: 1
                }}
            />
            <div
                ref={mainRef}
                css={{
                    position: "relative",
                    zIndex: 2,
                    marginTop: "200px",
                    marginLeft: "200px"
                }}
            >
                <ProximityHover
                    onUpdate={(progress) => {
                        if (mainTl) {
                            const toDuration =
                                mainTl.totalDuration() * progress;
                            mainTl.tweenTo(toDuration, {
                                overwrite: true,
                                duration: 0.25
                            });
                        }
                    }}
                >
                    <Text
                        css={{
                            display: "inline-block"
                        }}
                        variant="body"
                        color="obNeutral90"
                    >
                        Click me
                    </Text>
                </ProximityHover>
            </div>
        </div>
    );
};


