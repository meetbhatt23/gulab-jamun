import React from "react";
import { PngAnimatedLetter } from "./index";
import { pxToRem } from "../../utils/px-to-rem";

export default {
    title: "PngAnimatedLetter"
};

const jamunLetters = ["j", "a", "m", "u", "n"];
const triviaLetters = ["t", "r", "i", "v", "i", "a"];

export const DefaultPngAnimatedLetter = () => (
    <>
        <div
            css={{
                display: "grid",
                gridGap: pxToRem(8)
            }}
        >
            <div
                css={{
                    display: "grid",
                    gridAutoFlow: "column",
                    gridGap: pxToRem(8)
                }}
            >
                {jamunLetters.map((letter, index) => (
                    <PngAnimatedLetter
                        duration={1.5}
                        // delay={index * 0.1}
                        height={pxToRem(60)}
                        image1Src={`/images/jamun-trivia-letters/jamun/${letter}.png`}
                        image2Src={`/images/jamun-trivia-letters/jamun/${letter}-blue.png`}
                    />
                ))}
            </div>
            <div
                css={{
                    display: "grid",
                    gridAutoFlow: "column",
                    gridGap: pxToRem(8)
                }}
            >
                {triviaLetters.map((letter, index) => (
                    <PngAnimatedLetter
                        // delay={index * 0.1}
                        duration={1.5}
                        height={pxToRem(60)}
                        image1Src={`/images/jamun-trivia-letters/trivia/${letter}.png`}
                        image2Src={`/images/jamun-trivia-letters/trivia/${letter}-blue.png`}
                    />
                ))}
            </div>
        </div>
    </>
);
