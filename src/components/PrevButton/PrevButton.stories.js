import React from "react";
import { PrevButton } from "./index";
import { NextButton } from "../NextButton";
import { pxToRem } from "../../utils/px-to-rem";

export default {
    title: "PrevButton"
};

export const Default = () => (
    <div
        css={{
            display: "grid",
            gridAutoFlow: "column",
            gridGap: pxToRem(32),
            alignItems: "center"
        }}
    >
        <PrevButton />
        <NextButton />
    </div>
);
