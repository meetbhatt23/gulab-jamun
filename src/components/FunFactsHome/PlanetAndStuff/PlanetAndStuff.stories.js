import React from "react";
import { PlanetAndStuff } from "./index";

export default {
    title: "FunFacts/PlanetAndStuff",
};

export const Default = () => (
    <>
        <div css={{ width: "500px" }}>
            <PlanetAndStuff />
        </div>
    </>
);
