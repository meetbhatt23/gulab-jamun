import * as React from "react";

function Svg2DLedikeni(props: React.SVGProps<SVGSVGElement>) {
    return (
        <svg
            viewBox="0 0 57 59"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            {...props}
        >
            <g filter="url(#2_-_d-ledikeni_svg__filter0_iii_1_120)">
                <path
                    d="M7.682.018H33.72c7.91 0 13.66 2.441 17.248 7.324 3.613 4.858 5.42 12.036 5.42 21.533 0 19.531-11.28 29.26-33.838 29.187-3.125 0-5.92-.476-8.386-1.428-2.466-.952-4.455-2.234-5.97-3.845-1.513-1.612-2.71-3.504-3.588-5.677-.855-2.172-1.318-4.48-1.392-6.921-.073-2.466.183-5.005.77-7.617a34.125 34.125 0 012.746-7.69l2.783-5.604c.586-1.318.623-2.295.11-2.93-.488-.659-1.6-1.135-3.332-1.428-2.027-.341-3.565-1.147-4.615-2.417C.651 11.235.138 9.698.138 7.891c0-2.1.672-3.93 2.014-5.493C3.495.81 5.338.018 7.682.018zM33.72 49.712l12.964-31.274c.952-2.441 1.16-4.456.622-6.042-.512-1.587-1.538-2.723-3.076-3.406-1.514-.684-3.235-.83-5.164-.44l-18.75 3.59c-6.225 1.171-8.606 4.394-7.14 9.667l7.726 27.173c.513 1.831 1.367 3.21 2.564 4.138 1.22.904 2.478 1.319 3.772 1.245 1.318-.073 2.588-.525 3.808-1.355a7.22 7.22 0 002.674-3.296z"
                    fill="#000"
                    fillOpacity={0.2}
                />
            </g>
            <defs>
                <filter
                    id="2_-_d-ledikeni_svg__filter0_iii_1_120"
                    x={-3.862}
                    y={-1.982}
                    width={63.25}
                    height={69.154}
                    filterUnits="userSpaceOnUse"
                    colorInterpolationFilters="sRGB"
                >
                    <feFlood floodOpacity={0} result="BackgroundImageFix" />
                    <feBlend
                        in="SourceGraphic"
                        in2="BackgroundImageFix"
                        result="shape"
                    />
                    <feColorMatrix
                        in="SourceAlpha"
                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                        result="hardAlpha"
                    />
                    <feOffset dy={-2} />
                    <feGaussianBlur stdDeviation={5} />
                    <feComposite
                        in2="hardAlpha"
                        operator="arithmetic"
                        k2={-1}
                        k3={1}
                    />
                    <feColorMatrix values="0 0 0 0 0.0196078 0 0 0 0 0.843137 0 0 0 0 0.843137 0 0 0 1 0" />
                    <feBlend in2="shape" result="effect1_innerShadow_1_120" />
                    <feColorMatrix
                        in="SourceAlpha"
                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                        result="hardAlpha"
                    />
                    <feOffset dx={3} dy={10} />
                    <feGaussianBlur stdDeviation={4.5} />
                    <feComposite
                        in2="hardAlpha"
                        operator="arithmetic"
                        k2={-1}
                        k3={1}
                    />
                    <feColorMatrix values="0 0 0 0 0.568627 0 0 0 0 0.0705882 0 0 0 0 0.160235 0 0 0 1 0" />
                    <feBlend
                        in2="effect1_innerShadow_1_120"
                        result="effect2_innerShadow_1_120"
                    />
                    <feColorMatrix
                        in="SourceAlpha"
                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                        result="hardAlpha"
                    />
                    <feOffset dx={-4} />
                    <feGaussianBlur stdDeviation={2.5} />
                    <feComposite
                        in2="hardAlpha"
                        operator="arithmetic"
                        k2={-1}
                        k3={1}
                    />
                    <feColorMatrix values="0 0 0 0 1 0 0 0 0 0.546587 0 0 0 0 0.220833 0 0 0 1 0" />
                    <feBlend
                        in2="effect2_innerShadow_1_120"
                        result="effect3_innerShadow_1_120"
                    />
                </filter>
            </defs>
        </svg>
    );
}

export default Svg2DLedikeni;
