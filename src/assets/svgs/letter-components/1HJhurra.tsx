import * as React from "react";

function Svg1HJhurra(props: React.SVGProps<SVGSVGElement>) {
    return (
        <svg
            viewBox="0 0 57 61"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            {...props}
        >
            <g filter="url(#1_-_h-jhurra_svg__filter0_iii_1_66)">
                <path
                    d="M48.588.882c2.27 0 4.2.672 5.786 2.014 1.611 1.343 2.307 3.089 2.087 5.237l-4.504 41.712a20.96 20.96 0 01-.55 3.15c-.219.927-.585 1.855-1.098 2.782-.513.928-1.135 1.697-1.868 2.307-.732.61-1.696 1.1-2.893 1.465-1.172.39-2.539.586-4.101.586-2.368 0-4.248-.769-5.64-2.307-1.392-1.562-2.087-3.369-2.087-5.42 0-3.076 1.38-5.908 4.138-8.496l6.958-6.592c1.514-1.367 2.27-2.844 2.27-4.43 0-.977-.561-1.588-1.684-1.832-1.099-.244-2.295-.183-3.589.183l-28.491 5.64c-1.538.317-2.576.684-3.113 1.099-.513.39-.77.903-.77 1.538 0 .415.123.854.367 1.318.269.464.55.854.842 1.172l1.319 1.318a34.83 34.83 0 011.355 1.355c2.441 2.661 3.662 5.237 3.662 7.727 0 2.124-.903 3.943-2.71 5.457-1.782 1.513-3.82 2.27-6.116 2.27-2.392 0-4.345-.793-5.86-2.38C.81 56.144.237 53.678.579 50.357l3.955-36.95c.122-1.221.305-2.356.549-3.406.244-1.05.635-2.148 1.172-3.296.537-1.172 1.172-2.16 1.904-2.966.757-.83 1.746-1.514 2.966-2.051 1.221-.537 2.6-.806 4.139-.806 2.514 0 4.48.635 5.896 1.905 1.416 1.245 2.124 2.905 2.124 4.98 0 1.636-.415 3.137-1.245 4.504a8.867 8.867 0 01-3.223 3.223l-8.57 4.87c-1.05.611-1.757 1.21-2.123 1.795-.342.586-.513 1.27-.513 2.051 0 .976.39 1.733 1.172 2.27.805.538 1.867.806 3.186.806h31.897c1.587 0 2.783-.22 3.589-.659.83-.464 1.355-1.22 1.574-2.27.147-.342.171-.72.074-1.136a20.42 20.42 0 01-.147-.915c-.024-.196-.159-.452-.403-.77a7.609 7.609 0 00-.586-.695 12.11 12.11 0 00-.659-.696 27.086 27.086 0 00-.696-.732l-3.222-3.7c-2.442-2.856-3.662-5.321-3.662-7.397 0-2.197.744-3.98 2.233-5.346C43.474 1.578 45.683.882 48.588.882z"
                    fill="#000"
                    fillOpacity={0.2}
                />
            </g>
            <defs>
                <filter
                    id="1_-_h-jhurra_svg__filter0_iii_1_66"
                    x={-3.935}
                    y={-1.118}
                    width={63.726}
                    height={70.253}
                    filterUnits="userSpaceOnUse"
                    colorInterpolationFilters="sRGB"
                >
                    <feFlood floodOpacity={0} result="BackgroundImageFix" />
                    <feBlend
                        in="SourceGraphic"
                        in2="BackgroundImageFix"
                        result="shape"
                    />
                    <feColorMatrix
                        in="SourceAlpha"
                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                        result="hardAlpha"
                    />
                    <feOffset dy={-2} />
                    <feGaussianBlur stdDeviation={5} />
                    <feComposite
                        in2="hardAlpha"
                        operator="arithmetic"
                        k2={-1}
                        k3={1}
                    />
                    <feColorMatrix values="0 0 0 0 0.0196078 0 0 0 0 0.843137 0 0 0 0 0.843137 0 0 0 1 0" />
                    <feBlend in2="shape" result="effect1_innerShadow_1_66" />
                    <feColorMatrix
                        in="SourceAlpha"
                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                        result="hardAlpha"
                    />
                    <feOffset dx={3} dy={10} />
                    <feGaussianBlur stdDeviation={4.5} />
                    <feComposite
                        in2="hardAlpha"
                        operator="arithmetic"
                        k2={-1}
                        k3={1}
                    />
                    <feColorMatrix values="0 0 0 0 0.568627 0 0 0 0 0.0705882 0 0 0 0 0.160235 0 0 0 1 0" />
                    <feBlend
                        in2="effect1_innerShadow_1_66"
                        result="effect2_innerShadow_1_66"
                    />
                    <feColorMatrix
                        in="SourceAlpha"
                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                        result="hardAlpha"
                    />
                    <feOffset dx={-4} />
                    <feGaussianBlur stdDeviation={2.5} />
                    <feComposite
                        in2="hardAlpha"
                        operator="arithmetic"
                        k2={-1}
                        k3={1}
                    />
                    <feColorMatrix values="0 0 0 0 1 0 0 0 0 0.546587 0 0 0 0 0.220833 0 0 0 1 0" />
                    <feBlend
                        in2="effect2_innerShadow_1_66"
                        result="effect3_innerShadow_1_66"
                    />
                </filter>
            </defs>
        </svg>
    );
}

export default Svg1HJhurra;
