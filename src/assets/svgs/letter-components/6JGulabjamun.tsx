import * as React from "react";

function Svg6JGulabjamun(props: React.SVGProps<SVGSVGElement>) {
    return (
        <svg
            viewBox="0 0 34 61"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            {...props}
        >
            <g filter="url(#6_-_J-gulabjamun_svg__filter0_iii_2_167)">
                <path
                    d="M7.822 49.625l20.031-3.992c1.587-.293 2.564-1.147 2.93-2.563.366-1.416-.037-3.162-1.209-5.237L19.358 20.365c-.928-1.563-1.733-3.382-2.417-5.457-.683-2.075-1.025-3.882-1.025-5.42 0-1.025.134-2.014.403-2.966a9.797 9.797 0 011.355-2.783c.61-.88 1.477-1.587 2.6-2.124 1.123-.537 2.429-.806 3.918-.806 1.123 0 2.185.195 3.186.586 1.026.366 1.99.964 2.893 1.794.904.806 1.624 1.966 2.16 3.48.538 1.513.807 3.283.807 5.31v28.344c0 2.832-.11 5.225-.33 7.178-.195 1.953-.586 3.784-1.172 5.493-.561 1.685-1.343 3.028-2.344 4.029-1 .976-2.307 1.733-3.918 2.27-1.611.562-3.565.842-5.86.842-2.783 0-5.383-.451-7.8-1.355-2.417-.903-4.443-2.1-6.079-3.589-1.611-1.489-2.88-3.173-3.808-5.053-.928-1.88-1.392-3.797-1.392-5.75 0-3.174.574-6.225 1.721-9.155 1.148-2.954 2.588-5.286 4.321-6.995 1.758-1.733 3.504-2.6 5.237-2.6 1.148 0 2.16.256 3.04.77.879.512 1.513 1.159 1.904 1.94.39.781.5 1.66.33 2.637-.171.976-.66 1.892-1.465 2.746l-9.961 10.4c-.684.733-1.11 1.478-1.282 2.235-.146.757-.085 1.391.183 1.904.269.513.696.903 1.282 1.172.61.268 1.27.33 1.978.183z"
                    fill="#000"
                    fillOpacity={0.2}
                />
            </g>
            <defs>
                <filter
                    id="6_-_J-gulabjamun_svg__filter0_iii_2_167"
                    x={-3.466}
                    y={-1.191}
                    width={39.703}
                    height={70.326}
                    filterUnits="userSpaceOnUse"
                    colorInterpolationFilters="sRGB"
                >
                    <feFlood floodOpacity={0} result="BackgroundImageFix" />
                    <feBlend
                        in="SourceGraphic"
                        in2="BackgroundImageFix"
                        result="shape"
                    />
                    <feColorMatrix
                        in="SourceAlpha"
                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                        result="hardAlpha"
                    />
                    <feOffset dy={-2} />
                    <feGaussianBlur stdDeviation={5} />
                    <feComposite
                        in2="hardAlpha"
                        operator="arithmetic"
                        k2={-1}
                        k3={1}
                    />
                    <feColorMatrix values="0 0 0 0 0.0196078 0 0 0 0 0.843137 0 0 0 0 0.843137 0 0 0 1 0" />
                    <feBlend in2="shape" result="effect1_innerShadow_2_167" />
                    <feColorMatrix
                        in="SourceAlpha"
                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                        result="hardAlpha"
                    />
                    <feOffset dx={3} dy={10} />
                    <feGaussianBlur stdDeviation={4.5} />
                    <feComposite
                        in2="hardAlpha"
                        operator="arithmetic"
                        k2={-1}
                        k3={1}
                    />
                    <feColorMatrix values="0 0 0 0 0.568627 0 0 0 0 0.0705882 0 0 0 0 0.160235 0 0 0 1 0" />
                    <feBlend
                        in2="effect1_innerShadow_2_167"
                        result="effect2_innerShadow_2_167"
                    />
                    <feColorMatrix
                        in="SourceAlpha"
                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                        result="hardAlpha"
                    />
                    <feOffset dx={-4} />
                    <feGaussianBlur stdDeviation={2.5} />
                    <feComposite
                        in2="hardAlpha"
                        operator="arithmetic"
                        k2={-1}
                        k3={1}
                    />
                    <feColorMatrix values="0 0 0 0 1 0 0 0 0 0.546587 0 0 0 0 0.220833 0 0 0 1 0" />
                    <feBlend
                        in2="effect2_innerShadow_2_167"
                        result="effect3_innerShadow_2_167"
                    />
                </filter>
            </defs>
        </svg>
    );
}

export default Svg6JGulabjamun;
