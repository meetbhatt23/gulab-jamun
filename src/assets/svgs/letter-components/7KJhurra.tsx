import * as React from "react";

function Svg7KJhurra(props: React.SVGProps<SVGSVGElement>) {
    return (
        <svg
            viewBox="0 0 60 61"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            {...props}
        >
            <g filter="url(#7_-_k-jhurra_svg__filter0_iii_1_72)">
                <path
                    d="M12.187.882c1.83 0 3.259.44 4.284 1.319 1.026.879 1.587 1.99 1.685 3.332.122 1.343-.11 2.82-.696 4.431-.586 1.612-1.49 3.138-2.71 4.578l-1.209 1.392c-2.368 2.93-3.064 5.163-2.087 6.701.464.757.977 1.27 1.538 1.538.586.245 1.245.257 1.978.037a11.403 11.403 0 002.16-.989c.708-.44 1.587-1.062 2.637-1.867L39.36 6.485c2.686-2.026 4.932-3.466 6.739-4.32C47.928 1.31 49.93.881 52.103.881c2.076 0 3.773.977 5.09 2.93 1.32 1.953 1.795 4.077 1.43 6.372-.44 2.54-1.49 4.456-3.15 5.75-1.636 1.27-4.212 2.295-7.727 3.076L24.27 24.137c-.22.049-.598.122-1.135.22-.537.097-.94.183-1.208.256-.269.049-.647.134-1.136.256a8.217 8.217 0 00-1.135.293c-.268.098-.586.232-.952.403-.366.17-.66.342-.879.513-.195.17-.378.378-.55.622-.146.22-.243.464-.292.733-.342 1.977.61 3.503 2.856 4.577l26.111 12.525c1.05.512 1.99.342 2.82-.513.855-.855 1.11-1.831.769-2.93l-2.783-9.082c-.488-1.611-.366-2.905.366-3.882.757-1 1.77-1.538 3.04-1.61 1.44-.05 2.66.45 3.662 1.5 1.025 1.05 1.403 2.552 1.135 4.505l-2.71 18.75c-.44 3.003-1.538 5.237-3.296 6.702-1.758 1.44-3.857 2.16-6.299 2.16-.61 0-1.196-.06-1.758-.183a10.683 10.683 0 01-1.464-.366c-.415-.146-.867-.39-1.355-.733a34.248 34.248 0 01-1.209-.878c-.317-.269-.745-.684-1.282-1.245a72.95 72.95 0 00-1.208-1.355 71.645 71.645 0 00-1.392-1.685 45.618 45.618 0 00-1.501-1.831L10.94 27.433c-.708-.83-1.477-1.33-2.307-1.502-.805-.17-1.452-.037-1.94.403-.489.415-.806.977-.953 1.685-.146.708.086 1.538.696 2.49l9.375 14.319c1.733 2.563 2.356 5.334 1.868 8.313-.342 1.953-1.367 3.613-3.076 4.98-1.71 1.343-3.833 2.014-6.373 2.014-2.66 0-4.663-.903-6.005-2.71C.883 55.595.309 53.043.504 49.772L3.141 10.55C3.385 7.23 4.35 4.79 6.034 3.226 7.744 1.664 9.794.882 12.187.882z"
                    fill="#000"
                    fillOpacity={0.2}
                />
            </g>
            <defs>
                <filter
                    id="7_-_k-jhurra_svg__filter0_iii_1_72"
                    x={-3.789}
                    y={-1.118}
                    width={65.96}
                    height={70.253}
                    filterUnits="userSpaceOnUse"
                    colorInterpolationFilters="sRGB"
                >
                    <feFlood floodOpacity={0} result="BackgroundImageFix" />
                    <feBlend
                        in="SourceGraphic"
                        in2="BackgroundImageFix"
                        result="shape"
                    />
                    <feColorMatrix
                        in="SourceAlpha"
                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                        result="hardAlpha"
                    />
                    <feOffset dy={-2} />
                    <feGaussianBlur stdDeviation={5} />
                    <feComposite
                        in2="hardAlpha"
                        operator="arithmetic"
                        k2={-1}
                        k3={1}
                    />
                    <feColorMatrix values="0 0 0 0 0.0196078 0 0 0 0 0.843137 0 0 0 0 0.843137 0 0 0 1 0" />
                    <feBlend in2="shape" result="effect1_innerShadow_1_72" />
                    <feColorMatrix
                        in="SourceAlpha"
                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                        result="hardAlpha"
                    />
                    <feOffset dx={3} dy={10} />
                    <feGaussianBlur stdDeviation={4.5} />
                    <feComposite
                        in2="hardAlpha"
                        operator="arithmetic"
                        k2={-1}
                        k3={1}
                    />
                    <feColorMatrix values="0 0 0 0 0.568627 0 0 0 0 0.0705882 0 0 0 0 0.160235 0 0 0 1 0" />
                    <feBlend
                        in2="effect1_innerShadow_1_72"
                        result="effect2_innerShadow_1_72"
                    />
                    <feColorMatrix
                        in="SourceAlpha"
                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                        result="hardAlpha"
                    />
                    <feOffset dx={-4} />
                    <feGaussianBlur stdDeviation={2.5} />
                    <feComposite
                        in2="hardAlpha"
                        operator="arithmetic"
                        k2={-1}
                        k3={1}
                    />
                    <feColorMatrix values="0 0 0 0 1 0 0 0 0 0.546587 0 0 0 0 0.220833 0 0 0 1 0" />
                    <feBlend
                        in2="effect2_innerShadow_1_72"
                        result="effect3_innerShadow_1_72"
                    />
                </filter>
            </defs>
        </svg>
    );
}

export default Svg7KJhurra;
