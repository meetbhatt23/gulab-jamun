import * as React from "react";

function Svg12SJhurra(props: React.SVGProps<SVGSVGElement>) {
    return (
        <svg
            viewBox="0 0 56 61"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            {...props}
        >
            <g filter="url(#12_-_s-jhurra_svg__filter0_iii_1_77)">
                <path
                    d="M24.283 60.062c-3.467 0-6.604-.354-9.412-1.062-2.783-.708-5.078-1.648-6.885-2.82-1.806-1.196-3.32-2.588-4.54-4.175-1.197-1.611-2.051-3.247-2.564-4.907a18.846 18.846 0 01-.733-5.273c0-1.49.184-2.881.55-4.175a7.67 7.67 0 012.014-3.333c.976-.952 2.173-1.428 3.589-1.428 1.733 0 3.137.623 4.211 1.868 1.099 1.245 1.648 2.698 1.648 4.358v12.744c0 1.904.562 3.186 1.685 3.845 1.123.635 2.807.464 5.053-.513l26.258-12.707c2.173-1.05 3.564-1.856 4.175-2.417.927-.855 1.233-1.868.915-3.04-.342-1.342-1.05-2.16-2.124-2.453-1.074-.318-2.246-.147-3.516.512l-15.893 7.325c-2.417 1.05-4.517 1.342-6.299.879-1.782-.489-3.186-1.44-4.211-2.857-1.026-1.416-1.563-3.052-1.612-4.907l-.11-2.857c-.024-1.293-.33-2.16-.915-2.6-.562-.44-1.672-.842-3.333-1.208L7.51 27.726c-1.513-.367-2.746-1.197-3.698-2.49-.953-1.319-1.429-2.76-1.429-4.322 0-3.15.757-6.018 2.27-8.606a18.983 18.983 0 016.08-6.372c2.515-1.66 5.334-2.942 8.46-3.845a35.157 35.157 0 019.74-1.355c2.662 0 5.213.232 7.654.696 2.466.44 4.81 1.16 7.032 2.16 2.221.977 4.15 2.198 5.786 3.662 1.636 1.44 2.93 3.235 3.882 5.384.976 2.148 1.465 4.528 1.465 7.14 0 2.76-.538 4.957-1.612 6.593-1.074 1.61-2.685 2.417-4.834 2.417-.61 0-1.233-.11-1.867-.33a4.712 4.712 0 01-1.722-1.062 4.835 4.835 0 01-1.172-1.758c-.244-.683-.256-1.526-.036-2.527.22-1.025.72-2.111 1.501-3.259l3.296-4.944c.44-.708.635-1.355.586-1.94-.049-.587-.269-1.014-.66-1.283-.951-.683-2.71-.756-5.273-.22l-32.3 6.959c-2.783.561-4.04 1.782-3.771 3.662.097.806.415 1.416.952 1.831.561.415 1.452.732 2.673.952l38.16 6.922c2.075.341 3.65 1.184 4.724 2.526 1.098 1.319 1.647 2.918 1.647 4.798 0 2.588-.805 5.151-2.417 7.69-1.61 2.515-3.772 4.749-6.482 6.702-2.71 1.953-5.993 3.54-9.85 4.76-3.834 1.197-7.838 1.795-12.012 1.795z"
                    fill="#000"
                    fillOpacity={0.2}
                />
            </g>
            <defs>
                <filter
                    id="12_-_s-jhurra_svg__filter0_iii_1_77"
                    x={-3.851}
                    y={-1.264}
                    width={61.895}
                    height={70.326}
                    filterUnits="userSpaceOnUse"
                    colorInterpolationFilters="sRGB"
                >
                    <feFlood floodOpacity={0} result="BackgroundImageFix" />
                    <feBlend
                        in="SourceGraphic"
                        in2="BackgroundImageFix"
                        result="shape"
                    />
                    <feColorMatrix
                        in="SourceAlpha"
                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                        result="hardAlpha"
                    />
                    <feOffset dy={-2} />
                    <feGaussianBlur stdDeviation={5} />
                    <feComposite
                        in2="hardAlpha"
                        operator="arithmetic"
                        k2={-1}
                        k3={1}
                    />
                    <feColorMatrix values="0 0 0 0 0.0196078 0 0 0 0 0.843137 0 0 0 0 0.843137 0 0 0 1 0" />
                    <feBlend in2="shape" result="effect1_innerShadow_1_77" />
                    <feColorMatrix
                        in="SourceAlpha"
                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                        result="hardAlpha"
                    />
                    <feOffset dx={3} dy={10} />
                    <feGaussianBlur stdDeviation={4.5} />
                    <feComposite
                        in2="hardAlpha"
                        operator="arithmetic"
                        k2={-1}
                        k3={1}
                    />
                    <feColorMatrix values="0 0 0 0 0.568627 0 0 0 0 0.0705882 0 0 0 0 0.160235 0 0 0 1 0" />
                    <feBlend
                        in2="effect1_innerShadow_1_77"
                        result="effect2_innerShadow_1_77"
                    />
                    <feColorMatrix
                        in="SourceAlpha"
                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                        result="hardAlpha"
                    />
                    <feOffset dx={-4} />
                    <feGaussianBlur stdDeviation={2.5} />
                    <feComposite
                        in2="hardAlpha"
                        operator="arithmetic"
                        k2={-1}
                        k3={1}
                    />
                    <feColorMatrix values="0 0 0 0 1 0 0 0 0 0.546587 0 0 0 0 0.220833 0 0 0 1 0" />
                    <feBlend
                        in2="effect2_innerShadow_1_77"
                        result="effect3_innerShadow_1_77"
                    />
                </filter>
            </defs>
        </svg>
    );
}

export default Svg12SJhurra;
