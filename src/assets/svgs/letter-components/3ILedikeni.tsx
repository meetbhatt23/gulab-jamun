import * as React from "react";

function Svg3ILedikeni(props: React.SVGProps<SVGSVGElement>) {
    return (
        <svg
            viewBox="0 0 26 61"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            {...props}
        >
            <g filter="url(#3_-_i-ledikeni_svg__filter0_iii_1_121)">
                <path
                    d="M.003 18.644c0-.88 1.172-2.674 3.516-5.384l8.496-9.778c1.513-1.733 3.479-2.6 5.896-2.6 2.026 0 3.784.647 5.273 1.941 1.49 1.294 2.234 3.015 2.234 5.164 0 1.977-.537 3.54-1.611 4.687-1.05 1.123-2.564 2.039-4.541 2.747l-8.24 2.856c-.952.293-1.428.867-1.428 1.721 0 .953.647 1.355 1.94 1.209l5.128-.623c2.44-.293 4.357.11 5.749 1.209 1.416 1.074 2.075 2.551 1.978 4.431-.098 1.856-.953 3.711-2.564 5.567L7.51 48.123c-1.05 1.172-1.574 2.076-1.574 2.71 0 .782.268 1.416.805 1.905.562.464 1.184.55 1.868.256l9.009-3.882c.805-.342 1.599-.549 2.38-.622a5.393 5.393 0 012.27.183c.709.22 1.294.659 1.758 1.318.489.66.782 1.514.88 2.564.17 1.587-.294 2.978-1.392 4.175-1.099 1.171-2.515 2.026-4.248 2.563-1.734.562-3.601.842-5.603.842-4.2 0-7.264-1.245-9.192-3.735-1.905-2.515-2.344-5.676-1.319-9.485l6.08-22.192c.146-.513.22-.965.22-1.355 0-.83-.306-1.416-.916-1.758-.61-.366-1.55-.55-2.82-.55H2.493c-1.66 0-2.49-.805-2.49-2.416z"
                    fill="#000"
                    fillOpacity={0.2}
                />
            </g>
            <defs>
                <filter
                    id="3_-_i-ledikeni_svg__filter0_iii_1_121"
                    x={-3.997}
                    y={-1.118}
                    width={32.415}
                    height={70.253}
                    filterUnits="userSpaceOnUse"
                    colorInterpolationFilters="sRGB"
                >
                    <feFlood floodOpacity={0} result="BackgroundImageFix" />
                    <feBlend
                        in="SourceGraphic"
                        in2="BackgroundImageFix"
                        result="shape"
                    />
                    <feColorMatrix
                        in="SourceAlpha"
                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                        result="hardAlpha"
                    />
                    <feOffset dy={-2} />
                    <feGaussianBlur stdDeviation={5} />
                    <feComposite
                        in2="hardAlpha"
                        operator="arithmetic"
                        k2={-1}
                        k3={1}
                    />
                    <feColorMatrix values="0 0 0 0 0.0196078 0 0 0 0 0.843137 0 0 0 0 0.843137 0 0 0 1 0" />
                    <feBlend in2="shape" result="effect1_innerShadow_1_121" />
                    <feColorMatrix
                        in="SourceAlpha"
                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                        result="hardAlpha"
                    />
                    <feOffset dx={3} dy={10} />
                    <feGaussianBlur stdDeviation={4.5} />
                    <feComposite
                        in2="hardAlpha"
                        operator="arithmetic"
                        k2={-1}
                        k3={1}
                    />
                    <feColorMatrix values="0 0 0 0 0.568627 0 0 0 0 0.0705882 0 0 0 0 0.160235 0 0 0 1 0" />
                    <feBlend
                        in2="effect1_innerShadow_1_121"
                        result="effect2_innerShadow_1_121"
                    />
                    <feColorMatrix
                        in="SourceAlpha"
                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                        result="hardAlpha"
                    />
                    <feOffset dx={-4} />
                    <feGaussianBlur stdDeviation={2.5} />
                    <feComposite
                        in2="hardAlpha"
                        operator="arithmetic"
                        k2={-1}
                        k3={1}
                    />
                    <feColorMatrix values="0 0 0 0 1 0 0 0 0 0.546587 0 0 0 0 0.220833 0 0 0 1 0" />
                    <feBlend
                        in2="effect2_innerShadow_1_121"
                        result="effect3_innerShadow_1_121"
                    />
                </filter>
            </defs>
        </svg>
    );
}

export default Svg3ILedikeni;
