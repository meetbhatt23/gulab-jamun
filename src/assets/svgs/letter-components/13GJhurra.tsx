import * as React from "react";

function Svg13GJhurra(props: React.SVGProps<SVGSVGElement>) {
    return (
        <svg
            viewBox="0 0 59 61"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            {...props}
        >
            <g filter="url(#13_-_g-jhurra_svg__filter0_iii_1_78)">
                <path
                    d="M20.06 16.336l2.197 2.747c1.49 1.758 2.075 3.601 1.758 5.53-.293 1.928-1.55 3.637-3.772 5.127L8.158 38.016c-1.806 1.22-2.624 2.893-2.453 5.017.122 1.734.83 3.137 2.124 4.212 1.538 1.27 3.234 1.428 5.09.476.098-.05.207-.11.33-.183L44.962 28.64c1.758-1.05 3.43-1.709 5.017-1.977 1.587-.269 2.942-.184 4.065.256 1.123.44 2.027 1.184 2.71 2.234.684 1.05 1.062 2.344 1.136 3.882l.732 15.747c.171 3.882-.537 6.75-2.124 8.606-1.563 1.83-3.808 2.746-6.738 2.746-1.904 0-3.528-.586-4.87-1.758-1.344-1.196-2.1-2.783-2.271-4.76-.171-1.978.537-4.26 2.124-6.848l8.02-12.378c.659-1.221.732-2.21.22-2.967-.489-.756-1.21-1.135-2.161-1.135-.586 0-1.062.061-1.428.183-.367.098-.88.427-1.538.989-.66.562-1.416 1.416-2.271 2.563L33.28 50.064c-2.05 2.686-3.796 4.712-5.236 6.08-1.417 1.342-2.955 2.343-4.615 3.003-1.635.659-3.576.988-5.823.988-2.246 0-4.382-.61-6.408-1.83-2.027-1.221-3.845-2.992-5.457-5.31-1.611-2.32-2.893-5.323-3.845-9.01-.928-3.71-1.392-7.861-1.392-12.45 0-4.786.696-9.107 2.088-12.965 1.416-3.882 3.381-7.104 5.896-9.668 2.539-2.588 5.578-4.565 9.118-5.932C21.146 1.578 25.04.882 29.29.882c7.104 0 12.927 1.111 17.468 3.333 4.565 2.197 7.422 5.285 8.57 9.265.365 1.22.39 2.32.072 3.296a7.35 7.35 0 01-1.428 2.6c-.659.757-1.501 1.33-2.527 1.721a10.687 10.687 0 01-3.113.623c-1.025.024-2.075-.22-3.149-.733-1.074-.512-1.99-1.27-2.747-2.27l-8.35-10.95c-1.293-1.66-3.014-2.77-5.163-3.332-2.148-.586-4.37-.428-6.665.476-1.245.537-2.221 1.281-2.93 2.234-.707.952-1.11 1.965-1.208 3.04a9.107 9.107 0 00.366 3.222 8.96 8.96 0 001.575 2.93z"
                    fill="#000"
                    fillOpacity={0.2}
                />
            </g>
            <defs>
                <filter
                    id="13_-_g-jhurra_svg__filter0_iii_1_78"
                    x={-3.496}
                    y={-1.118}
                    width={65.374}
                    height={70.253}
                    filterUnits="userSpaceOnUse"
                    colorInterpolationFilters="sRGB"
                >
                    <feFlood floodOpacity={0} result="BackgroundImageFix" />
                    <feBlend
                        in="SourceGraphic"
                        in2="BackgroundImageFix"
                        result="shape"
                    />
                    <feColorMatrix
                        in="SourceAlpha"
                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                        result="hardAlpha"
                    />
                    <feOffset dy={-2} />
                    <feGaussianBlur stdDeviation={5} />
                    <feComposite
                        in2="hardAlpha"
                        operator="arithmetic"
                        k2={-1}
                        k3={1}
                    />
                    <feColorMatrix values="0 0 0 0 0.0196078 0 0 0 0 0.843137 0 0 0 0 0.843137 0 0 0 1 0" />
                    <feBlend in2="shape" result="effect1_innerShadow_1_78" />
                    <feColorMatrix
                        in="SourceAlpha"
                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                        result="hardAlpha"
                    />
                    <feOffset dx={3} dy={10} />
                    <feGaussianBlur stdDeviation={4.5} />
                    <feComposite
                        in2="hardAlpha"
                        operator="arithmetic"
                        k2={-1}
                        k3={1}
                    />
                    <feColorMatrix values="0 0 0 0 0.568627 0 0 0 0 0.0705882 0 0 0 0 0.160235 0 0 0 1 0" />
                    <feBlend
                        in2="effect1_innerShadow_1_78"
                        result="effect2_innerShadow_1_78"
                    />
                    <feColorMatrix
                        in="SourceAlpha"
                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                        result="hardAlpha"
                    />
                    <feOffset dx={-4} />
                    <feGaussianBlur stdDeviation={2.5} />
                    <feComposite
                        in2="hardAlpha"
                        operator="arithmetic"
                        k2={-1}
                        k3={1}
                    />
                    <feColorMatrix values="0 0 0 0 1 0 0 0 0 0.546587 0 0 0 0 0.220833 0 0 0 1 0" />
                    <feBlend
                        in2="effect2_innerShadow_1_78"
                        result="effect3_innerShadow_1_78"
                    />
                </filter>
            </defs>
        </svg>
    );
}

export default Svg13GJhurra;
