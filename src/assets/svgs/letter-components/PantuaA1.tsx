import * as React from "react";

function SvgPantuaA1(props: React.SVGProps<SVGSVGElement>) {
    return (
        <svg
            viewBox="0 0 57 60"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            {...props}
        >
            <g filter="url(#pantua-a-1_svg__filter0_iii_0_1)">
                <path
                    d="M32.327 41.107l-19.35.45c-6.6.15-12 2.625-12 8.625 0 7.05 4.875 9.525 9.225 9.525 6.15 0 11.7-5.55 15.15-8.475l7.35-6.3c2.625-2.25 4.8-1.725 4.8 2.4v3.9c0 4.575 2.925 8.475 9.075 8.475 7.725 0 10.275-6.45 8.7-14.85L48.452 8.782c-.975-5.1-4.05-8.325-8.55-8.325-4.875 0-7.575 1.95-14.1 7.125l-15.075 12c-3.45 2.775-5.7 5.7-5.7 8.925 0 4.65 3.6 8.325 8.025 8.325 2.775 0 6.975-.975 9.375-5.925l9.075-18.6c1.125-2.325 3.825-8.025 6.975-8.625 3.15-.6 5.175 2.025 4.65 6.15l-3.225 26.55c-.45 3.825-3.3 4.65-7.575 4.725z"
                    fill="#000"
                    fillOpacity={0.2}
                />
            </g>
            <defs>
                <filter
                    id="pantua-a-1_svg__filter0_iii_0_1"
                    x={-68.837}
                    y={-4.2}
                    width={402.625}
                    height={72.907}
                    filterUnits="userSpaceOnUse"
                    colorInterpolationFilters="sRGB"
                >
                    <feFlood floodOpacity={0} result="BackgroundImageFix" />
                    <feBlend
                        in="SourceGraphic"
                        in2="BackgroundImageFix"
                        result="shape"
                    />
                    <feColorMatrix
                        in="SourceAlpha"
                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                        result="hardAlpha"
                    />
                    <feOffset dy={-2} />
                    <feGaussianBlur stdDeviation={5} />
                    <feComposite
                        in2="hardAlpha"
                        operator="arithmetic"
                        k2={-1}
                        k3={1}
                    />
                    <feColorMatrix values="0 0 0 0 0.0196078 0 0 0 0 0.843137 0 0 0 0 0.843137 0 0 0 1 0" />
                    <feBlend in2="shape" result="effect1_innerShadow_0_1" />
                    <feColorMatrix
                        in="SourceAlpha"
                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                        result="hardAlpha"
                    />
                    <feOffset dx={3} dy={10} />
                    <feGaussianBlur stdDeviation={4.5} />
                    <feComposite
                        in2="hardAlpha"
                        operator="arithmetic"
                        k2={-1}
                        k3={1}
                    />
                    <feColorMatrix values="0 0 0 0 0.568627 0 0 0 0 0.0705882 0 0 0 0 0.160235 0 0 0 1 0" />
                    <feBlend
                        in2="effect1_innerShadow_0_1"
                        result="effect2_innerShadow_0_1"
                    />
                    <feColorMatrix
                        in="SourceAlpha"
                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                        result="hardAlpha"
                    />
                    <feOffset dx={-4} />
                    <feGaussianBlur stdDeviation={2.5} />
                    <feComposite
                        in2="hardAlpha"
                        operator="arithmetic"
                        k2={-1}
                        k3={1}
                    />
                    <feColorMatrix values="0 0 0 0 1 0 0 0 0 0.546587 0 0 0 0 0.220833 0 0 0 1 0" />
                    <feBlend
                        in2="effect2_innerShadow_0_1"
                        result="effect3_innerShadow_0_1"
                    />
                </filter>
            </defs>
        </svg>
    );
}

export default SvgPantuaA1;
