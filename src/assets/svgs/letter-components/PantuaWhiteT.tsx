import * as React from "react";

function SvgPantuaWhiteT(props: React.SVGProps<SVGSVGElement>) {
    return (
        <svg
            viewBox="0 0 35 36"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            {...props}
        >
            <path
                d="M16.857 5.864c-.9-2.565-.45-3.6.765-3.6.405 0 .9.225 1.35.72l5.04 5.625c4.14 4.59 10.8 1.98 10.8-3.195 0-2.295-1.485-4.995-6.165-4.995h-15.39C5.112.419.162 4.919.162 11.174c0 2.79 2.16 5.265 5.04 5.355 4.095.135 5.4-2.835 5.4-9.045v-1.98c0-1.98.45-3.465 1.98-3.465 1.215 0 1.71 1.17 1.755 4.005l.495 22.005c.09 4.77.99 7.245 5.31 7.245 4.815 0 5.805-3.465 3.96-8.37l-7.245-21.06z"
                fill="#fff"
            />
        </svg>
    );
}

export default SvgPantuaWhiteT;
