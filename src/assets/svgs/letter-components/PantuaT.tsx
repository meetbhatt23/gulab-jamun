import * as React from "react";

function SvgPantuaT(props: React.SVGProps<SVGSVGElement>) {
    return (
        <svg
            viewBox="0 0 59 59"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            {...props}
        >
            <g filter="url(#pantua-t_svg__filter0_iii_0_1)">
                <path
                    d="M28.453 9.075c-1.5-4.275-.75-6 1.275-6 .675 0 1.5.375 2.25 1.2l8.4 9.375c6.9 7.65 18 3.3 18-5.325C58.378 4.5 55.903 0 48.103 0h-25.65C8.878 0 .628 7.5.628 17.925c0 4.65 3.6 8.775 8.4 8.925 6.825.225 9-4.725 9-15.075v-3.3c0-3.3.75-5.775 3.3-5.775 2.025 0 2.85 1.95 2.925 6.675l.825 36.675c.15 7.95 1.65 12.075 8.85 12.075 8.025 0 9.675-5.775 6.6-13.95l-12.075-35.1z"
                    fill="#000"
                    fillOpacity={0.2}
                />
            </g>
            <defs>
                <filter
                    id="pantua-t_svg__filter0_iii_0_1"
                    x={-205.837}
                    y={-3.2}
                    width={402.625}
                    height={72.907}
                    filterUnits="userSpaceOnUse"
                    colorInterpolationFilters="sRGB"
                >
                    <feFlood floodOpacity={0} result="BackgroundImageFix" />
                    <feBlend
                        in="SourceGraphic"
                        in2="BackgroundImageFix"
                        result="shape"
                    />
                    <feColorMatrix
                        in="SourceAlpha"
                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                        result="hardAlpha"
                    />
                    <feOffset dy={-2} />
                    <feGaussianBlur stdDeviation={5} />
                    <feComposite
                        in2="hardAlpha"
                        operator="arithmetic"
                        k2={-1}
                        k3={1}
                    />
                    <feColorMatrix values="0 0 0 0 0.0196078 0 0 0 0 0.843137 0 0 0 0 0.843137 0 0 0 1 0" />
                    <feBlend in2="shape" result="effect1_innerShadow_0_1" />
                    <feColorMatrix
                        in="SourceAlpha"
                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                        result="hardAlpha"
                    />
                    <feOffset dx={3} dy={10} />
                    <feGaussianBlur stdDeviation={4.5} />
                    <feComposite
                        in2="hardAlpha"
                        operator="arithmetic"
                        k2={-1}
                        k3={1}
                    />
                    <feColorMatrix values="0 0 0 0 0.568627 0 0 0 0 0.0705882 0 0 0 0 0.160235 0 0 0 1 0" />
                    <feBlend
                        in2="effect1_innerShadow_0_1"
                        result="effect2_innerShadow_0_1"
                    />
                    <feColorMatrix
                        in="SourceAlpha"
                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                        result="hardAlpha"
                    />
                    <feOffset dx={-4} />
                    <feGaussianBlur stdDeviation={2.5} />
                    <feComposite
                        in2="hardAlpha"
                        operator="arithmetic"
                        k2={-1}
                        k3={1}
                    />
                    <feColorMatrix values="0 0 0 0 1 0 0 0 0 0.546587 0 0 0 0 0.220833 0 0 0 1 0" />
                    <feBlend
                        in2="effect2_innerShadow_0_1"
                        result="effect3_innerShadow_0_1"
                    />
                </filter>
            </defs>
        </svg>
    );
}

export default SvgPantuaT;
