import * as React from "react";

function Svg9UGulabjamun(props: React.SVGProps<SVGSVGElement>) {
    return (
        <svg
            viewBox="0 0 52 61"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            {...props}
        >
            <g filter="url(#9_-_u-gulabjamun_svg__filter0_iii_2_170)">
                <path
                    d="M41.82 41.605l-15.308-16.59c-.952-1.05-1.623-2.123-2.014-3.222-.39-1.123-.513-2.124-.366-3.003a4.56 4.56 0 011.135-2.27 4.45 4.45 0 012.198-1.319c.878-.22 1.904-.146 3.076.22 1.172.342 2.356.989 3.552 1.94l8.716 7.069c.488.39.976.634 1.465.732.512.073.927.049 1.245-.073a2.28 2.28 0 00.805-.513c1.123-1.123 1.075-2.392-.146-3.808l-4.358-5.054c-1.904-2.197-3.21-3.845-3.918-4.944-.684-1.123-1.026-2.32-1.026-3.589 0-1.83.61-3.332 1.831-4.504 1.22-1.197 2.881-1.795 4.98-1.795 2.637 0 4.542.818 5.714 2.454 1.196 1.611 1.794 4.09 1.794 7.434v28.125c0 14.16-8.3 21.24-24.902 21.24-5.03 0-9.473-.94-13.33-2.82-3.833-1.88-6.824-4.577-8.973-8.093C1.866 45.682.804 41.568.804 36.881V12.565c0-4.078.72-7.044 2.161-8.9C4.43 1.81 6.627.883 9.557.883c1.904 0 3.515.476 4.834 1.429 1.343.952 2.258 2.136 2.746 3.552.489 1.416.635 2.942.44 4.577a9.972 9.972 0 01-1.612 4.395L6.738 28.458c-1.514 2.295-2.063 4.407-1.648 6.336.44 1.904 1.782 3.417 4.028 4.54l21.314 10.95c6.103 3.125 10.327 3.028 12.67-.293.684-.976 1.014-2.026.99-3.15 0-1.147-.22-2.135-.66-2.965a9.153 9.153 0 00-1.611-2.271z"
                    fill="#000"
                    fillOpacity={0.2}
                />
            </g>
            <defs>
                <filter
                    id="9_-_u-gulabjamun_svg__filter0_iii_2_170"
                    x={-3.196}
                    y={-1.118}
                    width={57.391}
                    height={70.253}
                    filterUnits="userSpaceOnUse"
                    colorInterpolationFilters="sRGB"
                >
                    <feFlood floodOpacity={0} result="BackgroundImageFix" />
                    <feBlend
                        in="SourceGraphic"
                        in2="BackgroundImageFix"
                        result="shape"
                    />
                    <feColorMatrix
                        in="SourceAlpha"
                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                        result="hardAlpha"
                    />
                    <feOffset dy={-2} />
                    <feGaussianBlur stdDeviation={5} />
                    <feComposite
                        in2="hardAlpha"
                        operator="arithmetic"
                        k2={-1}
                        k3={1}
                    />
                    <feColorMatrix values="0 0 0 0 0.0196078 0 0 0 0 0.843137 0 0 0 0 0.843137 0 0 0 1 0" />
                    <feBlend in2="shape" result="effect1_innerShadow_2_170" />
                    <feColorMatrix
                        in="SourceAlpha"
                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                        result="hardAlpha"
                    />
                    <feOffset dx={3} dy={10} />
                    <feGaussianBlur stdDeviation={4.5} />
                    <feComposite
                        in2="hardAlpha"
                        operator="arithmetic"
                        k2={-1}
                        k3={1}
                    />
                    <feColorMatrix values="0 0 0 0 0.568627 0 0 0 0 0.0705882 0 0 0 0 0.160235 0 0 0 1 0" />
                    <feBlend
                        in2="effect1_innerShadow_2_170"
                        result="effect2_innerShadow_2_170"
                    />
                    <feColorMatrix
                        in="SourceAlpha"
                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                        result="hardAlpha"
                    />
                    <feOffset dx={-4} />
                    <feGaussianBlur stdDeviation={2.5} />
                    <feComposite
                        in2="hardAlpha"
                        operator="arithmetic"
                        k2={-1}
                        k3={1}
                    />
                    <feColorMatrix values="0 0 0 0 1 0 0 0 0 0.546587 0 0 0 0 0.220833 0 0 0 1 0" />
                    <feBlend
                        in2="effect2_innerShadow_2_170"
                        result="effect3_innerShadow_2_170"
                    />
                </filter>
            </defs>
        </svg>
    );
}

export default Svg9UGulabjamun;
