import * as React from "react";

function Svg4RJhurra(props: React.SVGProps<SVGSVGElement>) {
    return (
        <svg
            viewBox="0 0 57 61"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            {...props}
        >
            <g filter="url(#4_-_r-jhurra_svg__filter0_iii_1_69)">
                <path
                    d="M56.535 50.687c0 1.245-.183 2.405-.55 3.479a9.305 9.305 0 01-1.72 3.003c-.758.928-1.832 1.648-3.224 2.16-1.367.538-2.99.806-4.87.806-3.858 0-7.556-2.148-11.096-6.445L16.545 31.24c-1.588-2.002-2.38-3.576-2.38-4.724.024-.952.39-1.587 1.098-1.904.708-.318 1.782-.379 3.222-.183L41.3 27.36c1.929.244 3.601.244 5.017 0 1.44-.27 2.552-.733 3.333-1.392.806-.66 1.392-1.404 1.758-2.234.366-.855.549-1.795.549-2.82 0-1.904-.732-3.467-2.197-4.687-1.44-1.246-3.491-1.868-6.153-1.868H20.133c-3.71 0-6.628.964-8.752 2.893-2.1 1.904-3.15 4.248-3.15 7.031 0 1.367.318 2.82.953 4.358l7.653 17.981c.757 1.782 1.221 3.467 1.392 5.054.195 1.562.085 2.99-.33 4.284-.415 1.27-1.27 2.283-2.563 3.04-1.294.757-2.966 1.135-5.017 1.135a8.212 8.212 0 01-3.516-.769c-1.098-.488-2.136-1.257-3.113-2.307-.976-1.074-1.757-2.576-2.343-4.504-.562-1.929-.843-4.175-.843-6.739 0-5.151.428-9.973 1.282-14.465.855-4.492 2.173-8.606 3.955-12.341 1.782-3.76 3.98-6.97 6.592-9.632 2.612-2.685 5.762-4.773 9.448-6.262C25.468 1.626 29.545.882 34.013.882c2.563 0 4.931.232 7.104.696 2.173.44 4.029 1.038 5.567 1.795a18.515 18.515 0 014.138 2.673c1.22 1.025 2.21 2.087 2.966 3.186a17.329 17.329 0 011.868 3.516c.488 1.22.83 2.368 1.025 3.442a17.59 17.59 0 01.293 3.15c0 1.391-.159 2.648-.476 3.771-.317 1.099-.903 2.234-1.758 3.406-.854 1.172-2.136 2.283-3.845 3.333-1.685 1.025-3.809 1.99-6.372 2.893L32.218 37.1c-2.148.708-3.222 1.806-3.222 3.296 0 .854.317 1.464.952 1.83.634.342 1.672.513 3.112.513l14.759.147c2.514.048 4.59.793 6.225 2.234 1.66 1.416 2.49 3.271 2.49 5.566z"
                    fill="#000"
                    fillOpacity={0.2}
                />
            </g>
            <defs>
                <filter
                    id="4_-_r-jhurra_svg__filter0_iii_1_69"
                    x={-3.496}
                    y={-1.118}
                    width={63.47}
                    height={70.253}
                    filterUnits="userSpaceOnUse"
                    colorInterpolationFilters="sRGB"
                >
                    <feFlood floodOpacity={0} result="BackgroundImageFix" />
                    <feBlend
                        in="SourceGraphic"
                        in2="BackgroundImageFix"
                        result="shape"
                    />
                    <feColorMatrix
                        in="SourceAlpha"
                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                        result="hardAlpha"
                    />
                    <feOffset dy={-2} />
                    <feGaussianBlur stdDeviation={5} />
                    <feComposite
                        in2="hardAlpha"
                        operator="arithmetic"
                        k2={-1}
                        k3={1}
                    />
                    <feColorMatrix values="0 0 0 0 0.0196078 0 0 0 0 0.843137 0 0 0 0 0.843137 0 0 0 1 0" />
                    <feBlend in2="shape" result="effect1_innerShadow_1_69" />
                    <feColorMatrix
                        in="SourceAlpha"
                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                        result="hardAlpha"
                    />
                    <feOffset dx={3} dy={10} />
                    <feGaussianBlur stdDeviation={4.5} />
                    <feComposite
                        in2="hardAlpha"
                        operator="arithmetic"
                        k2={-1}
                        k3={1}
                    />
                    <feColorMatrix values="0 0 0 0 0.568627 0 0 0 0 0.0705882 0 0 0 0 0.160235 0 0 0 1 0" />
                    <feBlend
                        in2="effect1_innerShadow_1_69"
                        result="effect2_innerShadow_1_69"
                    />
                    <feColorMatrix
                        in="SourceAlpha"
                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                        result="hardAlpha"
                    />
                    <feOffset dx={-4} />
                    <feGaussianBlur stdDeviation={2.5} />
                    <feComposite
                        in2="hardAlpha"
                        operator="arithmetic"
                        k2={-1}
                        k3={1}
                    />
                    <feColorMatrix values="0 0 0 0 1 0 0 0 0 0.546587 0 0 0 0 0.220833 0 0 0 1 0" />
                    <feBlend
                        in2="effect2_innerShadow_1_69"
                        result="effect3_innerShadow_1_69"
                    />
                </filter>
            </defs>
        </svg>
    );
}

export default Svg4RJhurra;
