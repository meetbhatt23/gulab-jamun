import * as React from "react";

function Svg4BGulabjamun(props: React.SVGProps<SVGSVGElement>) {
    return (
        <svg
            viewBox="0 0 60 59"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            {...props}
        >
            <g filter="url(#4_-_b-gulabjamun_svg__filter0_iii_2_165)">
                <path
                    d="M46.812 21.002l-6.3-.293c-1.049-.025-1.891.22-2.526.732-.635.513-.952 1.135-.952 1.868 0 .585.183 1.123.55 1.61.365.49.89.782 1.574.88l8.166 1.282c7.862 1.245 11.792 5.322 11.792 12.231 0 3.687-1.062 6.97-3.186 9.851-2.1 2.857-5.041 5.066-8.825 6.629-3.76 1.562-8.057 2.343-12.89 2.343-5.567 0-10.511-.732-14.833-2.197-4.32-1.465-7.836-3.516-10.546-6.152A26.337 26.337 0 012.72 40.52C1.353 36.98.67 33.075.67 28.802c0-9.131 2.514-16.211 7.543-21.24C13.267 2.532 20.225.018 29.087.018h9.448c7.203 0 12.354.769 15.454 2.307 3.101 1.538 4.651 4.236 4.651 8.093 0 3.247-1.196 5.86-3.589 7.837-2.392 1.977-5.139 2.893-8.24 2.747zM16.27 44.769l29.7-.22c2.612-.025 4.663-.61 6.152-1.758 1.513-1.147 2.27-2.515 2.27-4.101 0-2.295-.879-3.895-2.636-4.798-1.758-.928-4.212-1.074-7.361-.44l-27.063 5.384c-1.343.244-2.454.195-3.333-.146-.854-.342-1.391-.892-1.611-1.648-.562-1.856.232-3.43 2.38-4.725L37.51 18.841c1.562-.928 2.783-1.953 3.662-3.076.903-1.148 1.428-2.283 1.575-3.406a7.32 7.32 0 00-.22-3.26c-.317-1.05-.867-2.038-1.648-2.966a7.558 7.558 0 00-3.332-2.307c-1.319-.488-2.735-.635-4.248-.44-1.514.172-3.125.83-4.834 1.978-1.71 1.123-3.296 2.686-4.761 4.688L8.469 30.742c-1 1.417-1.599 2.918-1.794 4.505-.17 1.587.024 3.015.586 4.285.66 1.66 1.758 2.954 3.296 3.882 1.538.903 3.442 1.355 5.713 1.355z"
                    fill="#000"
                    fillOpacity={0.2}
                />
            </g>
            <defs>
                <filter
                    id="4_-_b-gulabjamun_svg__filter0_iii_2_165"
                    x={-3.331}
                    y={-1.982}
                    width={65.447}
                    height={69.118}
                    filterUnits="userSpaceOnUse"
                    colorInterpolationFilters="sRGB"
                >
                    <feFlood floodOpacity={0} result="BackgroundImageFix" />
                    <feBlend
                        in="SourceGraphic"
                        in2="BackgroundImageFix"
                        result="shape"
                    />
                    <feColorMatrix
                        in="SourceAlpha"
                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                        result="hardAlpha"
                    />
                    <feOffset dy={-2} />
                    <feGaussianBlur stdDeviation={5} />
                    <feComposite
                        in2="hardAlpha"
                        operator="arithmetic"
                        k2={-1}
                        k3={1}
                    />
                    <feColorMatrix values="0 0 0 0 0.0196078 0 0 0 0 0.843137 0 0 0 0 0.843137 0 0 0 1 0" />
                    <feBlend in2="shape" result="effect1_innerShadow_2_165" />
                    <feColorMatrix
                        in="SourceAlpha"
                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                        result="hardAlpha"
                    />
                    <feOffset dx={3} dy={10} />
                    <feGaussianBlur stdDeviation={4.5} />
                    <feComposite
                        in2="hardAlpha"
                        operator="arithmetic"
                        k2={-1}
                        k3={1}
                    />
                    <feColorMatrix values="0 0 0 0 0.568627 0 0 0 0 0.0705882 0 0 0 0 0.160235 0 0 0 1 0" />
                    <feBlend
                        in2="effect1_innerShadow_2_165"
                        result="effect2_innerShadow_2_165"
                    />
                    <feColorMatrix
                        in="SourceAlpha"
                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                        result="hardAlpha"
                    />
                    <feOffset dx={-4} />
                    <feGaussianBlur stdDeviation={2.5} />
                    <feComposite
                        in2="hardAlpha"
                        operator="arithmetic"
                        k2={-1}
                        k3={1}
                    />
                    <feColorMatrix values="0 0 0 0 1 0 0 0 0 0.546587 0 0 0 0 0.220833 0 0 0 1 0" />
                    <feBlend
                        in2="effect2_innerShadow_2_165"
                        result="effect3_innerShadow_2_165"
                    />
                </filter>
            </defs>
        </svg>
    );
}

export default Svg4BGulabjamun;
