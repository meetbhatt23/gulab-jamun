import * as React from "react";

function Svg2Lkalajam(props: React.SVGProps<SVGSVGElement>) {
    return (
        <svg
            viewBox="0 0 38 59"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            {...props}
        >
            <g filter="url(#2_-_lkalajam_svg__filter0_iii_2_141)">
                <path
                    d="M1.097 44.461l4.432-31.75C6.09 8.707 7.128 5.74 8.64 3.812c1.538-1.953 3.577-2.93 6.116-2.93 2.368 0 4.468.757 6.299 2.27 1.855 1.514 2.783 3.602 2.783 6.263 0 2.075-.683 4.407-2.05 6.995L5.967 45.816c-1.44 2.588-2.16 4.53-2.16 5.823 0 .903.304 1.672.915 2.307.635.61 1.306.916 2.014.916.732 0 1.624-.33 2.673-.989 1.05-.683 2.686-1.88 4.908-3.589l6.884-5.31c2.71-2.05 5.286-3.076 7.727-3.076 2.369 0 4.334.867 5.896 2.6 1.563 1.733 2.344 3.723 2.344 5.97 0 2.367-.928 4.382-2.783 6.042C32.53 58.17 29.759 59 26.073 59H11.168c-4.004 0-6.738-.769-8.203-2.307-1.44-1.538-2.16-4.273-2.16-8.203 0-1.343.097-2.686.292-4.029z"
                    fill="#000"
                    fillOpacity={0.2}
                />
            </g>
            <defs>
                <filter
                    id="2_-_lkalajam_svg__filter0_iii_2_141"
                    x={-3.196}
                    y={-1.118}
                    width={43.365}
                    height={69.118}
                    filterUnits="userSpaceOnUse"
                    colorInterpolationFilters="sRGB"
                >
                    <feFlood floodOpacity={0} result="BackgroundImageFix" />
                    <feBlend
                        in="SourceGraphic"
                        in2="BackgroundImageFix"
                        result="shape"
                    />
                    <feColorMatrix
                        in="SourceAlpha"
                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                        result="hardAlpha"
                    />
                    <feOffset dy={-2} />
                    <feGaussianBlur stdDeviation={5} />
                    <feComposite
                        in2="hardAlpha"
                        operator="arithmetic"
                        k2={-1}
                        k3={1}
                    />
                    <feColorMatrix values="0 0 0 0 0.0196078 0 0 0 0 0.843137 0 0 0 0 0.843137 0 0 0 1 0" />
                    <feBlend in2="shape" result="effect1_innerShadow_2_141" />
                    <feColorMatrix
                        in="SourceAlpha"
                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                        result="hardAlpha"
                    />
                    <feOffset dx={3} dy={10} />
                    <feGaussianBlur stdDeviation={4.5} />
                    <feComposite
                        in2="hardAlpha"
                        operator="arithmetic"
                        k2={-1}
                        k3={1}
                    />
                    <feColorMatrix values="0 0 0 0 0.568627 0 0 0 0 0.0705882 0 0 0 0 0.160235 0 0 0 1 0" />
                    <feBlend
                        in2="effect1_innerShadow_2_141"
                        result="effect2_innerShadow_2_141"
                    />
                    <feColorMatrix
                        in="SourceAlpha"
                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                        result="hardAlpha"
                    />
                    <feOffset dx={-4} />
                    <feGaussianBlur stdDeviation={2.5} />
                    <feComposite
                        in2="hardAlpha"
                        operator="arithmetic"
                        k2={-1}
                        k3={1}
                    />
                    <feColorMatrix values="0 0 0 0 1 0 0 0 0 0.546587 0 0 0 0 0.220833 0 0 0 1 0" />
                    <feBlend
                        in2="effect2_innerShadow_2_141"
                        result="effect3_innerShadow_2_141"
                    />
                </filter>
            </defs>
        </svg>
    );
}

export default Svg2Lkalajam;
