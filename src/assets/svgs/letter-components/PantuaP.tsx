import * as React from "react";

function SvgPantuaP(props: React.SVGProps<SVGSVGElement>) {
    return (
        <svg
            viewBox="0 0 54 59"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            {...props}
        >
            <g filter="url(#pantua-p_svg__filter0_iii_0_1)">
                <path
                    d="M14.488.582C4.738.582.163 6.807.163 15.207v32.325c0 9.45 4.725 11.175 9.9 11.175 5.775 0 9-2.25 9-7.5 0-3.675-1.275-6.525-4.35-10.05l-9.225-10.35c-1.95-2.175-2.325-5.475-.3-7.725 2.325-2.55 5.85-2.4 7.725 0l8.625 11.325c5.025 6.6 9.45 9.225 15.9 9.225 11.325 0 16.05-8.475 16.05-19.725 0-15.225-7.275-23.325-23.7-23.325h-15.3zm-3.3 15.525c0-1.575 1.2-2.325 3.15-3.15l13.875-5.7c4.05-1.65 5.4-1.275 8.1 2.55l11.4 16.35c3.9 5.55-.3 10.35-7.875 6.525l-26.025-13.2c-1.875-.975-2.625-2.1-2.625-3.375z"
                    fill="#000"
                    fillOpacity={0.2}
                />
            </g>
            <defs>
                <filter
                    id="pantua-p_svg__filter0_iii_0_1"
                    x={-3.837}
                    y={-5.2}
                    width={402.625}
                    height={72.907}
                    filterUnits="userSpaceOnUse"
                    colorInterpolationFilters="sRGB"
                >
                    <feFlood floodOpacity={0} result="BackgroundImageFix" />
                    <feBlend
                        in="SourceGraphic"
                        in2="BackgroundImageFix"
                        result="shape"
                    />
                    <feColorMatrix
                        in="SourceAlpha"
                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                        result="hardAlpha"
                    />
                    <feOffset dy={-2} />
                    <feGaussianBlur stdDeviation={5} />
                    <feComposite
                        in2="hardAlpha"
                        operator="arithmetic"
                        k2={-1}
                        k3={1}
                    />
                    <feColorMatrix values="0 0 0 0 0.0196078 0 0 0 0 0.843137 0 0 0 0 0.843137 0 0 0 1 0" />
                    <feBlend in2="shape" result="effect1_innerShadow_0_1" />
                    <feColorMatrix
                        in="SourceAlpha"
                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                        result="hardAlpha"
                    />
                    <feOffset dx={3} dy={10} />
                    <feGaussianBlur stdDeviation={4.5} />
                    <feComposite
                        in2="hardAlpha"
                        operator="arithmetic"
                        k2={-1}
                        k3={1}
                    />
                    <feColorMatrix values="0 0 0 0 0.568627 0 0 0 0 0.0705882 0 0 0 0 0.160235 0 0 0 1 0" />
                    <feBlend
                        in2="effect1_innerShadow_0_1"
                        result="effect2_innerShadow_0_1"
                    />
                    <feColorMatrix
                        in="SourceAlpha"
                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                        result="hardAlpha"
                    />
                    <feOffset dx={-4} />
                    <feGaussianBlur stdDeviation={2.5} />
                    <feComposite
                        in2="hardAlpha"
                        operator="arithmetic"
                        k2={-1}
                        k3={1}
                    />
                    <feColorMatrix values="0 0 0 0 1 0 0 0 0 0.546587 0 0 0 0 0.220833 0 0 0 1 0" />
                    <feBlend
                        in2="effect2_innerShadow_0_1"
                        result="effect3_innerShadow_0_1"
                    />
                </filter>
            </defs>
        </svg>
    );
}

export default SvgPantuaP;
