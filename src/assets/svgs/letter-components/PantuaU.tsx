import * as React from "react";

function SvgPantuaU(props: React.SVGProps<SVGSVGElement>) {
    return (
        <svg
            viewBox="0 0 51 61"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            {...props}
        >
            <g filter="url(#pantua-u_svg__filter0_iii_0_1)">
                <path
                    d="M41.429 41.6c1.725 1.8 3.525 5.25 1.275 8.4-2.25 3.15-6.225 3.6-12.675.3l-21.3-10.95c-4.425-2.25-5.475-6.3-2.4-10.875l9.225-13.65C19.154 9.5 17.204.875 9.179.875c-5.7 0-8.775 3.375-8.775 11.7v24.3c0 14.25 10.35 23.25 25.5 23.25 16.275 0 24.9-6.75 24.9-21.225V10.775c0-6.525-2.1-9.9-7.5-9.9-4.275 0-6.825 2.7-6.825 6.3 0 2.775 1.65 4.725 4.95 8.55l4.35 5.025c1.5 1.725.975 3 .15 3.825-.6.6-1.95 1.125-3.525-.15l-8.7-7.05c-7.425-6-13.575 1.125-7.575 7.65l15.3 16.575z"
                    fill="#000"
                    fillOpacity={0.2}
                />
            </g>
            <defs>
                <filter
                    id="pantua-u_svg__filter0_iii_0_1"
                    x={-276.837}
                    y={-1.2}
                    width={402.625}
                    height={72.907}
                    filterUnits="userSpaceOnUse"
                    colorInterpolationFilters="sRGB"
                >
                    <feFlood floodOpacity={0} result="BackgroundImageFix" />
                    <feBlend
                        in="SourceGraphic"
                        in2="BackgroundImageFix"
                        result="shape"
                    />
                    <feColorMatrix
                        in="SourceAlpha"
                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                        result="hardAlpha"
                    />
                    <feOffset dy={-2} />
                    <feGaussianBlur stdDeviation={5} />
                    <feComposite
                        in2="hardAlpha"
                        operator="arithmetic"
                        k2={-1}
                        k3={1}
                    />
                    <feColorMatrix values="0 0 0 0 0.0196078 0 0 0 0 0.843137 0 0 0 0 0.843137 0 0 0 1 0" />
                    <feBlend in2="shape" result="effect1_innerShadow_0_1" />
                    <feColorMatrix
                        in="SourceAlpha"
                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                        result="hardAlpha"
                    />
                    <feOffset dx={3} dy={10} />
                    <feGaussianBlur stdDeviation={4.5} />
                    <feComposite
                        in2="hardAlpha"
                        operator="arithmetic"
                        k2={-1}
                        k3={1}
                    />
                    <feColorMatrix values="0 0 0 0 0.568627 0 0 0 0 0.0705882 0 0 0 0 0.160235 0 0 0 1 0" />
                    <feBlend
                        in2="effect1_innerShadow_0_1"
                        result="effect2_innerShadow_0_1"
                    />
                    <feColorMatrix
                        in="SourceAlpha"
                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                        result="hardAlpha"
                    />
                    <feOffset dx={-4} />
                    <feGaussianBlur stdDeviation={2.5} />
                    <feComposite
                        in2="hardAlpha"
                        operator="arithmetic"
                        k2={-1}
                        k3={1}
                    />
                    <feColorMatrix values="0 0 0 0 1 0 0 0 0 0.546587 0 0 0 0 0.220833 0 0 0 1 0" />
                    <feBlend
                        in2="effect2_innerShadow_0_1"
                        result="effect3_innerShadow_0_1"
                    />
                </filter>
            </defs>
        </svg>
    );
}

export default SvgPantuaU;
