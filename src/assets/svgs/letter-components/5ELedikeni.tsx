import * as React from "react";

function Svg5ELedikeni(props: React.SVGProps<SVGSVGElement>) {
    return (
        <svg
            viewBox="0 0 55 61"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            {...props}
        >
            <g filter="url(#5_-_e-ledikeni_svg__filter0_iii_1_123)">
                <path
                    d="M14.395 51.2l32.996-2.93c4.614-.415 6.921.964 6.921 4.138 0 2.515-1.404 4.444-4.211 5.786-2.784 1.343-7.386 2.014-13.807 2.014-4.834 0-9.387-.476-13.66-1.428-4.272-.976-8.044-2.343-11.315-4.101-3.247-1.782-5.823-4.04-7.727-6.775C1.687 45.169.735 42.142.735 38.822c0-2.246.635-4.126 1.905-5.64 1.294-1.514 2.954-2.417 4.98-2.71 2.05-.317 4.334-.012 6.848.916l15.015 6.152c.903.39 1.623.513 2.16.366.562-.146.965-.403 1.21-.769.268-.366.268-.842 0-1.428-.245-.586-.721-1.123-1.43-1.611L9.086 18.57c-3.516-2.514-5.273-5.139-5.273-7.873 0-1.807.512-3.357 1.538-4.651 1.025-1.318 2.441-2.332 4.248-3.04 1.83-.732 3.833-1.257 6.005-1.574 2.173-.318 4.578-.476 7.215-.476 4.931-.025 9.875.33 14.831 1.062 4.98.708 8.582 1.574 10.804 2.6 2.075.976 3.588 2.136 4.54 3.479.977 1.318 1.417 3.015 1.319 5.09-.122 2.295-1.025 3.809-2.71 4.541-1.684.732-4.138.562-7.36-.513L13.588 6.485c-1.22-.415-2.258-.427-3.112-.036-.855.39-1.502.927-1.941 1.611-.44.66-.66 1.294-.66 1.904 0 1.05.281 1.93.843 2.637.561.684 1.66 1.294 3.296 1.831l29.223 9.302c5.103 1.611 7.654 4.419 7.654 8.423 0 2.002-.61 3.71-1.831 5.127-1.22 1.416-3.162 2.282-5.823 2.6L14.395 43.18c-4.2.537-6.299 1.965-6.299 4.284 0 1.148.537 2.1 1.612 2.857 1.098.757 2.66 1.05 4.687.879z"
                    fill="#000"
                    fillOpacity={0.2}
                />
            </g>
            <defs>
                <filter
                    id="5_-_e-ledikeni_svg__filter0_iii_1_123"
                    x={-3.265}
                    y={-1.081}
                    width={60.723}
                    height={70.29}
                    filterUnits="userSpaceOnUse"
                    colorInterpolationFilters="sRGB"
                >
                    <feFlood floodOpacity={0} result="BackgroundImageFix" />
                    <feBlend
                        in="SourceGraphic"
                        in2="BackgroundImageFix"
                        result="shape"
                    />
                    <feColorMatrix
                        in="SourceAlpha"
                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                        result="hardAlpha"
                    />
                    <feOffset dy={-2} />
                    <feGaussianBlur stdDeviation={5} />
                    <feComposite
                        in2="hardAlpha"
                        operator="arithmetic"
                        k2={-1}
                        k3={1}
                    />
                    <feColorMatrix values="0 0 0 0 0.0196078 0 0 0 0 0.843137 0 0 0 0 0.843137 0 0 0 1 0" />
                    <feBlend in2="shape" result="effect1_innerShadow_1_123" />
                    <feColorMatrix
                        in="SourceAlpha"
                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                        result="hardAlpha"
                    />
                    <feOffset dx={3} dy={10} />
                    <feGaussianBlur stdDeviation={4.5} />
                    <feComposite
                        in2="hardAlpha"
                        operator="arithmetic"
                        k2={-1}
                        k3={1}
                    />
                    <feColorMatrix values="0 0 0 0 0.568627 0 0 0 0 0.0705882 0 0 0 0 0.160235 0 0 0 1 0" />
                    <feBlend
                        in2="effect1_innerShadow_1_123"
                        result="effect2_innerShadow_1_123"
                    />
                    <feColorMatrix
                        in="SourceAlpha"
                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                        result="hardAlpha"
                    />
                    <feOffset dx={-4} />
                    <feGaussianBlur stdDeviation={2.5} />
                    <feComposite
                        in2="hardAlpha"
                        operator="arithmetic"
                        k2={-1}
                        k3={1}
                    />
                    <feColorMatrix values="0 0 0 0 1 0 0 0 0 0.546587 0 0 0 0 0.220833 0 0 0 1 0" />
                    <feBlend
                        in2="effect2_innerShadow_1_123"
                        result="effect3_innerShadow_1_123"
                    />
                </filter>
            </defs>
        </svg>
    );
}

export default Svg5ELedikeni;
