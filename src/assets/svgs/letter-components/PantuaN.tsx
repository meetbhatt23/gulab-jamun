import * as React from "react";

function SvgPantuaN(props: React.SVGProps<SVGSVGElement>) {
    return (
        <svg
            viewBox="0 0 58 61"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            {...props}
        >
            <g filter="url(#pantua-n_svg__filter0_iii_0_1)">
                <path
                    d="M10.117 60.125c8.1 0 9.6-7.05 6.225-15.375l-10.2-24.975c-.675-1.65-1.35-5.025-1.35-7.125 0-4.275 2.475-7.425 6.675-7.425 2.7 0 5.025 1.95 4.425 5.775l-.675 4.275c-.975 6.225 2.175 12.9 5.175 17.025l12.45 17.25c5.325 7.35 7.65 10.575 12.225 10.575 4.875 0 7.35-3.3 8.175-10.5L57.067 14.9C57.817 8 55.567.8 48.142.8c-6.3 0-8.25 4.875-8.25 8.7 0 1.8-.15 4.2 1.725 9l6.9 17.775c.975 2.475 1.575 4.2 1.575 5.55 0 1.65-.9 2.475-2.775 2.475-1.35 0-2.4-1.05-4.2-3.9L25.342 12.575c-4.5-7.05-7.8-11.7-13.725-11.7C3.592.875.067 4.625.067 21.5v20.25c0 10.875.225 18.375 10.05 18.375z"
                    fill="#000"
                    fillOpacity={0.2}
                />
            </g>
            <defs>
                <filter
                    id="pantua-n_svg__filter0_iii_0_1"
                    x={-137.837}
                    y={-1.2}
                    width={402.625}
                    height={72.907}
                    filterUnits="userSpaceOnUse"
                    colorInterpolationFilters="sRGB"
                >
                    <feFlood floodOpacity={0} result="BackgroundImageFix" />
                    <feBlend
                        in="SourceGraphic"
                        in2="BackgroundImageFix"
                        result="shape"
                    />
                    <feColorMatrix
                        in="SourceAlpha"
                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                        result="hardAlpha"
                    />
                    <feOffset dy={-2} />
                    <feGaussianBlur stdDeviation={5} />
                    <feComposite
                        in2="hardAlpha"
                        operator="arithmetic"
                        k2={-1}
                        k3={1}
                    />
                    <feColorMatrix values="0 0 0 0 0.0196078 0 0 0 0 0.843137 0 0 0 0 0.843137 0 0 0 1 0" />
                    <feBlend in2="shape" result="effect1_innerShadow_0_1" />
                    <feColorMatrix
                        in="SourceAlpha"
                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                        result="hardAlpha"
                    />
                    <feOffset dx={3} dy={10} />
                    <feGaussianBlur stdDeviation={4.5} />
                    <feComposite
                        in2="hardAlpha"
                        operator="arithmetic"
                        k2={-1}
                        k3={1}
                    />
                    <feColorMatrix values="0 0 0 0 0.568627 0 0 0 0 0.0705882 0 0 0 0 0.160235 0 0 0 1 0" />
                    <feBlend
                        in2="effect1_innerShadow_0_1"
                        result="effect2_innerShadow_0_1"
                    />
                    <feColorMatrix
                        in="SourceAlpha"
                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                        result="hardAlpha"
                    />
                    <feOffset dx={-4} />
                    <feGaussianBlur stdDeviation={2.5} />
                    <feComposite
                        in2="hardAlpha"
                        operator="arithmetic"
                        k2={-1}
                        k3={1}
                    />
                    <feColorMatrix values="0 0 0 0 1 0 0 0 0 0.546587 0 0 0 0 0.220833 0 0 0 1 0" />
                    <feBlend
                        in2="effect2_innerShadow_0_1"
                        result="effect3_innerShadow_0_1"
                    />
                </filter>
            </defs>
        </svg>
    );
}

export default SvgPantuaN;
