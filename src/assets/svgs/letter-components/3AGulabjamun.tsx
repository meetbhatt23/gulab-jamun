import * as React from "react";

function Svg3AGulabjamun(props: React.SVGProps<SVGSVGElement>) {
    return (
        <svg
            viewBox="0 0 57 61"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            {...props}
        >
            <g filter="url(#3_-_a-gulabjamun_svg__filter0_iii_2_164)">
                <path
                    d="M12.546 41.971l19.372-.44c2.368-.048 4.163-.402 5.383-1.061 1.246-.66 1.966-1.88 2.161-3.662l3.223-26.55c.244-2.051-.074-3.663-.952-4.835-.88-1.172-2.112-1.611-3.7-1.318-.732.146-1.464.537-2.196 1.172-.708.61-1.416 1.49-2.124 2.637-.684 1.123-1.16 1.94-1.429 2.453a74.99 74.99 0 00-1.208 2.344l-9.082 18.604c-.757 1.537-1.734 2.783-2.93 3.735-1.172.928-2.27 1.526-3.296 1.794a12.1 12.1 0 01-3.15.403c-2.22 0-4.113-.806-5.675-2.417-1.563-1.611-2.344-3.577-2.344-5.896 0-2.88 1.904-5.86 5.713-8.936L25.363 7.988c3.564-2.832 6.226-4.724 7.983-5.676C35.13 1.358 37.167.882 39.462.882c2.222 0 4.077.745 5.566 2.234 1.49 1.465 2.49 3.491 3.003 6.08l6.812 36.071c.83 4.395.513 7.971-.952 10.73-1.44 2.759-4.016 4.138-7.727 4.138-2.93 0-5.176-.805-6.739-2.417-1.562-1.611-2.343-3.637-2.343-6.079v-3.882c0-2.002-.476-3.198-1.428-3.588-.928-.391-2.051 0-3.37 1.171l-7.36 6.3c-.22.17-.745.622-1.575 1.354a96.387 96.387 0 01-1.904 1.648c-.44.366-1.1.879-1.978 1.538-.854.66-1.624 1.184-2.307 1.575-.66.366-1.416.757-2.27 1.172-.855.415-1.71.72-2.564.915-.855.196-1.71.293-2.564.293-1.147 0-2.246-.17-3.296-.512A9.945 9.945 0 013.5 58.048c-.903-.708-1.623-1.697-2.16-2.967-.538-1.293-.806-2.783-.806-4.467 0-2.832 1.11-4.956 3.332-6.372 2.222-1.44 5.115-2.198 8.68-2.27z"
                    fill="#000"
                    fillOpacity={0.2}
                />
            </g>
            <defs>
                <filter
                    id="3_-_a-gulabjamun_svg__filter0_iii_2_164"
                    x={-3.466}
                    y={-1.118}
                    width={62.554}
                    height={70.253}
                    filterUnits="userSpaceOnUse"
                    colorInterpolationFilters="sRGB"
                >
                    <feFlood floodOpacity={0} result="BackgroundImageFix" />
                    <feBlend
                        in="SourceGraphic"
                        in2="BackgroundImageFix"
                        result="shape"
                    />
                    <feColorMatrix
                        in="SourceAlpha"
                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                        result="hardAlpha"
                    />
                    <feOffset dy={-2} />
                    <feGaussianBlur stdDeviation={5} />
                    <feComposite
                        in2="hardAlpha"
                        operator="arithmetic"
                        k2={-1}
                        k3={1}
                    />
                    <feColorMatrix values="0 0 0 0 0.0196078 0 0 0 0 0.843137 0 0 0 0 0.843137 0 0 0 1 0" />
                    <feBlend in2="shape" result="effect1_innerShadow_2_164" />
                    <feColorMatrix
                        in="SourceAlpha"
                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                        result="hardAlpha"
                    />
                    <feOffset dx={3} dy={10} />
                    <feGaussianBlur stdDeviation={4.5} />
                    <feComposite
                        in2="hardAlpha"
                        operator="arithmetic"
                        k2={-1}
                        k3={1}
                    />
                    <feColorMatrix values="0 0 0 0 0.568627 0 0 0 0 0.0705882 0 0 0 0 0.160235 0 0 0 1 0" />
                    <feBlend
                        in2="effect1_innerShadow_2_164"
                        result="effect2_innerShadow_2_164"
                    />
                    <feColorMatrix
                        in="SourceAlpha"
                        values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                        result="hardAlpha"
                    />
                    <feOffset dx={-4} />
                    <feGaussianBlur stdDeviation={2.5} />
                    <feComposite
                        in2="hardAlpha"
                        operator="arithmetic"
                        k2={-1}
                        k3={1}
                    />
                    <feColorMatrix values="0 0 0 0 1 0 0 0 0 0.546587 0 0 0 0 0.220833 0 0 0 1 0" />
                    <feBlend
                        in2="effect2_innerShadow_2_164"
                        result="effect3_innerShadow_2_164"
                    />
                </filter>
            </defs>
        </svg>
    );
}

export default Svg3AGulabjamun;
