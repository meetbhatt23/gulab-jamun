import * as React from "react";

function SvgPantuaWhiteN(props: React.SVGProps<SVGSVGElement>) {
    return (
        <svg
            viewBox="0 0 36 37"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            {...props}
        >
            <path
                d="M6.483 36.441c4.86 0 5.76-4.23 3.735-9.225l-6.12-14.985c-.405-.99-.81-3.015-.81-4.275 0-2.565 1.485-4.455 4.005-4.455 1.62 0 3.015 1.17 2.655 3.465l-.405 2.565c-.585 3.735 1.305 7.74 3.105 10.215l7.47 10.35c3.195 4.41 4.59 6.345 7.335 6.345 2.925 0 4.41-1.98 4.905-6.3l2.295-20.835c.45-4.14-.9-8.46-5.355-8.46-3.78 0-4.95 2.925-4.95 5.22 0 1.08-.09 2.52 1.035 5.4l4.14 10.665c.585 1.485.945 2.52.945 3.33 0 .99-.54 1.485-1.665 1.485-.81 0-1.44-.63-2.52-2.34L15.618 7.911c-2.7-4.23-4.68-7.02-8.235-7.02-4.815 0-6.93 2.25-6.93 12.375v12.15c0 6.525.135 11.025 6.03 11.025z"
                fill="#fff"
            />
        </svg>
    );
}

export default SvgPantuaWhiteN;
