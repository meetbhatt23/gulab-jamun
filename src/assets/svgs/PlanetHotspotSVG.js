import * as React from "react";

function SvgPlanetHotspot(props) {
    return (
        <svg
            width={28}
            height={25}
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            {...props}
        >
            <mask id="planet-hotspot_svg__a" fill="#fff">
                <path d="M27.104 12.26c0 6.628-6.393 12.665-13.02 12.665C7.455 24.925.32 21.797.32 15.17.32 8.542 4.424.794 11.051.794c6.628 0 16.053 4.839 16.053 11.466z" />
            </mask>
            <path id="mainPath"
                d="M27.104 12.26c0 6.628-6.393 12.665-13.02 12.665C7.455 24.925.32 21.797.32 15.17.32 8.542 4.424.794 11.051.794c6.628 0 16.053 4.839 16.053 11.466z"
                stroke="#fff"
                strokeWidth={2}
                strokeDasharray="5 5"
                mask="url(#planet-hotspot_svg__a)"
            />
            <circle cx={12.657} cy={12.925} r={5.303} fill="#fff" />
        </svg>
    );
}

export default SvgPlanetHotspot;

