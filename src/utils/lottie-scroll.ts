import lottie from "lottie-web";
import { gsap } from "gsap";
import { ScrollTrigger } from "gsap/ScrollTrigger";
import { useEffect, useState } from "react";

export const useLottieScrollTrigger = (vars) => {
    const [loaded, setLoaded] = useState(false);
    gsap.registerPlugin(ScrollTrigger);

    useEffect(() => {
        let playhead = { frame: 0 },
            target = gsap.utils.toArray(vars.target)[0],
            speeds = { slow: "+=15000", medium: "+=10000", fast: "+=5000" },
            st = { trigger: target, pin: true, start: "top 1", end: speeds[vars.speed] || "+=1000", scrub: 1 },
            animation = lottie.loadAnimation({
                // @ts-ignore
                container: target,
                renderer: vars.renderer || "svg",
                loop: false,
                autoplay: false,
                path: vars.path
            });
        for (let p in vars) { // let users override the ScrollTrigger defaults
            st[p] = vars[p];
        }
        animation.addEventListener("DOMLoaded", function() {
            setLoaded(true);
            gsap.to(playhead, {
                duration: vars.duration || 0.5,
                delay: vars.delay || 0,
                frame: animation.totalFrames - 1,
                ease: vars.ease || "none",
                onUpdate: () => animation.goToAndStop(playhead.frame, true),
                // @ts-ignore
                scrollTrigger: st
            });
            // in case there are any other ScrollTriggers on the page and the loading of this Lottie asset caused layout changes
            ScrollTrigger.sort();
            ScrollTrigger.refresh();
        });

    }, []);

    return { loaded };
};
