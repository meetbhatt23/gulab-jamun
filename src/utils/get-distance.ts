export const getDistance = (x1, y1, x2, y2) => {
    const y = x2 - x1;
    const x = y2 - y1;

    return Math.sqrt(x * x + y * y);
};
