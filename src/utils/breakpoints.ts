export const breakpoints = {
    small: "@media screen and (min-width: 576px)",
    medium: "@media screen and (min-width: 768px)",
}
