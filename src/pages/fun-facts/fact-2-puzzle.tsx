import React, { useState } from "react";

import type { HeadFC } from "gatsby";
import { WithNextButton } from "../../components/WithNextButton";
import { FfFuel } from "../../components/FfFuel";

import { SEO } from "../../components/Seo";

export const Head: HeadFC = () => <SEO title="Trivia 2" />;

const FunFact2PuzzlePage = () => {
    const [showButton, setShowButton] = useState(false);
    return (
        <main>
            <WithNextButton
                to="/fun-facts/fact-2-trivia"
                type="default"
                showButton={showButton}
            >
                <FfFuel onComplete={() => setShowButton(true)} />
            </WithNextButton>
        </main>
    );
};

export default FunFact2PuzzlePage;

