import '@emotion/react'
import {IThemeInterface} from "./index";

declare module '@emotion/react' {
    export interface Theme extends IThemeInterface {
    }
}
