import { ThemeProvider } from "@emotion/react";
import { theme } from "../src/themes";
import "../src/assets/css/font-face-declarations.css";

export const parameters = {
    actions: { argTypesRegex: "^on[A-Z].*" },
    controls: {
        matchers: {
            color: /(background|color)$/i,
            date: /Date$/
        }
    },
    backgrounds: {
        default: "black",
        values: [
            {
                name: "black",
                value: "#000"
            }
        ]
    },
    layout: "centered"
};

export const decorators = [
    (Story) => (
        <ThemeProvider theme={theme}>
            <Story />
        </ThemeProvider>
    )
];
