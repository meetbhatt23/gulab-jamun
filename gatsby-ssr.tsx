import React from "react";
import { ThemeProvider } from "@emotion/react";
import { theme } from "./src/themes";
import { UnstyledLayout } from "./src/components/UnstyledLayout";
import { withPrefix } from "gatsby";

const woffFontUrls = [
    "VCROSDMono.woff",
    "CothamSans.woff",
    "Pilowlava-Regular.woff",
    "Sligoil-Micro.woff",
    "SpaceMono-Regular.woff",
];

const woff2FontUrls = [
    "VCROSDMono.woff2",
    "CothamSans.woff2",
    "Pilowlava-Regular.woff2",
    "Sligoil-Micro.woff2",
    "SpaceMono-Regular.woff2",
];

export const onRenderBody = ({ setHeadComponents, setHtmlAttributes }) => {
    setHtmlAttributes({ lang: "en", translate: "no" });
    setHeadComponents([
        ...woffFontUrls.map((fontUrl) =>
            <link
                rel="preload"
                href={withPrefix(`/fonts/${fontUrl}`)}
                as="font"
                type="font/woff"
                crossOrigin="anonymous"
                key={fontUrl.replace(".woff", "")}
            />
        ),
        ...woff2FontUrls.map((fontUrl) =>
            <link
                rel="preload"
                href={withPrefix(`/fonts/${fontUrl}`)}
                as="font"
                type="font/woff2"
                crossOrigin="anonymous"
                key={fontUrl.replace(".woff2", "")}
            />
        )
    ]);
};

export const wrapRootElement = ({ element }) => {
    return (
        <ThemeProvider theme={theme}>
            <UnstyledLayout>
                {element}
            </UnstyledLayout>
        </ThemeProvider>
    );
};

