// eslint-disable-next-line func-names
module.exports = function (plop) {
  // controller generator
  plop.setGenerator('component', {
    description: 'create a new component with stories',
    prompts: [{
      type: 'input',
      name: 'name',
      message: 'component name',
    }],
    actions: [{
      type: 'add',
      path: 'src/components/{{pascalCase name}}/index.tsx',
      templateFile: 'plop-templates/component.hbs',
    }, {
      type: 'add',
      path: 'src/components/{{pascalCase name}}/{{pascalCase name}}.stories.js',
      templateFile: 'plop-templates/storyjs.hbs',
    }],
  });
};
