import {GATSBY_PATH_PREFIX} from './global-constants.js'

document.addEventListener('DOMContentLoaded', function () {
	const homeLink = document.querySelector('.home-link');
	homeLink.setAttribute('href', `${GATSBY_PATH_PREFIX}/`);
	
	const aboutLink = document.querySelector('.about-link');
	aboutLink.setAttribute('href', `${GATSBY_PATH_PREFIX}/about.html`);
	
	const logoLink = document.querySelector('.logo-link');
	logoLink.setAttribute('href', `${GATSBY_PATH_PREFIX}/`);
}, false);
