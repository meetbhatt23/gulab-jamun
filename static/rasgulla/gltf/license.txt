Model Information:
* title:	Rasgulla_c4d
* source:	https://sketchfab.com/3d-models/rasgulla-c4d-7089fc6418c64a8fba8b802c78064b99
* author:	bhavyaarora (https://sketchfab.com/bhavyaarora)

Model License:
* license type:	CC-BY-4.0 (http://creativecommons.org/licenses/by/4.0/)
* requirements:	Author must be credited. Commercial use is allowed.

If you use this 3D model in your project be sure to copy paste this credit wherever you share it:
This work is based on "Rasgulla_c4d" (https://sketchfab.com/3d-models/rasgulla-c4d-7089fc6418c64a8fba8b802c78064b99) by bhavyaarora (https://sketchfab.com/bhavyaarora) licensed under CC-BY-4.0 (http://creativecommons.org/licenses/by/4.0/)