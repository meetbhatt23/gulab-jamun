let graphic;
const Y_AXIS = 1;
const X_AXIS = 2;
let b1, b2, c1, c2;

function preload () {
// 	cotham = loadFont('fonts/CothamSans.otf');
}

function setup () {
  var clientHeight = document.getElementById('about').clientHeight;
  var clientWidth = document.getElementById('about').clientWidth;
  var cnv = createCanvas(clientWidth, clientHeight);
  cnv.parent("about");
  background(0);
  graphic = createGraphics(1440, 1440)
  graphic.textSize(125)
  graphic.textLeading(120)
  graphic.textFont("misto")
  graphic.textAlign(CENTER, CENTER)
  graphic.fill("#fff")
  graphic.text('there’s \n always \n room for \n something \n sweet.', clientWidth/2, clientHeight/2)
  b1 = color(0);
  b2 = color(56,35,89);
  c1 = color(204, 102, 0);
  c2 = color(0, 102, 153);
}

function draw() {
//   background("#000")
// Background
  setGradient(0, 0, width , height, b1, b2, X_AXIS);
//   setGradient(width / 2, 0, width / 2, height, b2, b1, X_AXIS);  
  const tileSize = 125
  
  for(let x= 0; x < 48; x = x + 1) {
    for(let y = 0; y < 24; y = y + 1) {
      
      const wave = 0.05
      const distX = sin(frameCount * wave + x * .5 + y * .1) * 10
      const distY = sin(frameCount * wave + x * .5 + y * .75) * 5
      
      const sx = x * tileSize + distX
      const sy = y * tileSize + distY
      const sw = tileSize
     	const sh = tileSize
      
      const dx = x * tileSize
      const dy = y * tileSize
      const dw = tileSize
      const dh = tileSize
      
      image(graphic, dx, dy, dw, dh, sx, sy, sw, sh);

    }
  }
  
//   windowResized();
}

function setGradient(x, y, w, h, c1, c2, axis) {
  noFill();

  if (axis === Y_AXIS) {
    // Top to bottom gradient
    for (let i = y; i <= y + h; i++) {
      let inter = map(i, y, y + h, 0, 1);
      let c = lerpColor(c1, c2, inter);
      stroke(c);
      line(x, i, x + w, i);
    }
  } else if (axis === X_AXIS) {
    // Left to right gradient
    for (let i = x; i <= x + w; i++) {
      let inter = map(i, x, x + w, 0, 1);
      let c = lerpColor(c1, c2, inter);
      stroke(c);
      line(i, y, i, y + h);
    }
  }
}

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
  graphic.textSize(75);

}