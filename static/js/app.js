!(function () {
    "use strict";
    function t(e) {
        return (t =
            "function" == typeof Symbol && "symbol" == typeof Symbol.iterator
                ? function (t) {
                      return typeof t;
                  }
                : function (t) {
                      return t && "function" == typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype ? "symbol" : typeof t;
                  })(e);
    }
    function e(t, e) {
        if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function");
    }
    function i(t, e) {
        for (var i = 0; i < e.length; i++) {
            var n = e[i];
            (n.enumerable = n.enumerable || !1), (n.configurable = !0), "value" in n && (n.writable = !0), Object.defineProperty(t, n.key, n);
        }
    }
    function n(t, e, n) {
        return e && i(t.prototype, e), n && i(t, n), t;
    }
    function s(t, e, i) {
        return e in t ? Object.defineProperty(t, e, { value: i, enumerable: !0, configurable: !0, writable: !0 }) : (t[e] = i), t;
    }
    function o(t, e) {
        return (
            (function (t) {
                if (Array.isArray(t)) return t;
            })(t) ||
            (function (t, e) {
                if ("undefined" == typeof Symbol || !(Symbol.iterator in Object(t))) return;
                var i = [],
                    n = !0,
                    s = !1,
                    o = void 0;
                try {
                    for (var r, a = t[Symbol.iterator](); !(n = (r = a.next()).done) && (i.push(r.value), !e || i.length !== e); n = !0);
                } catch (t) {
                    (s = !0), (o = t);
                } finally {
                    try {
                        n || null == a.return || a.return();
                    } finally {
                        if (s) throw o;
                    }
                }
                return i;
            })(t, e) ||
            a(t, e) ||
            (function () {
                throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
            })()
        );
    }
    function r(t) {
        return (
            (function (t) {
                if (Array.isArray(t)) return l(t);
            })(t) ||
            (function (t) {
                if ("undefined" != typeof Symbol && Symbol.iterator in Object(t)) return Array.from(t);
            })(t) ||
            a(t) ||
            (function () {
                throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
            })()
        );
    }
    function a(t, e) {
        if (t) {
            if ("string" == typeof t) return l(t, e);
            var i = Object.prototype.toString.call(t).slice(8, -1);
            return "Object" === i && t.constructor && (i = t.constructor.name), "Map" === i || "Set" === i ? Array.from(t) : "Arguments" === i || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(i) ? l(t, e) : void 0;
        }
    }
    function l(t, e) {
        (null == e || e > t.length) && (e = t.length);
        for (var i = 0, n = new Array(e); i < e; i++) n[i] = t[i];
        return n;
    }
    var c = (function () {
            function i(t) {
                e(this, i), (this.mAttr = "data-" + t.dataName), (this.mCaptureEvents = ["mouseenter", "mouseleave"]), (this.el = t.el);
            }
            return (
                n(i, [
                    {
                        key: "mInit",
                        value: function (t) {
                            var e = this;
                            (this.modules = t),
                                (this.mCheckEventTarget = this.mCheckEventTarget.bind(this)),
                                this.events &&
                                    Object.keys(this.events).forEach(function (t) {
                                        return e.mAddEvent(t);
                                    });
                        },
                    },
                    {
                        key: "mUpdate",
                        value: function (t) {
                            this.modules = t;
                        },
                    },
                    {
                        key: "mDestroy",
                        value: function () {
                            var t = this;
                            this.events &&
                                Object.keys(this.events).forEach(function (e) {
                                    return t.mRemoveEvent(e);
                                });
                        },
                    },
                    {
                        key: "mAddEvent",
                        value: function (t) {
                            var e = !!this.mCaptureEvents.includes(t);
                            this.el.addEventListener(t, this.mCheckEventTarget, e);
                        },
                    },
                    {
                        key: "mRemoveEvent",
                        value: function (t) {
                            var e = !!this.mCaptureEvents.includes(t);
                            this.el.removeEventListener(t, this.mCheckEventTarget, e);
                        },
                    },
                    {
                        key: "mCheckEventTarget",
                        value: function (t) {
                            var e = this.events[t.type];
                            if ("string" == typeof e) this[e](t);
                            else {
                                var i = "[" + this.mAttr + "]",
                                    n = t.target;
                                if (this.mCaptureEvents.includes(t.type)) n.matches(i) && this.mCallEventMethod(t, e, n);
                                else for (; n && n !== document && (!n.matches(i) || "undefined" == this.mCallEventMethod(t, e, n)); ) n = n.parentNode;
                            }
                        },
                    },
                    {
                        key: "mCallEventMethod",
                        value: function (t, e, i) {
                            var n = i.getAttribute(this.mAttr);
                            if (e.hasOwnProperty(n)) {
                                var s = e[n];
                                t.hasOwnProperty("currentTarget") || Object.defineProperty(t, "currentTarget", { value: i }), t.hasOwnProperty("curTarget") || Object.defineProperty(t, "curTarget", { value: i }), this[s](t);
                            }
                        },
                    },
                    {
                        key: "$",
                        value: function (e, i) {
                            var n = [e.indexOf("."), e.indexOf("#"), e.indexOf("[")].filter(function (t) {
                                    return -1 != t;
                                }),
                                s = !1,
                                o = e,
                                a = "",
                                l = this.el;
                            return n.length && ((s = Math.min.apply(Math, r(n))), (o = e.slice(0, s)), (a = e.slice(s))), "object" == t(i) && (l = i), l.querySelectorAll("[" + this.mAttr + "=" + o + "]" + a);
                        },
                    },
                    {
                        key: "parent",
                        value: function (t, e) {
                            for (var i = "[" + this.mAttr + "=" + t + "]", n = e.parentNode; n && n !== document; ) {
                                if (n.matches(i)) return n;
                                n = n.parentNode;
                            }
                        },
                    },
                    {
                        key: "getData",
                        value: function (t, e) {
                            return (e || this.el).getAttribute(this.mAttr + "-" + t);
                        },
                    },
                    {
                        key: "setData",
                        value: function (t, e, i) {
                            return (i || this.el).setAttribute(this.mAttr + "-" + t, e);
                        },
                    },
                    {
                        key: "call",
                        value: function (t, e, i, n) {
                            var s = this;
                            e && !i && ((i = e), (e = !1)),
                                this.modules[i] &&
                                    (n
                                        ? this.modules[i][n] && this.modules[i][n][t](e)
                                        : Object.keys(this.modules[i]).forEach(function (n) {
                                              s.modules[i][n][t](e);
                                          }));
                        },
                    },
                    {
                        key: "on",
                        value: function (t, e, i, n) {
                            var s = this;
                            this.modules[e] &&
                                (n
                                    ? this.modules[e][n].el.addEventListener(t, function (t) {
                                          return i(t);
                                      })
                                    : Object.keys(this.modules[e]).forEach(function (n) {
                                          s.modules[e][n].el.addEventListener(t, function (t) {
                                              return i(t);
                                          });
                                      }));
                        },
                    },
                    { key: "init", value: function () {} },
                    { key: "destroy", value: function () {} },
                ]),
                i
            );
        })(),
        h = (function () {
            function t(i) {
                e(this, t), this.app, (this.modules = i.modules), (this.currentModules = {}), (this.activeModules = {}), (this.newModules = {}), (this.moduleId = 0);
            }
            return (
                n(t, [
                    {
                        key: "init",
                        value: function (t, e) {
                            var i = this,
                                n = (e || document).querySelectorAll("*");
                            t && !this.app && (this.app = t),
                                (this.activeModules.app = { app: this.app }),
                                n.forEach(function (t) {
                                    Array.from(t.attributes).forEach(function (n) {
                                        if (n.name.startsWith("data-module")) {
                                            var s = !1,
                                                o = n.name.split("-").splice(2),
                                                r = i.toCamel(o);
                                            if ((i.modules[r] ? (s = !0) : i.modules[i.toUpper(r)] && ((r = i.toUpper(r)), (s = !0)), s)) {
                                                var a = { el: t, name: r, dataName: o.join("-") },
                                                    l = new i.modules[r](a),
                                                    c = n.value;
                                                c || (i.moduleId++, (c = "m" + i.moduleId), t.setAttribute(n.name, c)), i.addActiveModule(r, c, l);
                                                var h = r + "-" + c;
                                                e ? (i.newModules[h] = l) : (i.currentModules[h] = l);
                                            }
                                        }
                                    });
                                }),
                                Object.entries(this.currentModules).forEach(function (t) {
                                    var n = o(t, 2),
                                        s = n[0],
                                        r = n[1];
                                    if (e) {
                                        var a = s.split("-"),
                                            l = a.shift(),
                                            c = a.pop();
                                        i.addActiveModule(l, c, r);
                                    } else i.initModule(r);
                                });
                        },
                    },
                    {
                        key: "initModule",
                        value: function (t) {
                            t.mInit(this.activeModules), t.init();
                        },
                    },
                    {
                        key: "addActiveModule",
                        value: function (t, e, i) {
                            this.activeModules[t] ? Object.assign(this.activeModules[t], s({}, e, i)) : (this.activeModules[t] = s({}, e, i));
                        },
                    },
                    {
                        key: "update",
                        value: function (t) {
                            var e = this;
                            this.init(this.app, t),
                                Object.entries(this.currentModules).forEach(function (t) {
                                    var i = o(t, 2);
                                    i[0];
                                    i[1].mUpdate(e.activeModules);
                                }),
                                Object.entries(this.newModules).forEach(function (t) {
                                    var i = o(t, 2),
                                        n = (i[0], i[1]);
                                    e.initModule(n);
                                }),
                                Object.assign(this.currentModules, this.newModules);
                        },
                    },
                    {
                        key: "destroy",
                        value: function (t) {
                            t ? this.destroyScope(t) : this.destroyModules();
                        },
                    },
                    {
                        key: "destroyScope",
                        value: function (t) {
                            var e = this;
                            t.querySelectorAll("*").forEach(function (t) {
                                Array.from(t.attributes).forEach(function (t) {
                                    if (t.name.startsWith("data-module")) {
                                        var i = t.value,
                                            n = t.name.split("-").splice(2),
                                            s = e.toCamel(n) + "-" + i,
                                            o = !1;
                                        e.currentModules[s] ? (o = !0) : e.currentModules[e.toUpper(s)] && ((s = e.toUpper(s)), (o = !0)), o && (e.destroyModule(e.currentModules[s]), delete e.currentModules[s]);
                                    }
                                });
                            }),
                                (this.activeModules = {}),
                                (this.newModules = {});
                        },
                    },
                    {
                        key: "destroyModules",
                        value: function () {
                            var t = this;
                            Object.entries(this.currentModules).forEach(function (e) {
                                var i = o(e, 2),
                                    n = (i[0], i[1]);
                                t.destroyModule(n);
                            }),
                                (this.currentModules = []);
                        },
                    },
                    {
                        key: "destroyModule",
                        value: function (t) {
                            t.mDestroy(), t.destroy();
                        },
                    },
                    {
                        key: "toCamel",
                        value: function (t) {
                            var e = this;
                            return t.reduce(function (t, i) {
                                return t + e.toUpper(i);
                            });
                        },
                    },
                    {
                        key: "toUpper",
                        value: function (t) {
                            return t.charAt(0).toUpperCase() + t.slice(1);
                        },
                    },
                ]),
                t
            );
        })();
    function u(t, e) {
        if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function");
    }
    function d(t, e) {
        for (var i = 0; i < e.length; i++) {
            var n = e[i];
            (n.enumerable = n.enumerable || !1), (n.configurable = !0), "value" in n && (n.writable = !0), Object.defineProperty(t, n.key, n);
        }
    }
    function f(t, e, i) {
        return e && d(t.prototype, e), i && d(t, i), t;
    }
    function p(t, e, i) {
        return e in t ? Object.defineProperty(t, e, { value: i, enumerable: !0, configurable: !0, writable: !0 }) : (t[e] = i), t;
    }
    function v(t, e) {
        var i = Object.keys(t);
        if (Object.getOwnPropertySymbols) {
            var n = Object.getOwnPropertySymbols(t);
            e &&
                (n = n.filter(function (e) {
                    return Object.getOwnPropertyDescriptor(t, e).enumerable;
                })),
                i.push.apply(i, n);
        }
        return i;
    }
    function m(t) {
        for (var e = 1; e < arguments.length; e++) {
            var i = null != arguments[e] ? arguments[e] : {};
            e % 2
                ? v(Object(i), !0).forEach(function (e) {
                      p(t, e, i[e]);
                  })
                : Object.getOwnPropertyDescriptors
                ? Object.defineProperties(t, Object.getOwnPropertyDescriptors(i))
                : v(Object(i)).forEach(function (e) {
                      Object.defineProperty(t, e, Object.getOwnPropertyDescriptor(i, e));
                  });
        }
        return t;
    }
    function g(t, e) {
        if ("function" != typeof e && null !== e) throw new TypeError("Super expression must either be null or a function");
        (t.prototype = Object.create(e && e.prototype, { constructor: { value: t, writable: !0, configurable: !0 } })), e && w(t, e);
    }
    function y(t) {
        return (y = Object.setPrototypeOf
            ? Object.getPrototypeOf
            : function (t) {
                  return t.__proto__ || Object.getPrototypeOf(t);
              })(t);
    }
    function w(t, e) {
        return (w =
            Object.setPrototypeOf ||
            function (t, e) {
                return (t.__proto__ = e), t;
            })(t, e);
    }
    function b(t, e) {
        return !e || ("object" != typeof e && "function" != typeof e)
            ? (function (t) {
                  if (void 0 === t) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
                  return t;
              })(t)
            : e;
    }
    function k(t) {
        var e = (function () {
            if ("undefined" == typeof Reflect || !Reflect.construct) return !1;
            if (Reflect.construct.sham) return !1;
            if ("function" == typeof Proxy) return !0;
            try {
                return Date.prototype.toString.call(Reflect.construct(Date, [], function () {})), !0;
            } catch (t) {
                return !1;
            }
        })();
        return function () {
            var i,
                n = y(t);
            if (e) {
                var s = y(this).constructor;
                i = Reflect.construct(n, arguments, s);
            } else i = n.apply(this, arguments);
            return b(this, i);
        };
    }
    function x(t, e, i) {
        return (x =
            "undefined" != typeof Reflect && Reflect.get
                ? Reflect.get
                : function (t, e, i) {
                      var n = (function (t, e) {
                          for (; !Object.prototype.hasOwnProperty.call(t, e) && null !== (t = y(t)); );
                          return t;
                      })(t, e);
                      if (n) {
                          var s = Object.getOwnPropertyDescriptor(n, e);
                          return s.get ? s.get.call(i) : s.value;
                      }
                  })(t, e, i || t);
    }
    function E(t, e) {
        return (
            (function (t) {
                if (Array.isArray(t)) return t;
            })(t) ||
            (function (t, e) {
                if ("undefined" == typeof Symbol || !(Symbol.iterator in Object(t))) return;
                var i = [],
                    n = !0,
                    s = !1,
                    o = void 0;
                try {
                    for (var r, a = t[Symbol.iterator](); !(n = (r = a.next()).done) && (i.push(r.value), !e || i.length !== e); n = !0);
                } catch (t) {
                    (s = !0), (o = t);
                } finally {
                    try {
                        n || null == a.return || a.return();
                    } finally {
                        if (s) throw o;
                    }
                }
                return i;
            })(t, e) ||
            T(t, e) ||
            (function () {
                throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
            })()
        );
    }
    function T(t, e) {
        if (t) {
            if ("string" == typeof t) return S(t, e);
            var i = Object.prototype.toString.call(t).slice(8, -1);
            return "Object" === i && t.constructor && (i = t.constructor.name), "Map" === i || "Set" === i ? Array.from(t) : "Arguments" === i || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(i) ? S(t, e) : void 0;
        }
    }
    function S(t, e) {
        (null == e || e > t.length) && (e = t.length);
        for (var i = 0, n = new Array(e); i < e; i++) n[i] = t[i];
        return n;
    }
    function A(t, e) {
        var i;
        if ("undefined" == typeof Symbol || null == t[Symbol.iterator]) {
            if (Array.isArray(t) || (i = T(t)) || (e && t && "number" == typeof t.length)) {
                i && (t = i);
                var n = 0,
                    s = function () {};
                return {
                    s: s,
                    n: function () {
                        return n >= t.length ? { done: !0 } : { done: !1, value: t[n++] };
                    },
                    e: function (t) {
                        throw t;
                    },
                    f: s,
                };
            }
            throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
        }
        var o,
            r = !0,
            a = !1;
        return {
            s: function () {
                i = t[Symbol.iterator]();
            },
            n: function () {
                var t = i.next();
                return (r = t.done), t;
            },
            e: function (t) {
                (a = !0), (o = t);
            },
            f: function () {
                try {
                    r || null == i.return || i.return();
                } finally {
                    if (a) throw o;
                }
            },
        };
    }
    var C = (function () {
            function t(e) {
                u(this, t),
                    (this.defaults = {
                        name: "load",
                        loadingClass: "is-loading",
                        loadedClass: "is-loaded",
                        readyClass: "is-ready",
                        transitionsPrefix: "is-",
                        transitionsHistory: !0,
                        enterDelay: 0,
                        exitDelay: 0,
                        loadedDelay: 0,
                        isLoaded: !1,
                        isEntered: !1,
                        isUrl: !1,
                        transitionContainer: null,
                    }),
                    Object.assign(this, this.defaults, e),
                    (this.options = e),
                    (this.namespace = "modular"),
                    (this.html = document.documentElement),
                    (this.href = window.location.href),
                    (this.container = "data-" + this.name + "-container"),
                    (this.subContainer = !1),
                    (this.prevTransition = null),
                    (this.loadAttributes = ["src", "srcset", "style", "href"]),
                    (this.isInserted = !1),
                    (this.isLoading = !1),
                    (this.enterTimeout = !1),
                    (this.controller = new AbortController()),
                    (this.classContainer = this.html),
                    (this.isChrome = -1 != navigator.userAgent.indexOf("Chrome")),
                    this.init();
            }
            return (
                f(t, [
                    {
                        key: "init",
                        value: function () {
                            var t = this;
                            window.addEventListener(
                                "popstate",
                                function (e) {
                                    return t.checkState(e);
                                },
                                !1
                            ),
                                this.html.addEventListener(
                                    "click",
                                    function (e) {
                                        return t.checkClick(e);
                                    },
                                    !1
                                ),
                                this.loadEls(document);
                        },
                    },
                    {
                        key: "checkClick",
                        value: function (t) {
                            if (!t.ctrlKey && !t.metaKey)
                                for (var e = t.target; e && e !== document; ) {
                                    if (e.matches("a") && null == e.getAttribute("download")) {
                                        var i = e.getAttribute("href");
                                        i.startsWith("#") ||
                                            i.startsWith("mailto:") ||
                                            i.startsWith("tel:") ||
                                            (t.preventDefault(),
                                            this.reset(),
                                            "survey" === document.getElementsByTagName("html")[0].getAttribute("data-template") && "true" != document.getElementsByTagName("html")[0].getAttribute("data-survey-valid") && (this.blocked = !0),
                                            this.getClickOptions(e));
                                        break;
                                    }
                                    e = e.parentNode;
                                }
                        },
                    },
                    {
                        key: "checkState",
                        value: function () {
                            this.reset(), this.getStateOptions();
                        },
                    },
                    {
                        key: "reset",
                        value: function () {
                            this.isLoading && (this.controller.abort(), (this.isLoading = !1), (this.controller = new AbortController())),
                                window.clearTimeout(this.enterTimeout),
                                this.isInserted && this.removeContainer(),
                                (this.classContainer = this.html),
                                Object.assign(this, this.defaults, this.options);
                        },
                    },
                    {
                        key: "getClickOptions",
                        value: function (t) {
                            (this.transition = t.getAttribute("data-" + this.name)), (this.isUrl = t.getAttribute("data-" + this.name + "-url"));
                            var e = t.getAttribute("href"),
                                i = t.getAttribute("target");
                            (this.link = t), "_blank" != i ? ("false" != this.transition ? this.setOptions(e, !0) : (window.location = e)) : window.open(e, "_blank");
                        },
                    },
                    {
                        key: "getStateOptions",
                        value: function () {
                            this.transitionsHistory ? (this.transition = history.state) : (this.transition = !1);
                            var t = window.location.href;
                            this.setOptions(t);
                        },
                    },
                    {
                        key: "goTo",
                        value: function (t, e, i) {
                            this.reset(), (this.transition = e), (this.isUrl = i), this.setOptions(t, !0);
                        },
                    },
                    {
                        key: "setOptions",
                        value: function (t, e) {
                            var i,
                                n = "[" + this.container + "]";
                            if (
                                (this.transition &&
                                    "true" != this.transition &&
                                    ((this.transitionContainer = "[" + this.container + '="' + this.transition + '"]'),
                                    (this.loadingClass = this.transitions[this.transition].loadingClass || this.loadingClass),
                                    (this.loadedClass = this.transitions[this.transition].loadedClass || this.loadedClass),
                                    (this.readyClass = this.transitions[this.transition].readyClass || this.readyClass),
                                    (this.transitionsPrefix = this.transitions[this.transition].transitionsPrefix || this.transitionsPrefix),
                                    (this.enterDelay = this.transitions[this.transition].enterDelay || this.enterDelay),
                                    (this.exitDelay = this.transitions[this.transition].exitDelay || this.exitDelay),
                                    (this.loadedDelay = this.transitions[this.transition].loadedDelay || this.loadedDelay),
                                    (i = document.querySelector(this.transitionContainer))),
                                i
                                    ? ((n = this.transitionContainer),
                                      (this.oldContainer = i),
                                      (this.classContainer = this.oldContainer.parentNode),
                                      this.subContainer || history.replaceState(this.transition, null, this.href),
                                      (this.subContainer = !0))
                                    : ((this.oldContainer = document.querySelector(n)), this.subContainer && history.replaceState(this.prevTransition, null, this.href), (this.subContainer = !1)),
                                (this.href = t),
                                (this.parentContainer = this.oldContainer.parentNode),
                                "" === this.isUrl || (null != this.isUrl && "false" != this.isUrl && 0 != this.isUrl))
                            )
                                history.pushState(this.transition, null, t);
                            else {
                                if ((this.oldContainer.classList.add("is-old"), this.setLoading(), this.startEnterDelay(), this.blocked)) return;
                                this.loadHref(t, n, e);
                            }
                        },
                    },
                    {
                        key: "setLoading",
                        value: function () {
                            this.blocked ||
                                (this.classContainer.classList.remove(this.loadedClass, this.readyClass),
                                this.classContainer.classList.add(this.loadingClass),
                                this.classContainer.classList.remove(this.transitionsPrefix + this.prevTransition),
                                this.transition && this.classContainer.classList.add(this.transitionsPrefix + this.transition),
                                this.subContainer || (this.prevTransition = this.transition));
                            var t = new Event(this.namespace + "loading");
                            window.dispatchEvent(t);
                        },
                    },
                    {
                        key: "startEnterDelay",
                        value: function () {
                            var t = this;
                            this.enterTimeout = window.setTimeout(function () {
                                (t.isEntered = !0), t.isLoaded && t.transitionContainers();
                            }, this.enterDelay);
                        },
                    },
                    {
                        key: "loadHref",
                        value: function (t, e, i) {
                            var n = this;
                            this.isLoading = !0;
                            var s = this.controller.signal;
                            fetch(t, { signal: s })
                                .then(function (t) {
                                    return t.text();
                                })
                                .then(function (s) {
                                    i && history.pushState(n.transition, null, t);
                                    var o = new DOMParser();
                                    (n.data = o.parseFromString(s, "text/html")),
                                        (n.newContainer = n.data.querySelector(e)),
                                        n.newContainer.classList.add("is-new"),
                                        (n.parentNewContainer = n.newContainer.parentNode),
                                        n.hideContainer(),
                                        n.parentContainer.insertBefore(n.newContainer, n.oldContainer),
                                        (n.isInserted = !0),
                                        n.setSvgs(),
                                        (n.isLoaded = !0),
                                        n.isEntered && n.transitionContainers(),
                                        n.loadEls(n.newContainer),
                                        (n.isLoading = !1);
                                })
                                .catch(function (e) {
                                    window.location = t;
                                });
                        },
                    },
                    {
                        key: "transitionContainers",
                        value: function () {
                            var t = this;
                            this.setAttributes(),
                                this.showContainer(),
                                this.setLoaded(),
                                setTimeout(function () {
                                    t.removeContainer(), t.setReady();
                                }, this.exitDelay);
                        },
                    },
                    {
                        key: "setSvgs",
                        value: function () {
                            if (this.isChrome) {
                                var t = this.newContainer.querySelectorAll("use");
                                t.length &&
                                    t.forEach(function (t) {
                                        var e = t.getAttribute("xlink:href");
                                        if (e) t.parentNode.innerHTML = '<use xlink:href="' + e + '"></use>';
                                        else {
                                            var i = t.getAttribute("href");
                                            i && (t.parentNode.innerHTML = '<use href="' + i + '"></use>');
                                        }
                                    });
                            }
                        },
                    },
                    {
                        key: "setAttributes",
                        value: function () {
                            var t,
                                e,
                                i = this,
                                n = this.data.getElementsByTagName("title")[0],
                                s = this.data.head.querySelector('meta[name="description"]'),
                                o = document.head.querySelector('meta[name="description"]');
                            this.subContainer ? ((e = this.parentNewContainer), (t = document.querySelector(this.transitionContainer).parentNode)) : ((e = this.data.querySelector("html")), (t = document.querySelector("html")));
                            var r = Object.assign({}, e.dataset);
                            n && (document.title = n.innerText),
                                o && s && o.setAttribute("content", s.getAttribute("content")),
                                r &&
                                    Object.entries(r).forEach(function (e) {
                                        var n = E(e, 2),
                                            s = n[0],
                                            o = n[1];
                                        t.setAttribute("data-" + i.toDash(s), o);
                                    });
                        },
                    },
                    {
                        key: "toDash",
                        value: function (t) {
                            return t
                                .split(/(?=[A-Z])/)
                                .join("-")
                                .toLowerCase();
                        },
                    },
                    {
                        key: "hideContainer",
                        value: function () {
                            (this.newContainer.style.visibility = "hidden"), (this.newContainer.style.height = 0), (this.newContainer.style.overflow = "hidden");
                        },
                    },
                    {
                        key: "showContainer",
                        value: function () {
                            (this.newContainer.style.visibility = ""), (this.newContainer.style.height = ""), (this.newContainer.style.overflow = "");
                        },
                    },
                    {
                        key: "loadEls",
                        value: function (t) {
                            var e = this,
                                i = [];
                            this.loadAttributes.forEach(function (n) {
                                var s = "data-" + e.name + "-" + n,
                                    o = t.querySelectorAll("[" + s + "]");
                                o.length &&
                                    o.forEach(function (t) {
                                        var e = t.getAttribute(s);
                                        if ((t.setAttribute(n, e), "src" == n || "srcset" == n)) {
                                            var o = new Promise(function (e) {
                                                t.onload = function () {
                                                    return e(t);
                                                };
                                            });
                                            i.push(o);
                                        }
                                    });
                            }),
                                Promise.all(i).then(function (t) {
                                    var i = new Event(e.namespace + "images");
                                    window.dispatchEvent(i);
                                });
                        },
                    },
                    {
                        key: "setLoaded",
                        value: function () {
                            var t = this;
                            this.classContainer.classList.remove(this.loadingClass),
                                setTimeout(function () {
                                    t.classContainer.classList.add(t.loadedClass);
                                }, this.loadedDelay);
                            var e = new Event(this.namespace + "loaded");
                            window.dispatchEvent(e);
                        },
                    },
                    {
                        key: "removeContainer",
                        value: function () {
                            this.parentContainer.removeChild(this.oldContainer), this.newContainer.classList.remove("is-new"), (this.isInserted = !1);
                        },
                    },
                    {
                        key: "setReady",
                        value: function () {
                            this.classContainer.classList.add(this.readyClass);
                            var t = new Event(this.namespace + "ready");
                            window.dispatchEvent(t);
                        },
                    },
                    {
                        key: "on",
                        value: function (t, e) {
                            var i = this;
                            window.addEventListener(
                                this.namespace + t,
                                function () {
                                    switch (t) {
                                        case "loading":
                                            return e(i.transition, i.oldContainer, i.link);
                                        case "loaded":
                                            return e(i.transition, i.oldContainer, i.newContainer);
                                        case "ready":
                                            return e(i.transition, i.newContainer);
                                        default:
                                            return e();
                                    }
                                },
                                !1
                            );
                        },
                    },
                ]),
                t
            );
        })(),
        M = document.documentElement,
        L =
            (M.getAttribute("data-debug"),
            (function (t) {
                g(i, t);
                var e = k(i);
                function i(t) {
                    return u(this, i), e.call(this, t);
                }
                return (
                    f(i, [
                        {
                            key: "init",
                            value: function () {
                                var t = this;
                                (this.load = new C({ enterDelay: 800, transitions: { detailTransition: {}, langTransition: {} } })),
                                    this.load.on("loading", function (e, i, n) {
                                        if (
                                            "true" != document.getElementsByTagName("html")[0].getAttribute("data-survey-valid") &&
                                            "survey" === M.getAttribute("data-template") &&
                                            void 0 !== n &&
                                            ("false" === M.getAttribute("data-confirm") || null === M.getAttribute("data-confirm"))
                                        )
                                            return t.confirmTransition(n.getAttribute("href")), void (t.load.blocked = !0);
                                        M.classList.remove("has-scrolled"),
                                            M.classList.remove("has-footer-widget-open"),
                                            M.classList.remove("has-widget-user-open"),
                                            M.classList.remove("-footer"),
                                            M.classList.remove("has-menu-tools-open"),
                                            M.setAttribute("data-ui", ""),
                                            "langTransition" === e &&
                                                (M.classList.add("has-menu-lang-open-is-closing"),
                                                setTimeout(function () {
                                                    M.classList.remove("has-menu-lang-open"), M.classList.remove("has-menu-lang-open-is-closing");
                                                }, 600)),
                                            setTimeout(function () {
                                                M.classList.remove("is-animated"), M.classList.remove("has-nav-open");
                                            }, 200),
                                            (t.load.blocked = !1);
                                    }),
                                    this.load.on("loaded", function (e, i, n) {
                                        t.call("destroy", i, "app"),
                                            t.call("update", n, "app"),
                                            t.call("destroyEvents", "Cursor"),
                                            t.call("initEvents", "Cursor"),
                                            setTimeout(function () {
                                                M.classList.add("is-animated");
                                            }, 1200),
                                            M.setAttribute("data-theme", n.getAttribute("data-theme")),
                                            M.setAttribute("data-container-size", n.getAttribute("data-container-size"));
                                    });
                            },
                        },
                        {
                            key: "confirmTransition",
                            value: function (t) {
                                confirm(this.getData("confirm-text")) ? (M.setAttribute("data-confirm", "true"), (this.load.blocked = !1), (window.location = t)) : M.setAttribute("data-confirm", "false");
                            },
                        },
                        {
                            key: "goto",
                            value: function (t) {
                                this.load.goTo(t);
                            },
                        },
                    ]),
                    i
                );
            })(c));
    function O(t, e) {
        if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function");
    }
    function P(t, e) {
        for (var i = 0; i < e.length; i++) {
            var n = e[i];
            (n.enumerable = n.enumerable || !1), (n.configurable = !0), "value" in n && (n.writable = !0), Object.defineProperty(t, n.key, n);
        }
    }
    function _(t, e, i) {
        return e && P(t.prototype, e), i && P(t, i), t;
    }
    function R(t, e, i) {
        return e in t ? Object.defineProperty(t, e, { value: i, enumerable: !0, configurable: !0, writable: !0 }) : (t[e] = i), t;
    }
    function D(t, e) {
        var i = Object.keys(t);
        if (Object.getOwnPropertySymbols) {
            var n = Object.getOwnPropertySymbols(t);
            e &&
                (n = n.filter(function (e) {
                    return Object.getOwnPropertyDescriptor(t, e).enumerable;
                })),
                i.push.apply(i, n);
        }
        return i;
    }
    function B(t) {
        for (var e = 1; e < arguments.length; e++) {
            var i = null != arguments[e] ? arguments[e] : {};
            e % 2
                ? D(Object(i), !0).forEach(function (e) {
                      R(t, e, i[e]);
                  })
                : Object.getOwnPropertyDescriptors
                ? Object.defineProperties(t, Object.getOwnPropertyDescriptors(i))
                : D(Object(i)).forEach(function (e) {
                      Object.defineProperty(t, e, Object.getOwnPropertyDescriptor(i, e));
                  });
        }
        return t;
    }
    function I(t, e) {
        if ("function" != typeof e && null !== e) throw new TypeError("Super expression must either be null or a function");
        (t.prototype = Object.create(e && e.prototype, { constructor: { value: t, writable: !0, configurable: !0 } })), e && W(t, e);
    }
    function j(t) {
        return (j = Object.setPrototypeOf
            ? Object.getPrototypeOf
            : function (t) {
                  return t.__proto__ || Object.getPrototypeOf(t);
              })(t);
    }
    function W(t, e) {
        return (W =
            Object.setPrototypeOf ||
            function (t, e) {
                return (t.__proto__ = e), t;
            })(t, e);
    }
    function H(t) {
        if (void 0 === t) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
        return t;
    }
    function N(t, e) {
        return !e || ("object" != typeof e && "function" != typeof e) ? H(t) : e;
    }
    function F(t) {
        var e = (function () {
            if ("undefined" == typeof Reflect || !Reflect.construct) return !1;
            if (Reflect.construct.sham) return !1;
            if ("function" == typeof Proxy) return !0;
            try {
                return Date.prototype.toString.call(Reflect.construct(Date, [], function () {})), !0;
            } catch (t) {
                return !1;
            }
        })();
        return function () {
            var i,
                n = j(t);
            if (e) {
                var s = j(this).constructor;
                i = Reflect.construct(n, arguments, s);
            } else i = n.apply(this, arguments);
            return N(this, i);
        };
    }
    function U(t, e, i) {
        return (U =
            "undefined" != typeof Reflect && Reflect.get
                ? Reflect.get
                : function (t, e, i) {
                      var n = (function (t, e) {
                          for (; !Object.prototype.hasOwnProperty.call(t, e) && null !== (t = j(t)); );
                          return t;
                      })(t, e);
                      if (n) {
                          var s = Object.getOwnPropertyDescriptor(n, e);
                          return s.get ? s.get.call(i) : s.value;
                      }
                  })(t, e, i || t);
    }
    function z(t, e) {
        return (
            (function (t) {
                if (Array.isArray(t)) return t;
            })(t) ||
            (function (t, e) {
                if ("undefined" == typeof Symbol || !(Symbol.iterator in Object(t))) return;
                var i = [],
                    n = !0,
                    s = !1,
                    o = void 0;
                try {
                    for (var r, a = t[Symbol.iterator](); !(n = (r = a.next()).done) && (i.push(r.value), !e || i.length !== e); n = !0);
                } catch (t) {
                    (s = !0), (o = t);
                } finally {
                    try {
                        n || null == a.return || a.return();
                    } finally {
                        if (s) throw o;
                    }
                }
                return i;
            })(t, e) ||
            V(t, e) ||
            (function () {
                throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
            })()
        );
    }
    function G(t) {
        return (
            (function (t) {
                if (Array.isArray(t)) return X(t);
            })(t) ||
            (function (t) {
                if ("undefined" != typeof Symbol && Symbol.iterator in Object(t)) return Array.from(t);
            })(t) ||
            V(t) ||
            (function () {
                throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
            })()
        );
    }
    function V(t, e) {
        if (t) {
            if ("string" == typeof t) return X(t, e);
            var i = Object.prototype.toString.call(t).slice(8, -1);
            return "Object" === i && t.constructor && (i = t.constructor.name), "Map" === i || "Set" === i ? Array.from(t) : "Arguments" === i || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(i) ? X(t, e) : void 0;
        }
    }
    function X(t, e) {
        (null == e || e > t.length) && (e = t.length);
        for (var i = 0, n = new Array(e); i < e; i++) n[i] = t[i];
        return n;
    }
    var Y = {
            el: document,
            name: "scroll",
            offset: [0, 0],
            repeat: !1,
            smooth: !1,
            direction: "vertical",
            gestureDirection: "vertical",
            reloadOnContextChange: !1,
            lerp: 0.1,
            class: "is-inview",
            scrollbarContainer: !1,
            scrollbarClass: "c-scrollbar",
            scrollingClass: "has-scroll-scrolling",
            draggingClass: "has-scroll-dragging",
            smoothClass: "has-scroll-smooth",
            initClass: "has-scroll-init",
            getSpeed: !1,
            getDirection: !1,
            scrollFromAnywhere: !1,
            multiplier: 1,
            firefoxMultiplier: 50,
            touchMultiplier: 2,
            resetNativeScroll: !0,
            tablet: { smooth: !1, direction: "vertical", gestureDirection: "vertical", breakpoint: 1024 },
            smartphone: { smooth: !1, direction: "vertical", gestureDirection: "vertical" },
        },
        q = (function () {
            function t() {
                var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
                O(this, t),
                    Object.assign(this, Y, e),
                    (this.smartphone = Y.smartphone),
                    e.smartphone && Object.assign(this.smartphone, e.smartphone),
                    (this.tablet = Y.tablet),
                    e.tablet && Object.assign(this.tablet, e.tablet),
                    (this.namespace = "locomotive"),
                    (this.html = document.documentElement),
                    (this.windowHeight = window.innerHeight),
                    (this.windowWidth = window.innerWidth),
                    (this.windowMiddle = { x: this.windowWidth / 2, y: this.windowHeight / 2 }),
                    (this.els = {}),
                    (this.currentElements = {}),
                    (this.listeners = {}),
                    (this.hasScrollTicking = !1),
                    (this.hasCallEventSet = !1),
                    (this.checkScroll = this.checkScroll.bind(this)),
                    (this.checkResize = this.checkResize.bind(this)),
                    (this.checkEvent = this.checkEvent.bind(this)),
                    (this.instance = { scroll: { x: 0, y: 0 }, limit: { x: this.html.offsetHeight, y: this.html.offsetHeight }, currentElements: this.currentElements }),
                    this.isMobile ? (this.isTablet ? (this.context = "tablet") : (this.context = "smartphone")) : (this.context = "desktop"),
                    this.isMobile && (this.direction = this[this.context].direction),
                    "horizontal" === this.direction ? (this.directionAxis = "x") : (this.directionAxis = "y"),
                    this.getDirection && (this.instance.direction = null),
                    this.getDirection && (this.instance.speed = 0),
                    this.html.classList.add(this.initClass),
                    window.addEventListener("resize", this.checkResize, !1);
            }
            return (
                _(t, [
                    {
                        key: "init",
                        value: function () {
                            this.initEvents();
                        },
                    },
                    {
                        key: "checkScroll",
                        value: function () {
                            this.dispatchScroll();
                        },
                    },
                    {
                        key: "checkResize",
                        value: function () {
                            var t = this;
                            this.resizeTick ||
                                ((this.resizeTick = !0),
                                requestAnimationFrame(function () {
                                    t.resize(), (t.resizeTick = !1);
                                }));
                        },
                    },
                    { key: "resize", value: function () {} },
                    {
                        key: "checkContext",
                        value: function () {
                            if (this.reloadOnContextChange) {
                                (this.isMobile =
                                    /Android|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) || ("MacIntel" === navigator.platform && navigator.maxTouchPoints > 1) || this.windowWidth < this.tablet.breakpoint),
                                    (this.isTablet = this.isMobile && this.windowWidth >= this.tablet.breakpoint);
                                var t = this.context;
                                if ((this.isMobile ? (this.isTablet ? (this.context = "tablet") : (this.context = "smartphone")) : (this.context = "desktop"), t != this.context))
                                    ("desktop" == t ? this.smooth : this[t].smooth) != ("desktop" == this.context ? this.smooth : this[this.context].smooth) && window.location.reload();
                            }
                        },
                    },
                    {
                        key: "initEvents",
                        value: function () {
                            var t = this;
                            (this.scrollToEls = this.el.querySelectorAll("[data-".concat(this.name, "-to]"))),
                                (this.setScrollTo = this.setScrollTo.bind(this)),
                                this.scrollToEls.forEach(function (e) {
                                    e.addEventListener("click", t.setScrollTo, !1);
                                });
                        },
                    },
                    {
                        key: "setScrollTo",
                        value: function (t) {
                            t.preventDefault(),
                                this.scrollTo(t.currentTarget.getAttribute("data-".concat(this.name, "-href")) || t.currentTarget.getAttribute("href"), { offset: t.currentTarget.getAttribute("data-".concat(this.name, "-offset")) });
                        },
                    },
                    { key: "addElements", value: function () {} },
                    {
                        key: "detectElements",
                        value: function (t) {
                            var e = this,
                                i = this.instance.scroll.y,
                                n = i + this.windowHeight,
                                s = this.instance.scroll.x,
                                o = s + this.windowWidth;
                            Object.entries(this.els).forEach(function (r) {
                                var a = z(r, 2),
                                    l = a[0],
                                    c = a[1];
                                if ((!c || (c.inView && !t) || ("horizontal" === e.direction ? o >= c.left && s < c.right && e.setInView(c, l) : n >= c.top && i < c.bottom && e.setInView(c, l)), c && c.inView))
                                    if ("horizontal" === e.direction) {
                                        var h = c.right - c.left;
                                        (c.progress = (e.instance.scroll.x - (c.left - e.windowWidth)) / (h + e.windowWidth)), (o < c.left || s > c.right) && e.setOutOfView(c, l);
                                    } else {
                                        var u = c.bottom - c.top;
                                        (c.progress = (e.instance.scroll.y - (c.top - e.windowHeight)) / (u + e.windowHeight)), (n < c.top || i > c.bottom) && e.setOutOfView(c, l);
                                    }
                            }),
                                (this.hasScrollTicking = !1);
                        },
                    },
                    {
                        key: "setInView",
                        value: function (t, e) {
                            (this.els[e].inView = !0), t.el.classList.add(t.class), (this.currentElements[e] = t), t.call && this.hasCallEventSet && (this.dispatchCall(t, "enter"), t.repeat || (this.els[e].call = !1));
                        },
                    },
                    {
                        key: "setOutOfView",
                        value: function (t, e) {
                            var i = this;
                            (this.els[e].inView = !1),
                                Object.keys(this.currentElements).forEach(function (t) {
                                    t === e && delete i.currentElements[t];
                                }),
                                t.call && this.hasCallEventSet && this.dispatchCall(t, "exit"),
                                t.repeat && t.el.classList.remove(t.class);
                        },
                    },
                    {
                        key: "dispatchCall",
                        value: function (t, e) {
                            (this.callWay = e),
                                (this.callValue = t.call.split(",").map(function (t) {
                                    return t.trim();
                                })),
                                (this.callObj = t),
                                1 == this.callValue.length && (this.callValue = this.callValue[0]);
                            var i = new Event(this.namespace + "call");
                            this.el.dispatchEvent(i);
                        },
                    },
                    {
                        key: "dispatchScroll",
                        value: function () {
                            var t = new Event(this.namespace + "scroll");
                            this.el.dispatchEvent(t);
                        },
                    },
                    {
                        key: "setEvents",
                        value: function (t, e) {
                            this.listeners[t] || (this.listeners[t] = []);
                            var i = this.listeners[t];
                            i.push(e), 1 === i.length && this.el.addEventListener(this.namespace + t, this.checkEvent, !1), "call" === t && ((this.hasCallEventSet = !0), this.detectElements(!0));
                        },
                    },
                    {
                        key: "unsetEvents",
                        value: function (t, e) {
                            if (this.listeners[t]) {
                                var i = this.listeners[t],
                                    n = i.indexOf(e);
                                n < 0 || (i.splice(n, 1), 0 === i.index && this.el.removeEventListener(this.namespace + t, this.checkEvent, !1));
                            }
                        },
                    },
                    {
                        key: "checkEvent",
                        value: function (t) {
                            var e = this,
                                i = t.type.replace(this.namespace, ""),
                                n = this.listeners[i];
                            n &&
                                0 !== n.length &&
                                n.forEach(function (t) {
                                    switch (i) {
                                        case "scroll":
                                            return t(e.instance);
                                        case "call":
                                            return t(e.callValue, e.callWay, e.callObj);
                                        default:
                                            return t();
                                    }
                                });
                        },
                    },
                    { key: "startScroll", value: function () {} },
                    { key: "stopScroll", value: function () {} },
                    {
                        key: "setScroll",
                        value: function (t, e) {
                            this.instance.scroll = { x: 0, y: 0 };
                        },
                    },
                    {
                        key: "destroy",
                        value: function () {
                            var t = this;
                            window.removeEventListener("resize", this.checkResize, !1),
                                Object.keys(this.listeners).forEach(function (e) {
                                    t.el.removeEventListener(t.namespace + e, t.checkEvent, !1);
                                }),
                                (this.listeners = {}),
                                this.scrollToEls.forEach(function (e) {
                                    e.removeEventListener("click", t.setScrollTo, !1);
                                }),
                                this.html.classList.remove(this.initClass);
                        },
                    },
                ]),
                t
            );
        })(),
        $ = "undefined" != typeof globalThis ? globalThis : "undefined" != typeof window ? window : "undefined" != typeof global ? global : "undefined" != typeof self ? self : {};
    function K(t, e) {
        return t((e = { exports: {} }), e.exports), e.exports;
    }
    var J = K(function (t, e) {
            t.exports = {
                polyfill: function () {
                    var t = window,
                        e = document;
                    if (!("scrollBehavior" in e.documentElement.style) || !0 === t.__forceSmoothScrollPolyfill__) {
                        var i,
                            n = t.HTMLElement || t.Element,
                            s = { scroll: t.scroll || t.scrollTo, scrollBy: t.scrollBy, elementScroll: n.prototype.scroll || a, scrollIntoView: n.prototype.scrollIntoView },
                            o = t.performance && t.performance.now ? t.performance.now.bind(t.performance) : Date.now,
                            r = ((i = t.navigator.userAgent), new RegExp(["MSIE ", "Trident/", "Edge/"].join("|")).test(i) ? 1 : 0);
                        (t.scroll = t.scrollTo = function () {
                            void 0 !== arguments[0] &&
                                (!0 !== l(arguments[0])
                                    ? p.call(t, e.body, void 0 !== arguments[0].left ? ~~arguments[0].left : t.scrollX || t.pageXOffset, void 0 !== arguments[0].top ? ~~arguments[0].top : t.scrollY || t.pageYOffset)
                                    : s.scroll.call(
                                          t,
                                          void 0 !== arguments[0].left ? arguments[0].left : "object" != typeof arguments[0] ? arguments[0] : t.scrollX || t.pageXOffset,
                                          void 0 !== arguments[0].top ? arguments[0].top : void 0 !== arguments[1] ? arguments[1] : t.scrollY || t.pageYOffset
                                      ));
                        }),
                            (t.scrollBy = function () {
                                void 0 !== arguments[0] &&
                                    (l(arguments[0])
                                        ? s.scrollBy.call(
                                              t,
                                              void 0 !== arguments[0].left ? arguments[0].left : "object" != typeof arguments[0] ? arguments[0] : 0,
                                              void 0 !== arguments[0].top ? arguments[0].top : void 0 !== arguments[1] ? arguments[1] : 0
                                          )
                                        : p.call(t, e.body, ~~arguments[0].left + (t.scrollX || t.pageXOffset), ~~arguments[0].top + (t.scrollY || t.pageYOffset)));
                            }),
                            (n.prototype.scroll = n.prototype.scrollTo = function () {
                                if (void 0 !== arguments[0])
                                    if (!0 !== l(arguments[0])) {
                                        var t = arguments[0].left,
                                            e = arguments[0].top;
                                        p.call(this, this, void 0 === t ? this.scrollLeft : ~~t, void 0 === e ? this.scrollTop : ~~e);
                                    } else {
                                        if ("number" == typeof arguments[0] && void 0 === arguments[1]) throw new SyntaxError("Value could not be converted");
                                        s.elementScroll.call(
                                            this,
                                            void 0 !== arguments[0].left ? ~~arguments[0].left : "object" != typeof arguments[0] ? ~~arguments[0] : this.scrollLeft,
                                            void 0 !== arguments[0].top ? ~~arguments[0].top : void 0 !== arguments[1] ? ~~arguments[1] : this.scrollTop
                                        );
                                    }
                            }),
                            (n.prototype.scrollBy = function () {
                                void 0 !== arguments[0] &&
                                    (!0 !== l(arguments[0])
                                        ? this.scroll({ left: ~~arguments[0].left + this.scrollLeft, top: ~~arguments[0].top + this.scrollTop, behavior: arguments[0].behavior })
                                        : s.elementScroll.call(
                                              this,
                                              void 0 !== arguments[0].left ? ~~arguments[0].left + this.scrollLeft : ~~arguments[0] + this.scrollLeft,
                                              void 0 !== arguments[0].top ? ~~arguments[0].top + this.scrollTop : ~~arguments[1] + this.scrollTop
                                          ));
                            }),
                            (n.prototype.scrollIntoView = function () {
                                if (!0 !== l(arguments[0])) {
                                    var i = d(this),
                                        n = i.getBoundingClientRect(),
                                        o = this.getBoundingClientRect();
                                    i !== e.body
                                        ? (p.call(this, i, i.scrollLeft + o.left - n.left, i.scrollTop + o.top - n.top), "fixed" !== t.getComputedStyle(i).position && t.scrollBy({ left: n.left, top: n.top, behavior: "smooth" }))
                                        : t.scrollBy({ left: o.left, top: o.top, behavior: "smooth" });
                                } else s.scrollIntoView.call(this, void 0 === arguments[0] || arguments[0]);
                            });
                    }
                    function a(t, e) {
                        (this.scrollLeft = t), (this.scrollTop = e);
                    }
                    function l(t) {
                        if (null === t || "object" != typeof t || void 0 === t.behavior || "auto" === t.behavior || "instant" === t.behavior) return !0;
                        if ("object" == typeof t && "smooth" === t.behavior) return !1;
                        throw new TypeError("behavior member of ScrollOptions " + t.behavior + " is not a valid value for enumeration ScrollBehavior.");
                    }
                    function c(t, e) {
                        return "Y" === e ? t.clientHeight + r < t.scrollHeight : "X" === e ? t.clientWidth + r < t.scrollWidth : void 0;
                    }
                    function h(e, i) {
                        var n = t.getComputedStyle(e, null)["overflow" + i];
                        return "auto" === n || "scroll" === n;
                    }
                    function u(t) {
                        var e = c(t, "Y") && h(t, "Y"),
                            i = c(t, "X") && h(t, "X");
                        return e || i;
                    }
                    function d(t) {
                        for (; t !== e.body && !1 === u(t); ) t = t.parentNode || t.host;
                        return t;
                    }
                    function f(e) {
                        var i,
                            n,
                            s,
                            r,
                            a = (o() - e.startTime) / 468;
                        (r = a = a > 1 ? 1 : a),
                            (i = 0.5 * (1 - Math.cos(Math.PI * r))),
                            (n = e.startX + (e.x - e.startX) * i),
                            (s = e.startY + (e.y - e.startY) * i),
                            e.method.call(e.scrollable, n, s),
                            (n === e.x && s === e.y) || t.requestAnimationFrame(f.bind(t, e));
                    }
                    function p(i, n, r) {
                        var l,
                            c,
                            h,
                            u,
                            d = o();
                        i === e.body ? ((l = t), (c = t.scrollX || t.pageXOffset), (h = t.scrollY || t.pageYOffset), (u = s.scroll)) : ((l = i), (c = i.scrollLeft), (h = i.scrollTop), (u = a)),
                            f({ scrollable: l, method: u, startTime: d, startX: c, startY: h, x: n, y: r });
                    }
                },
            };
        }),
        Q =
            (J.polyfill,
            (function (t) {
                I(i, t);
                var e = F(i);
                function i() {
                    var t,
                        n = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
                    return (
                        O(this, i),
                        (t = e.call(this, n)).resetNativeScroll && (history.scrollRestoration && (history.scrollRestoration = "manual"), window.scrollTo(0, 0)),
                        window.addEventListener("scroll", t.checkScroll, !1),
                        void 0 === window.smoothscrollPolyfill && ((window.smoothscrollPolyfill = J), window.smoothscrollPolyfill.polyfill()),
                        t
                    );
                }
                return (
                    _(i, [
                        {
                            key: "init",
                            value: function () {
                                (this.instance.scroll.y = window.pageYOffset), this.addElements(), this.detectElements(), U(j(i.prototype), "init", this).call(this);
                            },
                        },
                        {
                            key: "checkScroll",
                            value: function () {
                                var t = this;
                                U(j(i.prototype), "checkScroll", this).call(this),
                                    this.getDirection && this.addDirection(),
                                    this.getSpeed && (this.addSpeed(), (this.speedTs = Date.now())),
                                    (this.instance.scroll.y = window.pageYOffset),
                                    Object.entries(this.els).length &&
                                        (this.hasScrollTicking ||
                                            (requestAnimationFrame(function () {
                                                t.detectElements();
                                            }),
                                            (this.hasScrollTicking = !0)));
                            },
                        },
                        {
                            key: "addDirection",
                            value: function () {
                                window.pageYOffset > this.instance.scroll.y
                                    ? "down" !== this.instance.direction && (this.instance.direction = "down")
                                    : window.pageYOffset < this.instance.scroll.y && "up" !== this.instance.direction && (this.instance.direction = "up");
                            },
                        },
                        {
                            key: "addSpeed",
                            value: function () {
                                window.pageYOffset != this.instance.scroll.y ? (this.instance.speed = (window.pageYOffset - this.instance.scroll.y) / Math.max(1, Date.now() - this.speedTs)) : (this.instance.speed = 0);
                            },
                        },
                        {
                            key: "resize",
                            value: function () {
                                Object.entries(this.els).length && ((this.windowHeight = window.innerHeight), this.updateElements());
                            },
                        },
                        {
                            key: "addElements",
                            value: function () {
                                var t = this;
                                (this.els = {}),
                                    this.el.querySelectorAll("[data-" + this.name + "]").forEach(function (e, i) {
                                        var n = e.getBoundingClientRect(),
                                            s = e.dataset[t.name + "Class"] || t.class,
                                            o = "string" == typeof e.dataset[t.name + "Id"] ? e.dataset[t.name + "Id"] : i,
                                            r = n.top + t.instance.scroll.y,
                                            a = n.left,
                                            l = n.right,
                                            c = r + e.offsetHeight,
                                            h = "string" == typeof e.dataset[t.name + "Offset"] ? e.dataset[t.name + "Offset"].split(",") : t.offset,
                                            u = e.dataset[t.name + "Repeat"],
                                            d = e.dataset[t.name + "Call"];
                                        u = "false" != u && (null != u || t.repeat);
                                        var f = t.getRelativeOffset(h),
                                            p = { el: e, id: o, class: s, top: r + f[0], bottom: c - f[1], left: a, right: l, offset: h, progress: 0, repeat: u, inView: !1, call: d };
                                        (t.els[o] = p), e.classList.contains(s) && t.setInView(t.els[o], o);
                                    });
                            },
                        },
                        {
                            key: "updateElements",
                            value: function () {
                                var t = this;
                                Object.entries(this.els).forEach(function (e) {
                                    var i = z(e, 2),
                                        n = i[0],
                                        s = i[1],
                                        o = s.el.getBoundingClientRect().top + t.instance.scroll.y,
                                        r = o + s.el.offsetHeight,
                                        a = t.getRelativeOffset(s.offset);
                                    (t.els[n].top = o + a[0]), (t.els[n].bottom = r - a[1]);
                                }),
                                    (this.hasScrollTicking = !1);
                            },
                        },
                        {
                            key: "getRelativeOffset",
                            value: function (t) {
                                var e = [0, 0];
                                if (t) for (var i = 0; i < t.length; i++) "string" == typeof t[i] ? (t[i].includes("%") ? (e[i] = parseInt((t[i].replace("%", "") * this.windowHeight) / 100)) : (e[i] = parseInt(t[i]))) : (e[i] = t[i]);
                                return e;
                            },
                        },
                        {
                            key: "scrollTo",
                            value: function (t) {
                                var e = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {},
                                    i = parseInt(e.offset) || 0,
                                    n = !!e.callback && e.callback;
                                if ("string" == typeof t) {
                                    if ("top" === t) t = this.html;
                                    else if ("bottom" === t) t = this.html.offsetHeight - window.innerHeight;
                                    else if (!(t = document.querySelector(t))) return;
                                } else if ("number" == typeof t) t = parseInt(t);
                                else if (!t || !t.tagName) return void console.warn("`target` parameter is not valid");
                                if (((i = "number" != typeof t ? t.getBoundingClientRect().top + i + this.instance.scroll.y : t + i), n)) {
                                    i = i.toFixed();
                                    var s = function t() {
                                        window.pageYOffset.toFixed() === i && (window.removeEventListener("scroll", t), n());
                                    };
                                    window.addEventListener("scroll", s);
                                }
                                window.scrollTo({ top: i, behavior: "smooth" });
                            },
                        },
                        {
                            key: "update",
                            value: function () {
                                this.addElements(), this.detectElements();
                            },
                        },
                        {
                            key: "destroy",
                            value: function () {
                                U(j(i.prototype), "destroy", this).call(this), window.removeEventListener("scroll", this.checkScroll, !1);
                            },
                        },
                    ]),
                    i
                );
            })(q)),
        Z = Object.getOwnPropertySymbols,
        tt = Object.prototype.hasOwnProperty,
        et = Object.prototype.propertyIsEnumerable;
    function it(t) {
        if (null == t) throw new TypeError("Object.assign cannot be called with null or undefined");
        return Object(t);
    }
    var nt = (function () {
        try {
            if (!Object.assign) return !1;
            var t = new String("abc");
            if (((t[5] = "de"), "5" === Object.getOwnPropertyNames(t)[0])) return !1;
            for (var e = {}, i = 0; i < 10; i++) e["_" + String.fromCharCode(i)] = i;
            if (
                "0123456789" !==
                Object.getOwnPropertyNames(e)
                    .map(function (t) {
                        return e[t];
                    })
                    .join("")
            )
                return !1;
            var n = {};
            return (
                "abcdefghijklmnopqrst".split("").forEach(function (t) {
                    n[t] = t;
                }),
                "abcdefghijklmnopqrst" === Object.keys(Object.assign({}, n)).join("")
            );
        } catch (t) {
            return !1;
        }
    })()
        ? Object.assign
        : function (t, e) {
              for (var i, n, s = it(t), o = 1; o < arguments.length; o++) {
                  for (var r in (i = Object(arguments[o]))) tt.call(i, r) && (s[r] = i[r]);
                  if (Z) {
                      n = Z(i);
                      for (var a = 0; a < n.length; a++) et.call(i, n[a]) && (s[n[a]] = i[n[a]]);
                  }
              }
              return s;
          };
    function st() {}
    st.prototype = {
        on: function (t, e, i) {
            var n = this.e || (this.e = {});
            return (n[t] || (n[t] = [])).push({ fn: e, ctx: i }), this;
        },
        once: function (t, e, i) {
            var n = this;
            function s() {
                n.off(t, s), e.apply(i, arguments);
            }
            return (s._ = e), this.on(t, s, i);
        },
        emit: function (t) {
            for (var e = [].slice.call(arguments, 1), i = ((this.e || (this.e = {}))[t] || []).slice(), n = 0, s = i.length; n < s; n++) i[n].fn.apply(i[n].ctx, e);
            return this;
        },
        off: function (t, e) {
            var i = this.e || (this.e = {}),
                n = i[t],
                s = [];
            if (n && e) for (var o = 0, r = n.length; o < r; o++) n[o].fn !== e && n[o].fn._ !== e && s.push(n[o]);
            return s.length ? (i[t] = s) : delete i[t], this;
        },
    };
    var ot = st,
        rt = K(function (t, e) {
            (function () {
                (null !== e ? e : this).Lethargy = (function () {
                    function t(t, e, i, n) {
                        (this.stability = null != t ? Math.abs(t) : 8),
                            (this.sensitivity = null != e ? 1 + Math.abs(e) : 100),
                            (this.tolerance = null != i ? 1 + Math.abs(i) : 1.1),
                            (this.delay = null != n ? n : 150),
                            (this.lastUpDeltas = function () {
                                var t, e, i;
                                for (i = [], t = 1, e = 2 * this.stability; 1 <= e ? t <= e : t >= e; 1 <= e ? t++ : t--) i.push(null);
                                return i;
                            }.call(this)),
                            (this.lastDownDeltas = function () {
                                var t, e, i;
                                for (i = [], t = 1, e = 2 * this.stability; 1 <= e ? t <= e : t >= e; 1 <= e ? t++ : t--) i.push(null);
                                return i;
                            }.call(this)),
                            (this.deltasTimestamp = function () {
                                var t, e, i;
                                for (i = [], t = 1, e = 2 * this.stability; 1 <= e ? t <= e : t >= e; 1 <= e ? t++ : t--) i.push(null);
                                return i;
                            }.call(this));
                    }
                    return (
                        (t.prototype.check = function (t) {
                            var e;
                            return (
                                null != (t = t.originalEvent || t).wheelDelta ? (e = t.wheelDelta) : null != t.deltaY ? (e = -40 * t.deltaY) : (null == t.detail && 0 !== t.detail) || (e = -40 * t.detail),
                                this.deltasTimestamp.push(Date.now()),
                                this.deltasTimestamp.shift(),
                                e > 0 ? (this.lastUpDeltas.push(e), this.lastUpDeltas.shift(), this.isInertia(1)) : (this.lastDownDeltas.push(e), this.lastDownDeltas.shift(), this.isInertia(-1))
                            );
                        }),
                        (t.prototype.isInertia = function (t) {
                            var e, i, n, s, o, r, a;
                            return null === (e = -1 === t ? this.lastDownDeltas : this.lastUpDeltas)[0]
                                ? t
                                : !(this.deltasTimestamp[2 * this.stability - 2] + this.delay > Date.now() && e[0] === e[2 * this.stability - 1]) &&
                                      ((n = e.slice(0, this.stability)),
                                      (i = e.slice(this.stability, 2 * this.stability)),
                                      (a = n.reduce(function (t, e) {
                                          return t + e;
                                      })),
                                      (o = i.reduce(function (t, e) {
                                          return t + e;
                                      })),
                                      (r = a / n.length),
                                      (s = o / i.length),
                                      Math.abs(r) < Math.abs(s * this.tolerance) && this.sensitivity < Math.abs(s) && t);
                        }),
                        (t.prototype.showLastUpDeltas = function () {
                            return this.lastUpDeltas;
                        }),
                        (t.prototype.showLastDownDeltas = function () {
                            return this.lastDownDeltas;
                        }),
                        t
                    );
                })();
            }.call($));
        }),
        at = {
            hasWheelEvent: "onwheel" in document,
            hasMouseWheelEvent: "onmousewheel" in document,
            hasTouch: "ontouchstart" in window || window.TouchEvent || (window.DocumentTouch && document instanceof DocumentTouch),
            hasTouchWin: navigator.msMaxTouchPoints && navigator.msMaxTouchPoints > 1,
            hasPointer: !!window.navigator.msPointerEnabled,
            hasKeyDown: "onkeydown" in document,
            isFirefox: navigator.userAgent.indexOf("Firefox") > -1,
        },
        lt = Object.prototype.toString,
        ct = Object.prototype.hasOwnProperty;
    function ht(t, e) {
        return function () {
            return t.apply(e, arguments);
        };
    }
    var ut = rt.Lethargy,
        dt = "virtualscroll",
        ft = wt,
        pt = 37,
        vt = 38,
        mt = 39,
        gt = 40,
        yt = 32;
    function wt(t) {
        !(function (t) {
            if (!t) return console.warn("bindAll requires at least one argument.");
            var e = Array.prototype.slice.call(arguments, 1);
            if (0 === e.length) for (var i in t) ct.call(t, i) && "function" == typeof t[i] && "[object Function]" == lt.call(t[i]) && e.push(i);
            for (var n = 0; n < e.length; n++) {
                var s = e[n];
                t[s] = ht(t[s], t);
            }
        })(this, "_onWheel", "_onMouseWheel", "_onTouchStart", "_onTouchMove", "_onKeyDown"),
            (this.el = window),
            t && t.el && ((this.el = t.el), delete t.el),
            (this.options = nt({ mouseMultiplier: 1, touchMultiplier: 2, firefoxMultiplier: 15, keyStep: 120, preventTouch: !1, unpreventTouchClass: "vs-touchmove-allowed", limitInertia: !1, useKeyboard: !0, useTouch: !0 }, t)),
            this.options.limitInertia && (this._lethargy = new ut()),
            (this._emitter = new ot()),
            (this._event = { y: 0, x: 0, deltaX: 0, deltaY: 0 }),
            (this.touchStartX = null),
            (this.touchStartY = null),
            (this.bodyTouchAction = null),
            void 0 !== this.options.passive && (this.listenerOptions = { passive: this.options.passive });
    }
    function bt(t, e, i) {
        return (1 - i) * t + i * e;
    }
    function kt(t) {
        var e = {};
        if (window.getComputedStyle) {
            var i = getComputedStyle(t),
                n = i.transform || i.webkitTransform || i.mozTransform,
                s = n.match(/^matrix3d\((.+)\)$/);
            return (
                s
                    ? ((e.x = s ? parseFloat(s[1].split(", ")[12]) : 0), (e.y = s ? parseFloat(s[1].split(", ")[13]) : 0))
                    : ((s = n.match(/^matrix\((.+)\)$/)), (e.x = s ? parseFloat(s[1].split(", ")[4]) : 0), (e.y = s ? parseFloat(s[1].split(", ")[5]) : 0)),
                e
            );
        }
    }
    function xt(t) {
        for (var e = []; t && t !== document; t = t.parentNode) e.push(t);
        return e;
    }
    (wt.prototype._notify = function (t) {
        var e = this._event;
        (e.x += e.deltaX), (e.y += e.deltaY), this._emitter.emit(dt, { x: e.x, y: e.y, deltaX: e.deltaX, deltaY: e.deltaY, originalEvent: t });
    }),
        (wt.prototype._onWheel = function (t) {
            var e = this.options;
            if (!this._lethargy || !1 !== this._lethargy.check(t)) {
                var i = this._event;
                (i.deltaX = t.wheelDeltaX || -1 * t.deltaX),
                    (i.deltaY = t.wheelDeltaY || -1 * t.deltaY),
                    at.isFirefox && 1 == t.deltaMode && ((i.deltaX *= e.firefoxMultiplier), (i.deltaY *= e.firefoxMultiplier)),
                    (i.deltaX *= e.mouseMultiplier),
                    (i.deltaY *= e.mouseMultiplier),
                    this._notify(t);
            }
        }),
        (wt.prototype._onMouseWheel = function (t) {
            if (!this.options.limitInertia || !1 !== this._lethargy.check(t)) {
                var e = this._event;
                (e.deltaX = t.wheelDeltaX ? t.wheelDeltaX : 0), (e.deltaY = t.wheelDeltaY ? t.wheelDeltaY : t.wheelDelta), this._notify(t);
            }
        }),
        (wt.prototype._onTouchStart = function (t) {
            var e = t.targetTouches ? t.targetTouches[0] : t;
            (this.touchStartX = e.pageX), (this.touchStartY = e.pageY);
        }),
        (wt.prototype._onTouchMove = function (t) {
            var e = this.options;
            e.preventTouch && !t.target.classList.contains(e.unpreventTouchClass) && t.preventDefault();
            var i = this._event,
                n = t.targetTouches ? t.targetTouches[0] : t;
            (i.deltaX = (n.pageX - this.touchStartX) * e.touchMultiplier), (i.deltaY = (n.pageY - this.touchStartY) * e.touchMultiplier), (this.touchStartX = n.pageX), (this.touchStartY = n.pageY), this._notify(t);
        }),
        (wt.prototype._onKeyDown = function (t) {
            var e = this._event;
            e.deltaX = e.deltaY = 0;
            var i = window.innerHeight - 40;
            switch (t.keyCode) {
                case pt:
                case vt:
                    e.deltaY = this.options.keyStep;
                    break;
                case mt:
                case gt:
                    e.deltaY = -this.options.keyStep;
                    break;
                case t.shiftKey:
                    e.deltaY = i;
                    break;
                case yt:
                    e.deltaY = -i;
                    break;
                default:
                    return;
            }
            this._notify(t);
        }),
        (wt.prototype._bind = function () {
            at.hasWheelEvent && this.el.addEventListener("wheel", this._onWheel, this.listenerOptions),
                at.hasMouseWheelEvent && this.el.addEventListener("mousewheel", this._onMouseWheel, this.listenerOptions),
                at.hasTouch && this.options.useTouch && (this.el.addEventListener("touchstart", this._onTouchStart, this.listenerOptions), this.el.addEventListener("touchmove", this._onTouchMove, this.listenerOptions)),
                at.hasPointer &&
                    at.hasTouchWin &&
                    ((this.bodyTouchAction = document.body.style.msTouchAction),
                    (document.body.style.msTouchAction = "none"),
                    this.el.addEventListener("MSPointerDown", this._onTouchStart, !0),
                    this.el.addEventListener("MSPointerMove", this._onTouchMove, !0)),
                at.hasKeyDown && this.options.useKeyboard && document.addEventListener("keydown", this._onKeyDown);
        }),
        (wt.prototype._unbind = function () {
            at.hasWheelEvent && this.el.removeEventListener("wheel", this._onWheel),
                at.hasMouseWheelEvent && this.el.removeEventListener("mousewheel", this._onMouseWheel),
                at.hasTouch && (this.el.removeEventListener("touchstart", this._onTouchStart), this.el.removeEventListener("touchmove", this._onTouchMove)),
                at.hasPointer &&
                    at.hasTouchWin &&
                    ((document.body.style.msTouchAction = this.bodyTouchAction), this.el.removeEventListener("MSPointerDown", this._onTouchStart, !0), this.el.removeEventListener("MSPointerMove", this._onTouchMove, !0)),
                at.hasKeyDown && this.options.useKeyboard && document.removeEventListener("keydown", this._onKeyDown);
        }),
        (wt.prototype.on = function (t, e) {
            this._emitter.on(dt, t, e);
            var i = this._emitter.e;
            i && i[dt] && 1 === i[dt].length && this._bind();
        }),
        (wt.prototype.off = function (t, e) {
            this._emitter.off(dt, t, e);
            var i = this._emitter.e;
            (!i[dt] || i[dt].length <= 0) && this._unbind();
        }),
        (wt.prototype.reset = function () {
            var t = this._event;
            (t.x = 0), (t.y = 0);
        }),
        (wt.prototype.destroy = function () {
            this._emitter.off(), this._unbind();
        });
    var Et = "function" == typeof Float32Array;
    function Tt(t, e) {
        return 1 - 3 * e + 3 * t;
    }
    function St(t, e) {
        return 3 * e - 6 * t;
    }
    function At(t) {
        return 3 * t;
    }
    function Ct(t, e, i) {
        return ((Tt(e, i) * t + St(e, i)) * t + At(e)) * t;
    }
    function Mt(t, e, i) {
        return 3 * Tt(e, i) * t * t + 2 * St(e, i) * t + At(e);
    }
    function Lt(t) {
        return t;
    }
    var Ot = function (t, e, i, n) {
            if (!(0 <= t && t <= 1 && 0 <= i && i <= 1)) throw new Error("bezier x values must be in [0, 1] range");
            if (t === e && i === n) return Lt;
            for (var s = Et ? new Float32Array(11) : new Array(11), o = 0; o < 11; ++o) s[o] = Ct(0.1 * o, t, i);
            function r(e) {
                for (var n = 0, o = 1; 10 !== o && s[o] <= e; ++o) n += 0.1;
                --o;
                var r = n + 0.1 * ((e - s[o]) / (s[o + 1] - s[o])),
                    a = Mt(r, t, i);
                return a >= 0.001
                    ? (function (t, e, i, n) {
                          for (var s = 0; s < 4; ++s) {
                              var o = Mt(e, i, n);
                              if (0 === o) return e;
                              e -= (Ct(e, i, n) - t) / o;
                          }
                          return e;
                      })(e, r, t, i)
                    : 0 === a
                    ? r
                    : (function (t, e, i, n, s) {
                          var o,
                              r,
                              a = 0;
                          do {
                              (o = Ct((r = e + (i - e) / 2), n, s) - t) > 0 ? (i = r) : (e = r);
                          } while (Math.abs(o) > 1e-7 && ++a < 10);
                          return r;
                      })(e, n, n + 0.1, t, i);
            }
            return function (t) {
                return 0 === t ? 0 : 1 === t ? 1 : Ct(r(t), e, n);
            };
        },
        Pt = 38,
        _t = 40,
        Rt = 32,
        Dt = 9,
        Bt = 33,
        It = 34,
        jt = 36,
        Wt = 35,
        Ht = (function (t) {
            I(i, t);
            var e = F(i);
            function i() {
                var t,
                    n = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
                return (
                    O(this, i),
                    history.scrollRestoration && (history.scrollRestoration = "manual"),
                    window.scrollTo(0, 0),
                    (t = e.call(this, n)).inertia && (t.lerp = 0.1 * t.inertia),
                    (t.isScrolling = !1),
                    (t.isDraggingScrollbar = !1),
                    (t.isTicking = !1),
                    (t.hasScrollTicking = !1),
                    (t.parallaxElements = {}),
                    (t.stop = !1),
                    (t.scrollbarContainer = n.scrollbarContainer),
                    (t.checkKey = t.checkKey.bind(H(t))),
                    window.addEventListener("keydown", t.checkKey, !1),
                    t
                );
            }
            return (
                _(i, [
                    {
                        key: "init",
                        value: function () {
                            var t = this;
                            this.html.classList.add(this.smoothClass),
                                this.html.setAttribute("data-".concat(this.name, "-direction"), this.direction),
                                (this.instance = B({ delta: { x: 0, y: 0 } }, this.instance)),
                                (this.vs = new ft({
                                    el: this.scrollFromAnywhere ? document : this.el,
                                    mouseMultiplier: navigator.platform.indexOf("Win") > -1 ? 1 : 0.4,
                                    firefoxMultiplier: this.firefoxMultiplier,
                                    touchMultiplier: this.touchMultiplier,
                                    useKeyboard: !1,
                                    passive: !0,
                                })),
                                this.vs.on(function (e) {
                                    t.stop ||
                                        t.isDraggingScrollbar ||
                                        requestAnimationFrame(function () {
                                            t.updateDelta(e), t.isScrolling || t.startScrolling();
                                        });
                                }),
                                this.setScrollLimit(),
                                this.initScrollBar(),
                                this.addSections(),
                                this.addElements(),
                                this.checkScroll(!0),
                                this.transformElements(!0, !0),
                                U(j(i.prototype), "init", this).call(this);
                        },
                    },
                    {
                        key: "setScrollLimit",
                        value: function () {
                            if (((this.instance.limit.y = this.el.offsetHeight - this.windowHeight), "horizontal" === this.direction)) {
                                for (var t = 0, e = this.el.children, i = 0; i < e.length; i++) t += e[i].offsetWidth;
                                this.instance.limit.x = t - this.windowWidth;
                            }
                        },
                    },
                    {
                        key: "startScrolling",
                        value: function () {
                            (this.startScrollTs = Date.now()), (this.isScrolling = !0), this.checkScroll(), this.html.classList.add(this.scrollingClass);
                        },
                    },
                    {
                        key: "stopScrolling",
                        value: function () {
                            cancelAnimationFrame(this.checkScrollRaf),
                                this.scrollToRaf && (cancelAnimationFrame(this.scrollToRaf), (this.scrollToRaf = null)),
                                (this.isScrolling = !1),
                                (this.instance.scroll.y = Math.round(this.instance.scroll.y)),
                                this.html.classList.remove(this.scrollingClass);
                        },
                    },
                    {
                        key: "checkKey",
                        value: function (t) {
                            var e = this;
                            if (this.stop)
                                t.keyCode == Dt &&
                                    requestAnimationFrame(function () {
                                        (e.html.scrollTop = 0), (document.body.scrollTop = 0), (e.html.scrollLeft = 0), (document.body.scrollLeft = 0);
                                    });
                            else {
                                switch (t.keyCode) {
                                    case Dt:
                                        requestAnimationFrame(function () {
                                            (e.html.scrollTop = 0), (document.body.scrollTop = 0), (e.html.scrollLeft = 0), (document.body.scrollLeft = 0), e.scrollTo(document.activeElement, -window.innerHeight / 2);
                                        });
                                        break;
                                    case Pt:
                                        this.instance.delta[this.directionAxis] -= 240;
                                        break;
                                    case _t:
                                        this.instance.delta[this.directionAxis] += 240;
                                        break;
                                    case Bt:
                                        this.instance.delta[this.directionAxis] -= window.innerHeight;
                                        break;
                                    case It:
                                        this.instance.delta[this.directionAxis] += window.innerHeight;
                                        break;
                                    case jt:
                                        this.instance.delta[this.directionAxis] -= this.instance.limit[this.directionAxis];
                                        break;
                                    case Wt:
                                        this.instance.delta[this.directionAxis] += this.instance.limit[this.directionAxis];
                                        break;
                                    case Rt:
                                        document.activeElement instanceof HTMLInputElement ||
                                            document.activeElement instanceof HTMLTextAreaElement ||
                                            (t.shiftKey ? (this.instance.delta[this.directionAxis] -= window.innerHeight) : (this.instance.delta[this.directionAxis] += window.innerHeight));
                                        break;
                                    default:
                                        return;
                                }
                                this.instance.delta[this.directionAxis] < 0 && (this.instance.delta[this.directionAxis] = 0),
                                    this.instance.delta[this.directionAxis] > this.instance.limit[this.directionAxis] && (this.instance.delta[this.directionAxis] = this.instance.limit[this.directionAxis]),
                                    this.stopScrolling(),
                                    (this.isScrolling = !0),
                                    this.checkScroll(),
                                    this.html.classList.add(this.scrollingClass);
                            }
                        },
                    },
                    {
                        key: "checkScroll",
                        value: function () {
                            var t = this,
                                e = arguments.length > 0 && void 0 !== arguments[0] && arguments[0];
                            if (e || this.isScrolling || this.isDraggingScrollbar) {
                                this.hasScrollTicking ||
                                    ((this.checkScrollRaf = requestAnimationFrame(function () {
                                        return t.checkScroll();
                                    })),
                                    (this.hasScrollTicking = !0)),
                                    this.updateScroll();
                                var n = Math.abs(this.instance.delta[this.directionAxis] - this.instance.scroll[this.directionAxis]),
                                    s = Date.now() - this.startScrollTs;
                                if (
                                    (!this.animatingScroll && s > 100 && ((n < 0.5 && 0 != this.instance.delta[this.directionAxis]) || (n < 0.5 && 0 == this.instance.delta[this.directionAxis])) && this.stopScrolling(),
                                    Object.entries(this.sections).forEach(function (e) {
                                        var i = z(e, 2),
                                            n = (i[0], i[1]);
                                        n.persistent || (t.instance.scroll[t.directionAxis] > n.offset[t.directionAxis] && t.instance.scroll[t.directionAxis] < n.limit[t.directionAxis])
                                            ? ("horizontal" === t.direction ? t.transform(n.el, -t.instance.scroll[t.directionAxis], 0) : t.transform(n.el, 0, -t.instance.scroll[t.directionAxis]),
                                              n.inView || ((n.inView = !0), (n.el.style.opacity = 1), (n.el.style.pointerEvents = "all"), n.el.setAttribute("data-".concat(t.name, "-section-inview"), "")))
                                            : (n.inView && ((n.inView = !1), (n.el.style.opacity = 0), (n.el.style.pointerEvents = "none"), n.el.removeAttribute("data-".concat(t.name, "-section-inview"))), t.transform(n.el, 0, 0));
                                    }),
                                    this.getDirection && this.addDirection(),
                                    this.getSpeed && (this.addSpeed(), (this.speedTs = Date.now())),
                                    this.detectElements(),
                                    this.transformElements(),
                                    this.hasScrollbar)
                                ) {
                                    var o = (this.instance.scroll[this.directionAxis] / this.instance.limit[this.directionAxis]) * this.scrollBarLimit[this.directionAxis];
                                    "horizontal" === this.direction ? this.transform(this.scrollbarThumb, o, 0) : this.transform(this.scrollbarThumb, 0, o);
                                }
                                U(j(i.prototype), "checkScroll", this).call(this), (this.hasScrollTicking = !1);
                            }
                        },
                    },
                    {
                        key: "resize",
                        value: function () {
                            (this.windowHeight = window.innerHeight), (this.windowWidth = window.innerWidth), this.checkContext(), (this.windowMiddle = { x: this.windowWidth / 2, y: this.windowHeight / 2 }), this.update();
                        },
                    },
                    {
                        key: "updateDelta",
                        value: function (t) {
                            var e,
                                i = this[this.context] && this[this.context].gestureDirection ? this[this.context].gestureDirection : this.gestureDirection;
                            (e = "both" === i ? t.deltaX + t.deltaY : "vertical" === i ? t.deltaY : "horizontal" === i ? t.deltaX : t.deltaY),
                                (this.instance.delta[this.directionAxis] -= e * this.multiplier),
                                this.instance.delta[this.directionAxis] < 0 && (this.instance.delta[this.directionAxis] = 0),
                                this.instance.delta[this.directionAxis] > this.instance.limit[this.directionAxis] && (this.instance.delta[this.directionAxis] = this.instance.limit[this.directionAxis]);
                        },
                    },
                    {
                        key: "updateScroll",
                        value: function (t) {
                            this.isScrolling || this.isDraggingScrollbar
                                ? (this.instance.scroll[this.directionAxis] = bt(this.instance.scroll[this.directionAxis], this.instance.delta[this.directionAxis], this.lerp))
                                : this.instance.scroll[this.directionAxis] > this.instance.limit[this.directionAxis]
                                ? this.setScroll(this.instance.scroll[this.directionAxis], this.instance.limit[this.directionAxis])
                                : this.instance.scroll.y < 0
                                ? this.setScroll(this.instance.scroll[this.directionAxis], 0)
                                : this.setScroll(this.instance.scroll[this.directionAxis], this.instance.delta[this.directionAxis]);
                        },
                    },
                    {
                        key: "addDirection",
                        value: function () {
                            this.instance.delta.y > this.instance.scroll.y
                                ? "down" !== this.instance.direction && (this.instance.direction = "down")
                                : this.instance.delta.y < this.instance.scroll.y && "up" !== this.instance.direction && (this.instance.direction = "up"),
                                this.instance.delta.x > this.instance.scroll.x
                                    ? "right" !== this.instance.direction && (this.instance.direction = "right")
                                    : this.instance.delta.x < this.instance.scroll.x && "left" !== this.instance.direction && (this.instance.direction = "left");
                        },
                    },
                    {
                        key: "addSpeed",
                        value: function () {
                            this.instance.delta[this.directionAxis] != this.instance.scroll[this.directionAxis]
                                ? (this.instance.speed = (this.instance.delta[this.directionAxis] - this.instance.scroll[this.directionAxis]) / Math.max(1, Date.now() - this.speedTs))
                                : (this.instance.speed = 0);
                        },
                    },
                    {
                        key: "initScrollBar",
                        value: function () {
                            if (
                                ((this.scrollbar = document.createElement("span")),
                                (this.scrollbarThumb = document.createElement("span")),
                                this.scrollbar.classList.add("".concat(this.scrollbarClass)),
                                this.scrollbarThumb.classList.add("".concat(this.scrollbarClass, "_thumb")),
                                this.scrollbar.append(this.scrollbarThumb),
                                this.scrollbarContainer ? this.scrollbarContainer.append(this.scrollbar) : document.body.append(this.scrollbar),
                                (this.getScrollBar = this.getScrollBar.bind(this)),
                                (this.releaseScrollBar = this.releaseScrollBar.bind(this)),
                                (this.moveScrollBar = this.moveScrollBar.bind(this)),
                                this.scrollbarThumb.addEventListener("mousedown", this.getScrollBar),
                                window.addEventListener("mouseup", this.releaseScrollBar),
                                window.addEventListener("mousemove", this.moveScrollBar),
                                (this.hasScrollbar = !1),
                                "horizontal" == this.direction)
                            ) {
                                if (this.instance.limit.x + this.windowWidth <= this.windowWidth) return;
                            } else if (this.instance.limit.y + this.windowHeight <= this.windowHeight) return;
                            (this.hasScrollbar = !0),
                                (this.scrollbarBCR = this.scrollbar.getBoundingClientRect()),
                                (this.scrollbarHeight = this.scrollbarBCR.height),
                                (this.scrollbarWidth = this.scrollbarBCR.width),
                                "horizontal" === this.direction
                                    ? (this.scrollbarThumb.style.width = "".concat((this.scrollbarWidth * this.scrollbarWidth) / (this.instance.limit.x + this.scrollbarWidth), "px"))
                                    : (this.scrollbarThumb.style.height = "".concat((this.scrollbarHeight * this.scrollbarHeight) / (this.instance.limit.y + this.scrollbarHeight), "px")),
                                (this.scrollbarThumbBCR = this.scrollbarThumb.getBoundingClientRect()),
                                (this.scrollBarLimit = { x: this.scrollbarWidth - this.scrollbarThumbBCR.width, y: this.scrollbarHeight - this.scrollbarThumbBCR.height });
                        },
                    },
                    {
                        key: "reinitScrollBar",
                        value: function () {
                            if (((this.hasScrollbar = !1), "horizontal" == this.direction)) {
                                if (this.instance.limit.x + this.windowWidth <= this.windowWidth) return;
                            } else if (this.instance.limit.y + this.windowHeight <= this.windowHeight) return;
                            (this.hasScrollbar = !0),
                                (this.scrollbarBCR = this.scrollbar.getBoundingClientRect()),
                                (this.scrollbarHeight = this.scrollbarBCR.height),
                                (this.scrollbarWidth = this.scrollbarBCR.width),
                                "horizontal" === this.direction
                                    ? (this.scrollbarThumb.style.width = "".concat((this.scrollbarWidth * this.scrollbarWidth) / (this.instance.limit.x + this.scrollbarWidth), "px"))
                                    : (this.scrollbarThumb.style.height = "".concat((this.scrollbarHeight * this.scrollbarHeight) / (this.instance.limit.y + this.scrollbarHeight), "px")),
                                (this.scrollbarThumbBCR = this.scrollbarThumb.getBoundingClientRect()),
                                (this.scrollBarLimit = { x: this.scrollbarWidth - this.scrollbarThumbBCR.width, y: this.scrollbarHeight - this.scrollbarThumbBCR.height });
                        },
                    },
                    {
                        key: "destroyScrollBar",
                        value: function () {
                            this.scrollbarThumb.removeEventListener("mousedown", this.getScrollBar),
                                window.removeEventListener("mouseup", this.releaseScrollBar),
                                window.removeEventListener("mousemove", this.moveScrollBar),
                                this.scrollbar.remove();
                        },
                    },
                    {
                        key: "getScrollBar",
                        value: function (t) {
                            (this.isDraggingScrollbar = !0), this.checkScroll(), this.html.classList.remove(this.scrollingClass), this.html.classList.add(this.draggingClass);
                        },
                    },
                    {
                        key: "releaseScrollBar",
                        value: function (t) {
                            (this.isDraggingScrollbar = !1), this.html.classList.add(this.scrollingClass), this.html.classList.remove(this.draggingClass);
                        },
                    },
                    {
                        key: "moveScrollBar",
                        value: function (t) {
                            var e = this;
                            this.isDraggingScrollbar &&
                                requestAnimationFrame(function () {
                                    var i = (((100 * (t.clientX - e.scrollbarBCR.left)) / e.scrollbarWidth) * e.instance.limit.x) / 100,
                                        n = (((100 * (t.clientY - e.scrollbarBCR.top)) / e.scrollbarHeight) * e.instance.limit.y) / 100;
                                    n > 0 && n < e.instance.limit.y && (e.instance.delta.y = n), i > 0 && i < e.instance.limit.x && (e.instance.delta.x = i);
                                });
                        },
                    },
                    {
                        key: "addElements",
                        value: function () {
                            var t = this;
                            (this.els = {}),
                                (this.parallaxElements = {}),
                                this.el.querySelectorAll("[data-".concat(this.name, "]")).forEach(function (e, i) {
                                    var n,
                                        s,
                                        o,
                                        r = xt(e),
                                        a = Object.entries(t.sections)
                                            .map(function (t) {
                                                var e = z(t, 2);
                                                e[0];
                                                return e[1];
                                            })
                                            .find(function (t) {
                                                return r.includes(t.el);
                                            }),
                                        l = e.dataset[t.name + "Class"] || t.class,
                                        c = "string" == typeof e.dataset[t.name + "Id"] ? e.dataset[t.name + "Id"] : "el" + i,
                                        h = e.dataset[t.name + "Repeat"],
                                        u = e.dataset[t.name + "Call"],
                                        d = e.dataset[t.name + "Position"],
                                        f = e.dataset[t.name + "Delay"],
                                        p = e.dataset[t.name + "Direction"],
                                        v = "string" == typeof e.dataset[t.name + "Sticky"],
                                        m = !!e.dataset[t.name + "Speed"] && parseFloat(e.dataset[t.name + "Speed"]) / 10,
                                        g = "string" == typeof e.dataset[t.name + "Offset"] ? e.dataset[t.name + "Offset"].split(",") : t.offset,
                                        y = e.dataset[t.name + "Target"],
                                        w = (o = void 0 !== y ? document.querySelector("".concat(y)) : e).getBoundingClientRect();
                                    null === a || a.inView ? ((n = w.top + t.instance.scroll.y - kt(o).y), (s = w.left + t.instance.scroll.x - kt(o).x)) : ((n = w.top - kt(a.el).y - kt(o).y), (s = w.left - kt(a.el).x - kt(o).x));
                                    var b = n + o.offsetHeight,
                                        k = s + o.offsetWidth,
                                        x = { x: (k - s) / 2 + s, y: (b - n) / 2 + n };
                                    if (v) {
                                        var E = e.getBoundingClientRect(),
                                            T = E.top,
                                            S = E.left,
                                            A = { x: S - s, y: T - n };
                                        (n += window.innerHeight),
                                            (s += window.innerWidth),
                                            (b = T + o.offsetHeight - e.offsetHeight - A[t.directionAxis]),
                                            (x = { x: ((k = S + o.offsetWidth - e.offsetWidth - A[t.directionAxis]) - s) / 2 + s, y: (b - n) / 2 + n });
                                    }
                                    h = "false" != h && (null != h || t.repeat);
                                    var C = [0, 0];
                                    if (g)
                                        if ("horizontal" === t.direction) {
                                            for (var M = 0; M < g.length; M++) "string" == typeof g[M] ? (g[M].includes("%") ? (C[M] = parseInt((g[M].replace("%", "") * t.windowWidth) / 100)) : (C[M] = parseInt(g[M]))) : (C[M] = g[M]);
                                            (s += C[0]), (k -= C[1]);
                                        } else {
                                            for (M = 0; M < g.length; M++) "string" == typeof g[M] ? (g[M].includes("%") ? (C[M] = parseInt((g[M].replace("%", "") * t.windowHeight) / 100)) : (C[M] = parseInt(g[M]))) : (C[M] = g[M]);
                                            (n += C[0]), (b -= C[1]);
                                        }
                                    var L = {
                                        el: e,
                                        id: c,
                                        class: l,
                                        section: a,
                                        top: n,
                                        middle: x,
                                        bottom: b,
                                        left: s,
                                        right: k,
                                        offset: g,
                                        progress: 0,
                                        repeat: h,
                                        inView: !1,
                                        call: u,
                                        speed: m,
                                        delay: f,
                                        position: d,
                                        target: o,
                                        direction: p,
                                        sticky: v,
                                    };
                                    (t.els[c] = L), e.classList.contains(l) && t.setInView(t.els[c], c), (!1 !== m || v) && (t.parallaxElements[c] = L);
                                });
                        },
                    },
                    {
                        key: "addSections",
                        value: function () {
                            var t = this;
                            this.sections = {};
                            var e = this.el.querySelectorAll("[data-".concat(this.name, "-section]"));
                            0 === e.length && (e = [this.el]),
                                e.forEach(function (e, i) {
                                    var n = "string" == typeof e.dataset[t.name + "Id"] ? e.dataset[t.name + "Id"] : "section" + i,
                                        s = e.getBoundingClientRect(),
                                        o = { x: s.left - 1.5 * window.innerWidth - kt(e).x, y: s.top - 1.5 * window.innerHeight - kt(e).y },
                                        r = { x: o.x + s.width + 2 * window.innerWidth, y: o.y + s.height + 2 * window.innerHeight },
                                        a = "string" == typeof e.dataset[t.name + "Persistent"];
                                    e.setAttribute("data-scroll-section-id", n);
                                    var l = { el: e, offset: o, limit: r, inView: !1, persistent: a, id: n };
                                    t.sections[n] = l;
                                });
                        },
                    },
                    {
                        key: "transform",
                        value: function (t, e, i, n) {
                            var s;
                            if (n) {
                                var o = kt(t),
                                    r = bt(o.x, e, n),
                                    a = bt(o.y, i, n);
                                s = "matrix3d(1,0,0.00,0,0.00,1,0.00,0,0,0,1,0,".concat(r, ",").concat(a, ",0,1)");
                            } else s = "matrix3d(1,0,0.00,0,0.00,1,0.00,0,0,0,1,0,".concat(e, ",").concat(i, ",0,1)");
                            (t.style.webkitTransform = s), (t.style.msTransform = s), (t.style.transform = s);
                        },
                    },
                    {
                        key: "transformElements",
                        value: function (t) {
                            var e = this,
                                i = arguments.length > 1 && void 0 !== arguments[1] && arguments[1],
                                n = this.instance.scroll.x + this.windowWidth,
                                s = this.instance.scroll.y + this.windowHeight,
                                o = { x: this.instance.scroll.x + this.windowMiddle.x, y: this.instance.scroll.y + this.windowMiddle.y };
                            Object.entries(this.parallaxElements).forEach(function (r) {
                                var a = z(r, 2),
                                    l = (a[0], a[1]),
                                    c = !1;
                                if ((t && (c = 0), l.inView || i))
                                    switch (l.position) {
                                        case "top":
                                            c = e.instance.scroll[e.directionAxis] * -l.speed;
                                            break;
                                        case "elementTop":
                                            c = (s - l.top) * -l.speed;
                                            break;
                                        case "bottom":
                                            c = (e.instance.limit[e.directionAxis] - s + e.windowHeight) * l.speed;
                                            break;
                                        case "left":
                                            c = e.instance.scroll[e.directionAxis] * -l.speed;
                                            break;
                                        case "elementLeft":
                                            c = (n - l.left) * -l.speed;
                                            break;
                                        case "right":
                                            c = (e.instance.limit[e.directionAxis] - n + e.windowHeight) * l.speed;
                                            break;
                                        default:
                                            c = (o[e.directionAxis] - l.middle[e.directionAxis]) * -l.speed;
                                    }
                                l.sticky &&
                                    (c = l.inView
                                        ? "horizontal" === e.direction
                                            ? e.instance.scroll.x - l.left + window.innerWidth
                                            : e.instance.scroll.y - l.top + window.innerHeight
                                        : "horizontal" === e.direction
                                        ? e.instance.scroll.x < l.left - window.innerWidth && e.instance.scroll.x < l.left - window.innerWidth / 2
                                            ? 0
                                            : e.instance.scroll.x > l.right && e.instance.scroll.x > l.right + 100 && l.right - l.left + window.innerWidth
                                        : e.instance.scroll.y < l.top - window.innerHeight && e.instance.scroll.y < l.top - window.innerHeight / 2
                                        ? 0
                                        : e.instance.scroll.y > l.bottom && e.instance.scroll.y > l.bottom + 100 && l.bottom - l.top + window.innerHeight),
                                    !1 !== c && ("horizontal" === l.direction || ("horizontal" === e.direction && "vertical" !== l.direction) ? e.transform(l.el, c, 0, !t && l.delay) : e.transform(l.el, 0, c, !t && l.delay));
                            });
                        },
                    },
                    {
                        key: "scrollTo",
                        value: function (t) {
                            var e = this,
                                i = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {},
                                n = parseInt(i.offset) || 0,
                                s = i.duration || 1e3,
                                o = i.easing || [0.25, 0, 0.35, 1],
                                r = !!i.disableLerp,
                                a = !!i.callback && i.callback;
                            if (((o = Ot.apply(void 0, G(o))), "string" == typeof t)) {
                                if ("top" === t) t = 0;
                                else if ("bottom" === t) t = this.instance.limit.y;
                                else if ("left" === t) t = 0;
                                else if ("right" === t) t = this.instance.limit.x;
                                else if (!(t = document.querySelector(t))) return;
                            } else if ("number" == typeof t) t = parseInt(t);
                            else if (!t || !t.tagName) return void console.warn("`target` parameter is not valid");
                            if ("number" != typeof t) {
                                var l = xt(t).includes(this.el);
                                if (!l) return;
                                var c = t.getBoundingClientRect(),
                                    h = c.top,
                                    u = c.left,
                                    d = xt(t),
                                    f = d.find(function (t) {
                                        return Object.entries(e.sections)
                                            .map(function (t) {
                                                var e = z(t, 2);
                                                e[0];
                                                return e[1];
                                            })
                                            .find(function (e) {
                                                return e.el == t;
                                            });
                                    }),
                                    p = 0;
                                (p = f ? kt(f)[this.directionAxis] : -this.instance.scroll[this.directionAxis]), (n = "horizontal" === this.direction ? u + n - p : h + n - p);
                            } else n = t + n;
                            var v = parseFloat(this.instance.delta[this.directionAxis]),
                                m = Math.max(0, Math.min(n, this.instance.limit[this.directionAxis])),
                                g = m - v,
                                y = function (t) {
                                    r ? ("horizontal" === e.direction ? e.setScroll(v + g * t, e.instance.delta.y) : e.setScroll(e.instance.delta.x, v + g * t)) : (e.instance.delta[e.directionAxis] = v + g * t);
                                };
                            (this.animatingScroll = !0), this.stopScrolling(), this.startScrolling();
                            var w = Date.now(),
                                b = function t() {
                                    var i = (Date.now() - w) / s;
                                    i > 1 ? (y(1), (e.animatingScroll = !1), 0 == s && e.update(), a && a()) : ((e.scrollToRaf = requestAnimationFrame(t)), y(o(i)));
                                };
                            b();
                        },
                    },
                    {
                        key: "update",
                        value: function () {
                            this.setScrollLimit(), this.addSections(), this.addElements(), this.detectElements(), this.updateScroll(), this.transformElements(!0), this.reinitScrollBar(), this.checkScroll(!0);
                        },
                    },
                    {
                        key: "startScroll",
                        value: function () {
                            this.stop = !1;
                        },
                    },
                    {
                        key: "stopScroll",
                        value: function () {
                            this.stop = !0;
                        },
                    },
                    {
                        key: "setScroll",
                        value: function (t, e) {
                            this.instance = B(B({}, this.instance), {}, { scroll: { x: t, y: e }, delta: { x: t, y: e }, speed: 0 });
                        },
                    },
                    {
                        key: "destroy",
                        value: function () {
                            U(j(i.prototype), "destroy", this).call(this),
                                this.stopScrolling(),
                                this.html.classList.remove(this.smoothClass),
                                this.vs.destroy(),
                                this.destroyScrollBar(),
                                window.removeEventListener("keydown", this.checkKey, !1);
                        },
                    },
                ]),
                i
            );
        })(q),
        Nt = (function () {
            function t() {
                var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
                O(this, t),
                    (this.options = e),
                    Object.assign(this, Y, e),
                    (this.smartphone = Y.smartphone),
                    e.smartphone && Object.assign(this.smartphone, e.smartphone),
                    (this.tablet = Y.tablet),
                    e.tablet && Object.assign(this.tablet, e.tablet),
                    this.smooth || "horizontal" != this.direction || console.warn("ðŸš¨ `smooth:false` & `horizontal` direction are not yet compatible"),
                    this.tablet.smooth || "horizontal" != this.tablet.direction || console.warn("ðŸš¨ `smooth:false` & `horizontal` direction are not yet compatible (tablet)"),
                    this.smartphone.smooth || "horizontal" != this.smartphone.direction || console.warn("ðŸš¨ `smooth:false` & `horizontal` direction are not yet compatible (smartphone)"),
                    this.init();
            }
            return (
                _(t, [
                    {
                        key: "init",
                        value: function () {
                            if (
                                ((this.options.isMobile =
                                    /Android|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) || ("MacIntel" === navigator.platform && navigator.maxTouchPoints > 1) || window.innerWidth < this.tablet.breakpoint),
                                (this.options.isTablet = this.options.isMobile && window.innerWidth >= this.tablet.breakpoint),
                                (this.smooth && !this.options.isMobile) || (this.tablet.smooth && this.options.isTablet) || (this.smartphone.smooth && this.options.isMobile && !this.options.isTablet)
                                    ? (this.scroll = new Ht(this.options))
                                    : (this.scroll = new Q(this.options)),
                                this.scroll.init(),
                                window.location.hash)
                            ) {
                                var t = window.location.hash.slice(1, window.location.hash.length),
                                    e = document.getElementById(t);
                                e && this.scroll.scrollTo(e);
                            }
                        },
                    },
                    {
                        key: "update",
                        value: function () {
                            this.scroll.update();
                        },
                    },
                    {
                        key: "start",
                        value: function () {
                            this.scroll.startScroll();
                        },
                    },
                    {
                        key: "stop",
                        value: function () {
                            this.scroll.stopScroll();
                        },
                    },
                    {
                        key: "scrollTo",
                        value: function (t, e) {
                            this.scroll.scrollTo(t, e);
                        },
                    },
                    {
                        key: "setScroll",
                        value: function (t, e) {
                            this.scroll.setScroll(t, e);
                        },
                    },
                    {
                        key: "on",
                        value: function (t, e) {
                            this.scroll.setEvents(t, e);
                        },
                    },
                    {
                        key: "off",
                        value: function (t, e) {
                            this.scroll.unsetEvents(t, e);
                        },
                    },
                    {
                        key: "destroy",
                        value: function () {
                            this.scroll.destroy();
                        },
                    },
                ]),
                t
            );
        })();
    function Ft(t, e, i) {
        return (1 - i) * t + i * e;
    }
    var Ut = (function (t) {
            g(i, t);
            var e = k(i);
            function i(t) {
                return u(this, i), e.call(this, t);
            }
            return (
                f(i, [
                    {
                        key: "init",
                        value: function () {
                            var t = this;
                            window.scrollTo(0, 0),
                                (this.hasScrolledValue = window.isMobile ? 50 : 200),
                                setTimeout(function () {
                                    (t.speed = 0),
                                        (t.lerpedSpeed = 0),
                                        t.render(),
                                        (t.scroll = new Nt({ el: t.el, smooth: !1, offset: ["10%"], getSpeed: !0 })),
                                        t.scroll.on("call", function (e, i, n, s) {
                                            t.call(e[0], { way: i, obj: n }, e[1], e[2]);
                                        }),
                                        t.scroll.on("scroll", function (e) {
                                            e.scroll.y > t.hasScrolledValue ? M.classList.add("has-scrolled") : M.classList.remove("has-scrolled"), isNaN(e.speed) || (t.speed = e.speed);
                                        });
                                }, 600);
                        },
                    },
                    {
                        key: "render",
                        value: function () {
                            var t = this;
                            (this.raf = requestAnimationFrame(function () {
                                return t.render();
                            })),
                                (this.speed = Ft(this.speed, 0, 0.1)),
                                (this.lerpedSpeed = Ft(this.lerpedSpeed, this.speed, 0.1)),
                                Math.abs(this.lerpedSpeed) < 0.1 ? M.classList.remove("is-scrolling") : M.classList.add("is-scrolling");
                        },
                    },
                    {
                        key: "toggleLazy",
                        value: function (t) {
                            var e = this.getData("lazy", t.obj.el);
                            e.length && ("IMG" == t.obj.el.tagName ? (t.obj.el.src = e) : (t.obj.el.style.backgroundImage = "url(".concat(e, ")")), this.setData("lazy", "", t.obj.el));
                        },
                    },
                    {
                        key: "scrollTo",
                        value: function (t) {
                            this.scroll.scrollTo(t);
                        },
                    },
                    {
                        key: "update",
                        value: function () {
                            this.scroll && this.scroll.update && this.scroll.update();
                        },
                    },
                    {
                        key: "destroy",
                        value: function () {
                            this.scroll.destroy(), void 0 !== this.raf && cancelAnimationFrame(this.raf);
                        },
                    },
                ]),
                i
            );
        })(c),
        zt = "has-widget-user-open",
        Gt = (function (t) {
            g(i, t);
            var e = k(i);
            function i(t) {
                var n;
                return u(this, i), ((n = e.call(this, t)).events = { click: { button: "toggleWidget", inner: "toggleWidget", close: "close" } }), n;
            }
            return (
                f(i, [
                    {
                        key: "init",
                        value: function () {
                            this.isOpen = !1;
                        },
                    },
                    {
                        key: "toggleWidget",
                        value: function (t) {
                            (!t || ("A" !== t.target.tagName && "BUTTON" !== t.target.tagName)) && (M.classList.contains("has-widget-open") || M.classList.contains("-footer") || (this.isOpen ? this.close() : this.open()));
                        },
                    },
                    {
                        key: "open",
                        value: function () {
                            (this.isOpen = !0), M.classList.add(zt);
                        },
                    },
                    {
                        key: "close",
                        value: function () {
                            (this.isOpen = !1), M.classList.remove(zt);
                        },
                    },
                ]),
                i
            );
        })(c),
        Vt = (function (t) {
            g(i, t);
            var e = k(i);
            function i(t) {
                var n;
                return u(this, i), ((n = e.call(this, t)).events = { click: "toggle" }), n;
            }
            return (
                f(i, [
                    { key: "init", value: function () {} },
                    {
                        key: "toggle",
                        value: function () {
                            this.call("toggleWidget", "WidgetUser");
                        },
                    },
                    { key: "destroy", value: function () {} },
                ]),
                i
            );
        })(c),
        Xt = "has-nav-open",
        Yt = (function (t) {
            g(i, t);
            var e = k(i);
            function i(t) {
                var n;
                return u(this, i), ((n = e.call(this, t)).events = { click: { closeButton: "close" } }), n;
            }
            return (
                f(i, [
                    {
                        key: "init",
                        value: function () {
                            var t = this;
                            (this.closeBind = function (e) {
                                "Escape" === e.key && t.close();
                            }),
                                document.addEventListener("keyup", this.closeBind);
                        },
                    },
                    {
                        key: "open",
                        value: function () {
                            M.classList.add(Xt), this.call("close", null, "WidgetUser");
                        },
                    },
                    {
                        key: "close",
                        value: function () {
                            M.classList.remove(Xt);
                        },
                    },
                    {
                        key: "destroy",
                        value: function () {
                            document.removeEventListener("keyup", this.closeBind);
                        },
                    },
                ]),
                i
            );
        })(c),
        qt = (function (t) {
            g(i, t);
            var e = k(i);
            function i(t) {
                var n;
                return u(this, i), ((n = e.call(this, t)).events = { click: { toggler: "toggleMenu" } }), n;
            }
            return (
                f(i, [
                    { key: "init", value: function () {} },
                    {
                        key: "toggleMenu",
                        value: function () {
                            M.classList.contains("has-nav-open") ? this.close() : this.open();
                        },
                    },
                    {
                        key: "open",
                        value: function () {
                            this.call("open", null, "Menu");
                        },
                    },
                    {
                        key: "close",
                        value: function () {
                            this.call("close", null, "Menu");
                        },
                    },
                ]),
                i
            );
        })(c),
        $t = "has-menu-tools-open",
        Kt = (function (t) {
            g(i, t);
            var e = k(i);
            function i(t) {
                var n;
                return u(this, i), ((n = e.call(this, t)).events = { click: { closeButton: "close" } }), n;
            }
            return (
                f(i, [
                    { key: "init", value: function () {} },
                    {
                        key: "open",
                        value: function () {
                            M.classList.add($t);
                        },
                    },
                    {
                        key: "close",
                        value: function () {
                            M.classList.remove($t);
                        },
                    },
                    { key: "destroy", value: function () {} },
                ]),
                i
            );
        })(c),
        Jt = (function (t) {
            g(i, t);
            var e = k(i);
            function i(t) {
                var n;
                return u(this, i), ((n = e.call(this, t)).events = { click: "toggleMenu" }), n;
            }
            return (
                f(i, [
                    { key: "init", value: function () {} },
                    {
                        key: "toggleMenu",
                        value: function () {
                            M.classList.contains("has-menu-tools-open") ? this.close() : this.open();
                        },
                    },
                    {
                        key: "open",
                        value: function () {
                            this.call("open", "MenuTools");
                        },
                    },
                    {
                        key: "close",
                        value: function () {
                            this.call("close", "MenuTools");
                        },
                    },
                ]),
                i
            );
        })(c),
        Qt = (function (t) {
            g(i, t);
            var e = k(i);
            function i(t) {
                var n;
                return u(this, i), ((n = e.call(this, t)).events = { click: "escape" }), n;
            }
            return (
                f(i, [
                    {
                        key: "init",
                        value: function () {
                            this.urls = [
                                "https://erableduquebec.ca/recettes/gratin-de-navets-et-de-pommes-a-lerable/",
                                "https://www.ricardocuisine.com/recettes/5864-crepes-fines-a-dejeuner",
                                "https://www.lepanierbleu.ca/produits",
                                "https://www.ikea.com/ca/en/cat/shelf-units-11465/",
                                "http://meteomedia.ca",
                            ];
                            var t = Math.round(Math.random() * (this.urls.length - 1));
                            this.el.href = this.urls[t];
                        },
                    },
                    {
                        key: "escape",
                        value: function () {
                            window.gtag && gtag("event", "escape", { event_category: "ui_actions" }), (M.innerHTML = ""), (window.document.title = "");
                        },
                    },
                ]),
                i
            );
        })(c),
        Zt = (function (t) {
            g(i, t);
            var e = k(i);
            function i(t) {
                var n;
                return u(this, i), ((n = e.call(this, t)).events = { click: { prev: "prev", next: "next" } }), n;
            }
            return (
                f(i, [
                    {
                        key: "init",
                        value: function () {
                            var t = this,
                                e = 1;
                            (e =
                                "small" === M.getAttribute("data-container-size") && "sos-info" === this.el.getAttribute("data-module-carousel")
                                    ? 1.5
                                    : "small" === M.getAttribute("data-container-size") && "news" === this.el.getAttribute("data-module-carousel")
                                    ? 2.5
                                    : this.getData("slides")),
                                (this.swiper = new Swiper(this.$("slideshow")[0], {
                                    slidesPerView: 1.1,
                                    speed: 600,
                                    scrollbar: { el: this.$("scrollbar")[0], draggable: !0 },
                                    breakpoints: { 1e3: { slidesPerView: 3 === this.getData("slides") ? 2 : this.getData("slides") }, 1200: { slidesPerView: e } },
                                })),
                                this.swiper.on("slideChange", function () {
                                    t.check();
                                }),
                                this.check();
                        },
                    },
                    {
                        key: "check",
                        value: function () {
                            this.swiper.isBeginning ? this.el.classList.add("is-beginning") : this.el.classList.remove("is-beginning"), this.swiper.isEnd ? this.el.classList.add("is-end") : this.el.classList.remove("is-end");
                        },
                    },
                    {
                        key: "prev",
                        value: function () {
                            this.swiper.slidePrev();
                        },
                    },
                    {
                        key: "next",
                        value: function () {
                            this.swiper.slideNext();
                        },
                    },
                ]),
                i
            );
        })(c),
        te = (function (t) {
            g(i, t);
            var e = k(i);
            function i(t) {
                var n;
                return u(this, i), ((n = e.call(this, t)).events = { click: { prev: "prev", next: "next" } }), n;
            }
            return (
                f(i, [
                    {
                        key: "init",
                        value: function () {
                            var t = this;
                            this.swiper = new Swiper(this.$("slideshow")[0], {
                                effect: "fade",
                                autoHeight: !0,
                                speed: 900,
                                allowTouchMove: !0,
                                pagination: { el: this.$("pagination"), type: "bullets", clickable: !0 },
                                breakpoints: { 1e3: {} },
                                on: {
                                    slideChangeTransitionEnd: function (e) {
                                        t.call("update", null, "scrollTo");
                                    },
                                },
                            });
                        },
                    },
                    {
                        key: "prev",
                        value: function () {
                            this.swiper.slidePrev();
                        },
                    },
                    {
                        key: "next",
                        value: function () {
                            this.swiper.slideNext();
                        },
                    },
                ]),
                i
            );
        })(c),
        ee = (function (t) {
            g(i, t);
            var e = k(i);
            function i(t) {
                var n;
                return u(this, i), ((n = e.call(this, t)).events = { click: { toggler: "toggle" } }), n;
            }
            return (
                f(i, [
                    { key: "init", value: function () {} },
                    {
                        key: "toggle",
                        value: function (t) {
                            var e = t.curTarget,
                                i = this.parent("item", e);
                            i.classList.contains("is-open") ? this.close() : (this.close(), this.open(i));
                        },
                    },
                    {
                        key: "open",
                        value: function (t) {
                            var e = this,
                                i = this.$("content", t)[0];
                            t.classList.add("is-open"),
                                gsap.to(i, 0.3, {
                                    height: this.compute(this.$("inner", t)[0]).height,
                                    onComplete: function () {
                                        e.call("update", "Scroll");
                                    },
                                });
                        },
                    },
                    {
                        key: "close",
                        value: function () {
                            var t = this;
                            this.$("item").forEach(function (e) {
                                e.classList.remove("is-open");
                                var i = t.$("content", e)[0];
                                i &&
                                    gsap.to(i, 0.3, {
                                        height: 0,
                                        onComplete: function () {
                                            t.call("update", "Scroll");
                                        },
                                    });
                            });
                        },
                    },
                    {
                        key: "compute",
                        value: function (t) {
                            return t.getBoundingClientRect();
                        },
                    },
                ]),
                i
            );
        })(c);
    var ie = (function () {
        function t(e, i, n) {
            u(this, t),
                (this.el = e),
                (this.isFixed = n),
                (this.containerWidth = 0),
                (this.requestAnimation = null),
                (this.scrollPosition = -1),
                (this.translation = 0),
                (this.grabbed = !1),
                (this.preventClick = !1),
                (this.paused = !0),
                (this.originalVelocity = -i),
                (this.velocity = this.originalVelocity),
                (this.orientation = Math.sign(this.velocity)),
                (this.lastDelta = null),
                this.initializeElements();
        }
        return (
            f(t, [
                {
                    key: "launch",
                    value: function () {
                        this.initializeEvents(), this.updateOrientation(), this.resume();
                    },
                },
                {
                    key: "initializeElements",
                    value: function () {
                        (this.$railMove = this.el.querySelectorAll(".js-rail-group-container")), (this.$railMoveChildren = this.el.querySelectorAll(".js-rail-item")), this.getBCR();
                    },
                },
                {
                    key: "initializeEvents",
                    value: function () {
                        this.update();
                    },
                },
                {
                    key: "setContainerWidth",
                    value: function (t) {
                        this.containerWidth = t;
                    },
                },
                {
                    key: "getBCR",
                    value: function () {
                        this.railMoveBCR = this.$railMove[0].getBoundingClientRect();
                    },
                },
                {
                    key: "updateScroll",
                    value: function (t) {
                        if (!this.paused && document.documentElement.classList.contains("has-scroll-smooth") && !this.isFixed) {
                            var e = Math.round(t.scroll.y) ? Math.round(t.scroll.y) : 0,
                                i = e - this.scrollPosition;
                            if (((this.scrollPosition = e), 0 != i)) {
                                var n = this.originalVelocity * i;
                                (this.velocity = n), Math.sign(n) != this.orientation && ((this.orientation = Math.sign(n)), this.updateOrientation());
                            }
                        }
                    },
                },
                {
                    key: "pause",
                    value: function () {
                        (this.paused = !0), this.el.classList.add("is-paused"), void 0 !== this.requestAnimation && cancelAnimationFrame(this.requestAnimation);
                    },
                },
                {
                    key: "resume",
                    value: function () {
                        this.el.classList.remove("is-paused"), (this.paused = !1), this.update();
                    },
                },
                {
                    key: "update",
                    value: function () {
                        var t,
                            e = this;
                        this.grabbed || (this.translation > this.railMoveBCR.width / 2 || this.translation < -this.railMoveBCR.width / 2 ? (this.translation = 0) : (this.translation = this.translation + this.velocity)),
                            (t = this.translation > 0 ? -this.containerWidth + (this.translation % this.containerWidth) : this.translation % this.containerWidth),
                            TweenMax.set(this.$railMoveChildren, { x: Math.round(t), force3D: !0 }),
                            this.paused ||
                                (this.requestAnimation = requestAnimationFrame(function () {
                                    return e.update();
                                }));
                    },
                },
                {
                    key: "updateOrientation",
                    value: function () {
                        this.orientation > 0
                            ? (this.el.classList.remove("is-going-left"), this.el.classList.add("is-going-right"))
                            : this.orientation < 0 && (this.el.classList.remove("is-going-right"), this.el.classList.add("is-going-left"));
                    },
                },
                {
                    key: "destroy",
                    value: function () {
                        void 0 !== this.requestAnimation && cancelAnimationFrame(this.requestAnimation), TweenMax.set(this.$railMove, { x: 0 }), (this.translation = 0);
                    },
                },
            ]),
            t
        );
    })();
    var ne = (function (t) {
            g(i, t);
            var e = k(i);
            function i(t) {
                var n;
                return (
                    u(this, i),
                    ((n = e.call(this, t)).speed = n.el.getAttribute("data-rail-speed") || 1),
                    (n.isFixed = "string" == typeof n.el.getAttribute("data-fixed")),
                    (n.shuffle = "string" == typeof n.el.getAttribute("data-shuffle")),
                    (n.paused = "string" == typeof n.el.getAttribute("data-paused")),
                    (n.isMobile = "string" != typeof n.el.getAttribute("data-desktop")),
                    (n.initialHTML = n.el.outerHTML),
                    (n.launched = !1),
                    (n.active = !1),
                    n
                );
            }
            return (
                f(i, [
                    {
                        key: "init",
                        value: function () {
                            (window.isMobile && !this.isMobile) ||
                                (this.setClasses(),
                                this.createTrack(),
                                this.fillScreen(),
                                this.groupTracks(),
                                this.duplicateGroup(),
                                this.wrapGroups(),
                                (this.railMove = new ie(this.el, this.speed, this.isFixed)),
                                this.railMove.setContainerWidth(this.trackGroupBCR.width),
                                this.paused && this.railMove.pause());
                        },
                    },
                    {
                        key: "setClasses",
                        value: function () {
                            this.el.classList.add("c-rail_wrapper"), (this.children = Array.from(this.el.children));
                            var t,
                                e = A(this.children);
                            try {
                                for (e.s(); !(t = e.n()).done; ) {
                                    var i = t.value;
                                    i.classList.add("c-rail_item"), i.classList.add("js-rail-item");
                                }
                            } catch (t) {
                                e.e(t);
                            } finally {
                                e.f();
                            }
                            if (null != this.shuffle) {
                                var n,
                                    s = A(
                                        (function (t) {
                                            for (var e = t.length - 1; e > 0; e--) {
                                                var i = Math.floor(Math.random() * (e + 1)),
                                                    n = [t[i], t[e]];
                                                (t[e] = n[0]), (t[i] = n[1]);
                                            }
                                            return t;
                                        })(this.children)
                                    );
                                try {
                                    for (s.s(); !(n = s.n()).done; ) {
                                        var o = n.value;
                                        this.el.appendChild(o);
                                    }
                                } catch (t) {
                                    s.e(t);
                                } finally {
                                    s.f();
                                }
                            }
                        },
                    },
                    {
                        key: "createTrack",
                        value: function () {
                            (this.track = document.createElement("div")), this.track.classList.add("c-rail_track"), this.el.appendChild(this.track), (this.tracks = []), this.tracks.push(this.track);
                            var t,
                                e = A(this.children);
                            try {
                                for (e.s(); !(t = e.n()).done; ) {
                                    var i = t.value;
                                    this.track.appendChild(i);
                                }
                            } catch (t) {
                                e.e(t);
                            } finally {
                                e.f();
                            }
                        },
                    },
                    {
                        key: "fillScreen",
                        value: function () {
                            var t = window.innerWidth / this.track.getBoundingClientRect().width;
                            if (t === 1 / 0) throw new Error("Ratio is Infinity");
                            for (var e = 0; e < t; e++) {
                                var i = this.track.cloneNode(!0);
                                this.tracks.push(i), this.el.appendChild(i);
                            }
                        },
                    },
                    {
                        key: "groupTracks",
                        value: function () {
                            (this.trackGroup = document.createElement("div")), this.trackGroup.classList.add("c-rail_track-group"), this.el.appendChild(this.trackGroup);
                            var t,
                                e = A(this.tracks);
                            try {
                                for (e.s(); !(t = e.n()).done; ) {
                                    var i = t.value;
                                    this.trackGroup.appendChild(i);
                                }
                            } catch (t) {
                                e.e(t);
                            } finally {
                                e.f();
                            }
                            this.trackGroupBCR = this.trackGroup.getBoundingClientRect();
                        },
                    },
                    {
                        key: "duplicateGroup",
                        value: function () {
                            (this.trackGroupClone = this.trackGroup.cloneNode(!0)), this.el.appendChild(this.trackGroupClone);
                        },
                    },
                    {
                        key: "wrapGroups",
                        value: function () {
                            (this.groupsWrap = document.createElement("div")), this.groupsWrap.classList.add("js-rail-group-container"), this.groupsWrap.classList.add("c-rail_group-container"), this.el.append(this.groupsWrap);
                            for (var t = 0, e = [this.trackGroup, this.trackGroupClone]; t < e.length; t++) {
                                var i = e[t];
                                this.groupsWrap.append(i);
                            }
                        },
                    },
                    {
                        key: "launch",
                        value: function () {
                            (this.launched = !0), this.railMove.launch(), this.lazyLoad();
                        },
                    },
                    {
                        key: "trigger",
                        value: function (t) {
                            t.obj.el != this.el ||
                                (window.isMobile && !this.isMobile) ||
                                ("enter" === t.way ? ((this.active = !0), this.launched ? (this.railMove.pause(), this.railMove.resume()) : this.launch()) : ((this.active = !1), this.railMove.pause()));
                        },
                    },
                    {
                        key: "lazyLoad",
                        value: function () {
                            var t = this;
                            this.lazy = this.$("lazy");
                            for (
                                var e = function () {
                                        var e = n[i],
                                            s = t.getData("lazy", e);
                                        if (s.length) {
                                            var o = new Image(),
                                                r = function () {
                                                    "IMG" == e.tagName ? (e.src = s) : (e.style.backgroundImage = "url(".concat(s, ")")),
                                                        requestAnimationFrame(function () {
                                                            requestAnimationFrame(function () {
                                                                var t = (function (t, e) {
                                                                    for (
                                                                        Element.prototype.matches ||
                                                                        (Element.prototype.matches =
                                                                            Element.prototype.matchesSelector ||
                                                                            Element.prototype.mozMatchesSelector ||
                                                                            Element.prototype.msMatchesSelector ||
                                                                            Element.prototype.oMatchesSelector ||
                                                                            Element.prototype.webkitMatchesSelector ||
                                                                            function (t) {
                                                                                for (var e = (this.document || this.ownerDocument).querySelectorAll(t), i = e.length; --i >= 0 && e.item(i) !== this; );
                                                                                return i > -1;
                                                                            });
                                                                        t && t !== document;
                                                                        t = t.parentNode
                                                                    )
                                                                        if (t.matches(e)) return t;
                                                                    return null;
                                                                })(e, ".o-lazy_wrapper");
                                                                t && t.classList.add("is-loaded"), e.classList.add("is-loaded");
                                                            });
                                                        });
                                                };
                                            o.decode
                                                ? ((o.src = s),
                                                  o
                                                      .decode()
                                                      .then(r)
                                                      .catch(function (t) {
                                                          console.error(t);
                                                      }))
                                                : ((o.onload = r), (o.src = s)),
                                                t.setData("lazy", "", e);
                                        }
                                    },
                                    i = 0,
                                    n = Array.from(this.lazy);
                                i < n.length;
                                i++
                            )
                                e();
                        },
                    },
                    {
                        key: "onScroll",
                        value: function (t) {
                            this.railMove.updateScroll(t);
                        },
                    },
                    {
                        key: "destroy",
                        value: function () {
                            x(y(i.prototype), "destroy", this).call(this);
                        },
                    },
                ]),
                i
            );
        })(c),
        se = (function (t) {
            g(i, t);
            var e = k(i);
            function i(t) {
                return u(this, i), e.call(this, t);
            }
            return (
                f(i, [
                    {
                        key: "init",
                        value: function () {
                            this.currentTheme = M.getAttribute("data-theme");
                        },
                    },
                    {
                        key: "triggerWidget",
                        value: function (t) {
                            "enter" === t.way ? (M.classList.add("has-widget-user-open"), M.classList.add("-footer")) : (M.classList.remove("has-widget-user-open"), M.classList.remove("-footer"));
                        },
                    },
                    {
                        key: "triggerTheme",
                        value: function (t) {
                            "enter" === t.way ? (M.classList.add("has-footer-widget-open"), this.call("expand", null, "Background")) : (M.classList.remove("has-footer-widget-open"), this.call("shrink", null, "Background"));
                        },
                    },
                ]),
                i
            );
        })(c),
        oe = (function (t) {
            g(i, t);
            var e = k(i);
            function i(t) {
                return u(this, i), e.call(this, t);
            }
            return (
                f(i, [
                    {
                        key: "init",
                        value: function () {
                            new SplitText(this.el, { type: null !== this.getData("type") ? this.getData("type") : "lines" });
                            for (var t = this.el.querySelectorAll("div"), e = 0; e < t.length; e++) {
                                var i = t[e],
                                    n = i.innerHTML;
                                (i.innerHTML = "<div></div>"), (i.querySelector("div").innerHTML = n);
                            }
                        },
                    },
                ]),
                i
            );
        })(c),
        re = (function (t) {
            g(i, t);
            var e = k(i);
            function i(t) {
                return u(this, i), e.call(this, t);
            }
            return (
                f(i, [
                    { key: "init", value: function () {} },
                    {
                        key: "trigger",
                        value: function (t) {
                            "enter" === t.way ? M.setAttribute("data-ui", t.obj.el.getAttribute("data-value")) : M.setAttribute("data-ui", "");
                        },
                    },
                ]),
                i
            );
        })(c),
        ae = (function (t) {
            g(i, t);
            var e = k(i);
            function i(t) {
                var n;
                return u(this, i), ((n = e.call(this, t)).events = { click: "trigger" }), n;
            }
            return (
                f(i, [
                    { key: "init", value: function () {} },
                    {
                        key: "trigger",
                        value: function (t) {
                            this.call("open", { provider: this.el.getAttribute("data-provider"), id: this.el.getAttribute("data-id"), iframe: this.el.getAttribute("data-iframe") }, "Popup");
                        },
                    },
                ]),
                i
            );
        })(c),
        le = (function (t) {
            g(i, t);
            var e = k(i);
            function i(t) {
                var n;
                return u(this, i), ((n = e.call(this, t)).events = { click: { closeButton: "close" } }), n;
            }
            return (
                f(i, [
                    {
                        key: "init",
                        value: function () {
                            this.$inner = this.$("inner")[0];
                        },
                    },
                    {
                        key: "open",
                        value: function (t) {
                            void 0 !== this.timeout && clearTimeout(this.timeout),
                                null !== t.iframe
                                    ? (this.$inner.innerHTML = t.iframe)
                                    : "vimeo" === t.provider
                                    ? (this.$inner.innerHTML = '<iframe src="https://player.vimeo.com/video/'.concat(
                                          t.id,
                                          '?controls=true&amp;autoplay=1&amp;transparent=false&amp;autopause=false&amp;muted=0"frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen="" allow="autoplay; encrypted-media"></iframe>'
                                      ))
                                    : (t.provider = "youtube") &&
                                      (this.$inner.innerHTML = '<iframe width="560" height="315" src="https://www.youtube.com/embed/'.concat(
                                          t.id,
                                          '?autoplay=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'
                                      )),
                                M.classList.add("has-popup-open");
                        },
                    },
                    {
                        key: "close",
                        value: function () {
                            var t = this;
                            M.classList.add("has-popup-closing"),
                                M.classList.remove("has-popup-open"),
                                (this.timeout = setTimeout(function () {
                                    t.$inner.innerHTML = "";
                                }, 600)),
                                setTimeout(function () {
                                    M.classList.remove("has-popup-closing");
                                }, 600);
                        },
                    },
                ]),
                i
            );
        })(c);
    function ce(t, e) {
        (t.style.webkitTransform = e), (t.style.msTransform = e), (t.style.transform = e);
    }
    var he = (function (t) {
            g(i, t);
            var e = k(i);
            function i(t) {
                var n;
                return u(this, i), ((n = e.call(this, t)).events = { click: { closeButton: "close" } }), n;
            }
            return (
                f(i, [
                    {
                        key: "init",
                        value: function () {
                            (this.bindMousemove = this.mousemove.bind(this)),
                                window.addEventListener("mousemove", this.bindMousemove),
                                (this.bindResize = this.resize.bind(this)),
                                window.addEventListener("resize", this.bindResize),
                                this.resize(),
                                (this.$items = this.$("item")),
                                (this.data = new Array()),
                                (this.mouseY = 0),
                                (this.lerpedMouseY = 0);
                            for (var t = 0; t < this.$items.length; t++) {
                                var e = this.$items[t];
                                this.data.push({ el: e, offset: (e.getBoundingClientRect().top + e.getBoundingClientRect().height / 2) / this.windowHeight });
                            }
                        },
                    },
                    {
                        key: "mousemove",
                        value: function (t) {
                            this.mouseY = t.clientY / this.windowHeight;
                        },
                    },
                    {
                        key: "animate",
                        value: function () {
                            var t = this;
                            (this.raf = requestAnimationFrame(function () {
                                return t.animate();
                            })),
                                (this.lerpedMouseY = Ft(this.lerpedMouseY, this.mouseY, 0.1));
                            for (var e = 0; e < this.data.length; e++) {
                                var i = this.data[e].offset,
                                    n = Math.max(0.7, 1 - Math.abs(this.lerpedMouseY - i)),
                                    s = "scale(".concat(n, ")");
                                ce(this.data[e].el, s);
                            }
                        },
                    },
                    {
                        key: "resize",
                        value: function () {
                            if (((this.windowHeight = window.innerHeight), void 0 !== this.data))
                                for (var t = 0; t < this.data.length; t++) {
                                    var e = this.data[t].el;
                                    this.data[t].offset = (e.getBoundingClientRect().top + e.getBoundingClientRect().height / 2) / this.windowHeight;
                                }
                        },
                    },
                    {
                        key: "open",
                        value: function () {
                            M.classList.add("has-menu-lang-open"),
                                void 0 !== this.timeout && clearTimeout(this.timeout),
                                (this.timeout = setTimeout(function () {
                                    M.classList.remove("has-nav-open");
                                }, 800)),
                                window.isMobile || this.animate();
                        },
                    },
                    {
                        key: "close",
                        value: function () {
                            var t = this;
                            M.classList.add("has-menu-lang-open-is-closing"),
                                void 0 !== this.timeout && clearTimeout(this.timeout),
                                (this.timeout = setTimeout(function () {
                                    M.classList.remove("has-menu-lang-open"), M.classList.remove("has-menu-lang-open-is-closing"), window.isMobile || cancelAnimationFrame(t.raf);
                                }, 600));
                        },
                    },
                    {
                        key: "destroy",
                        value: function () {
                            window.isMobile && (cancelAnimationFrame(this.raf), window.removeEventListener("mousemove", this.bindMousemove), window.removeEventListener("resize", this.bindMousemove));
                        },
                    },
                ]),
                i
            );
        })(c),
        ue = (function (t) {
            g(i, t);
            var e = k(i);
            function i(t) {
                var n;
                return u(this, i), ((n = e.call(this, t)).events = { click: "trigger" }), n;
            }
            return (
                f(i, [
                    { key: "init", value: function () {} },
                    {
                        key: "trigger",
                        value: function () {
                            M.classList.contains("has-menu-lang-open") ? this.call("close", "MenuLang") : this.call("open", "MenuLang");
                        },
                    },
                ]),
                i
            );
        })(c),
        de = (function (t) {
            g(i, t);
            var e = k(i);
            function i(t) {
                var n;
                return u(this, i), ((n = e.call(this, t)).events = { input: { select: "change" } }), n;
            }
            return (
                f(i, [
                    {
                        key: "init",
                        value: function () {
                            (this.bindResize = this.resize.bind(this)), window.addEventListener("resize", this.bindResize), (this.$contents = this.$("content")), this.resize();
                        },
                    },
                    {
                        key: "change",
                        value: function (t) {
                            for (var e = 0; e < this.$contents.length; e++) {
                                var i = this.$contents[e];
                                i.classList.contains("is-active") && i.classList.remove("is-active"), i.getAttribute("id") === t.curTarget.value && i.classList.add("is-active");
                            }
                        },
                    },
                    {
                        key: "resize",
                        value: function () {
                            for (var t = 0, e = 0; e < this.$contents.length; e++) {
                                var i = this.$contents[e];
                                i.getBoundingClientRect().height > t && (t = i.getBoundingClientRect().height);
                            }
                            this.$("wrapper")[0].style.height = "".concat(t, "px");
                        },
                    },
                    {
                        key: "destroy",
                        value: function () {
                            window.removeEventListener("resize", this.bindMousemove);
                        },
                    },
                ]),
                i
            );
        })(c),
        fe = (function (t) {
            g(i, t);
            var e = k(i);
            function i(t) {
                return u(this, i), e.call(this, t);
            }
            return (
                f(i, [
                    {
                        key: "init",
                        value: function () {
                            window.gtag && gtag("event", "test_started", { event_category: "test_auto-evaluation" });
                            var t = JSON.parse(this.el.getAttribute("data-urls")),
                                e = this.getData("back-url");
                            M.setAttribute("data-survey-valid", "false"), (this.apiURL = t.questions), (this.resultsURL = t.results);
                            var i = this;
                            this.app = new Vue({
                                el: this.el,
                                delimiters: ["${", "}"],
                                data: { currentIndex: 0, questions: [], currentQuestion: !1 },
                                created: function () {
                                    this.fetchData();
                                },
                                updated: function () {
                                    this.$nextTick(function () {
                                        i.call("update", null, "Scroll");
                                    });
                                },
                                filters: {
                                    pad: function (t) {
                                        return i.pad(t, 2);
                                    },
                                },
                                methods: {
                                    fetchData: function () {
                                        var t = this,
                                            e = new XMLHttpRequest();
                                        e.open("GET", i.apiURL),
                                            (e.onload = function () {
                                                (t.questions = JSON.parse(e.responseText)), t.loaded();
                                            }),
                                            e.send();
                                    },
                                    loaded: function () {
                                        this.setCurrentQuestion();
                                    },
                                    next: function (t) {
                                        var e = t.currentTarget;
                                        (this.currentQuestion.answer = "true" === e.getAttribute("data-value")),
                                            this.currentIndex + 1 < this.questions.length
                                                ? (this.currentIndex++, this.goto())
                                                : (sessionStorage.setItem("autoEvaluationResults", JSON.stringify(this.questions)), M.setAttribute("data-survey-valid", "true"), i.call("goto", i.resultsURL, "Load"));
                                    },
                                    prev: function () {
                                        0 != this.currentIndex ? (this.currentIndex--, this.goto()) : i.call("confirmTransition", e, "Load");
                                    },
                                    goto: function () {
                                        var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : this.currentIndex;
                                        this.setCurrentQuestion(t),
                                            requestAnimationFrame(function () {
                                                i.call("scrollTo", "#main", "Scroll");
                                            });
                                    },
                                    setCurrentQuestion: function () {
                                        var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : this.currentIndex;
                                        window.gtag && gtag("event", "steps", { event_category: "test_auto-evaluation", event_label: "current_step", value: this.currentIndex }), (this.currentQuestion = this.questions[t]);
                                    },
                                },
                            });
                        },
                    },
                    {
                        key: "updateScroll",
                        value: function () {
                            var t = this;
                            this.call("update", null, "Scroll"),
                                requestAnimationFrame(function () {
                                    t.call("scrollTo", "#main", "Scroll");
                                });
                        },
                    },
                    {
                        key: "pad",
                        value: function (t, e, i) {
                            return (i = i || "0"), (t += "").length >= e ? t : new Array(e - t.length + 1).join(i) + t;
                        },
                    },
                    {
                        key: "hideFixedUI",
                        value: function (t) {
                            "enter" == t.way ? this.app.$el.classList.add("-hide-fixed-ui") : this.app.$el.classList.remove("-hide-fixed-ui");
                        },
                    },
                    {
                        key: "destroy",
                        value: function () {
                            this.app && this.app.$destroy();
                        },
                    },
                ]),
                i
            );
        })(c),
        pe = function () {
            return { results: [], hasViolence: !1 };
        },
        ve = {
            delimiters: ["${", "}"],
            methods: {
                fetchData: function () {
                    var t,
                        e = sessionStorage.getItem("autoEvaluationResults");
                    (null !== (t = e) && void 0 !== t) || (e = !1), e.length ? (this.results = JSON.parse(e)) : (this.results = []);
                },
            },
        },
        me = (function (t) {
            g(i, t);
            var e = k(i);
            function i(t) {
                return u(this, i), e.call(this, t);
            }
            return (
                f(i, [
                    {
                        key: "init",
                        value: function () {
                            var t = this;
                            this.app = new Vue(
                                m(
                                    m({}, ve),
                                    {},
                                    {
                                        el: this.el,
                                        mixins: [{ data: pe }],
                                        data: { hasRedFlag: !1, hasViolence: !1, violentResults: [], violenceTypes: [] },
                                        created: function () {
                                            this.results && (this.fetchData(), this.loaded(), window.gtag && gtag("event", "test_completed", { event_category: "test_auto-evaluation" }));
                                        },
                                        updated: function () {},
                                        filters: {
                                            pad: function (e) {
                                                return t.pad(e, 2);
                                            },
                                        },
                                        methods: m(
                                            m({}, ve.methods),
                                            {},
                                            {
                                                loaded: function () {
                                                    for (
                                                        var t = this,
                                                            e = 1,
                                                            i = function (i) {
                                                                var n = t.results[i];
                                                                return n.answer
                                                                    ? (t.violentResults.push({ index: e++, item: n }),
                                                                      !t.hasRedFlag && n.isRedFlag && (t.hasRedFlag = !0),
                                                                      void 0 === n.violenceType
                                                                          ? "continue"
                                                                          : ((t.hasViolence = !0),
                                                                            void (
                                                                                0 ===
                                                                                    t.violenceTypes.filter(function (t) {
                                                                                        return JSON.stringify(t) === JSON.stringify(n.violenceType);
                                                                                    }).length && t.violenceTypes.push(n.violenceType)
                                                                            )))
                                                                    : "continue";
                                                            },
                                                            n = 0;
                                                        n < this.results.length;
                                                        n++
                                                    )
                                                        i(n);
                                                },
                                                toggleWidget: function () {
                                                    t.call("toggleWidget", "WidgetUser");
                                                },
                                            }
                                        ),
                                    }
                                )
                            );
                        },
                    },
                    {
                        key: "pad",
                        value: function (t, e, i) {
                            return (i = i || "0"), (t += "").length >= e ? t : new Array(e - t.length + 1).join(i) + t;
                        },
                    },
                    {
                        key: "destroy",
                        value: function () {
                            this.app && this.app.$destroy();
                        },
                    },
                ]),
                i
            );
        })(c),
        ge = (function (t) {
            g(i, t);
            var e = k(i);
            function i(t) {
                return u(this, i), e.call(this, t);
            }
            return (
                f(i, [
                    {
                        key: "init",
                        value: function () {
                            this.action = this.el.getAttribute("data-send-results");
                            this.app = new Vue(
                                m(
                                    m({}, ve),
                                    {},
                                    {
                                        el: this.el,
                                        mixins: [{ data: pe }],
                                        data: { action: this.action, email: "", emailResults: [] },
                                        created: function () {
                                            this.fetchData();
                                            var t = this.results
                                                .filter(function (t) {
                                                    return t.answer;
                                                })
                                                .map(function (t) {
                                                    return t.id;
                                                });
                                            (this.hasViolence = 0 !== t.length), (this.emailResults = JSON.stringify(t));
                                        },
                                        methods: m(
                                            m({}, ve.methods),
                                            {},
                                            {
                                                submit: function () {
                                                    var t = this,
                                                        e = { email: this.email, data: this.emailResults },
                                                        i = !1;
                                                    fetch(this.action, { method: "POST", headers: { "Content-Type": "application/json" }, body: JSON.stringify(e) })
                                                        .then(function (t) {
                                                            return 200 !== t.status && 202 !== t.status && (i = !0), t.json();
                                                        })
                                                        .then(function (e) {
                                                            i ? (t.hasErrors = !0) : (window.gtag && gtag("event", "download_test", { event_category: "results" }), t.$refs.divEmail.classList.add("has-feedback"));
                                                        });
                                                },
                                            }
                                        ),
                                    }
                                )
                            );
                        },
                    },
                    {
                        key: "destroy",
                        value: function () {
                            this.app && this.app.$destroy();
                        },
                    },
                ]),
                i
            );
        })(me),
        ye = (function (t) {
            g(i, t);
            var e = k(i);
            function i(t) {
                var n;
                return u(this, i), ((n = e.call(this, t)).events = { click: { closeButton: "close" } }), n;
            }
            return (
                f(i, [
                    { key: "init", value: function () {} },
                    {
                        key: "open",
                        value: function (t) {
                            void 0 !== this.timeout && clearTimeout(this.timeout), M.classList.add("has-history-open");
                        },
                    },
                    {
                        key: "close",
                        value: function () {
                            M.classList.add("has-history-closing"),
                                M.classList.remove("has-history-open"),
                                setTimeout(function () {
                                    M.classList.remove("has-history-closing");
                                }, 600);
                        },
                    },
                ]),
                i
            );
        })(c),
        we = (function (t) {
            g(i, t);
            var e = k(i);
            function i(t) {
                var n;
                return u(this, i), ((n = e.call(this, t)).events = { click: "toggle" }), n;
            }
            return (
                f(i, [
                    { key: "init", value: function () {} },
                    {
                        key: "toggle",
                        value: function () {
                            M.classList.contains("has-history-open") ? this.close() : this.open();
                        },
                    },
                    {
                        key: "open",
                        value: function () {
                            this.call("open", "History");
                        },
                    },
                    {
                        key: "close",
                        value: function () {
                            this.call("close", "History");
                        },
                    },
                ]),
                i
            );
        })(c),
        be = (function (t) {
            g(i, t);
            var e = k(i);
            function i(t) {
                var n;
                return u(this, i), ((n = e.call(this, t)).events = { click: { button: "handleButton", back: "back" } }), n;
            }
            return (
                f(i, [
                    {
                        key: "init",
                        value: function () {
                            var t = this;
                            (this.steps = this.$("step")),
                                (this.titles = this.$("title")),
                                setTimeout(function () {
                                    t.goto(0);
                                }, 600);
                        },
                    },
                    {
                        key: "handleButton",
                        value: function (t) {
                            this.goto(t.curTarget.getAttribute("data-href"), t.curTarget.getAttribute("data-title"));
                        },
                    },
                    {
                        key: "back",
                        value: function () {
                            this.goto(0);
                        },
                    },
                    {
                        key: "goto",
                        value: function (t) {
                            var e = this;
                            t > 0 ? this.el.classList.add("has-began") : this.el.classList.remove("has-began"),
                                this.steps.forEach(function (i, n) {
                                    i.classList.contains("is-active") &&
                                        (i.classList.add("is-disappears"),
                                        e.titles[n].classList.add("is-disappears"),
                                        setTimeout(function () {
                                            i.classList.remove("is-disappears"), i.classList.remove("is-active"), e.titles[n].classList.remove("is-disappears"), e.titles[n].classList.remove("is-active");
                                        }, 800)),
                                        i.getAttribute("data-id") == t && (i.classList.add("is-active"), e.titles[n].classList.add("is-active"));
                                });
                        },
                    },
                ]),
                i
            );
        })(c),
        ke = (function (t) {
            g(i, t);
            var e = k(i);
            function i(t) {
                var n;
                return u(this, i), ((n = e.call(this, t)).events = { change: { select: "changeLanguage" } }), n;
            }
            return (
                f(i, [
                    {
                        key: "changeLanguage",
                        value: function () {
                            window.location.href = this.$("select")[0]
                                .querySelector('option[value="'.concat(this.$("select")[0].value, '"]'))
                                .getAttribute("data-langswitcher-url");
                        },
                    },
                ]),
                i
            );
        })(c),
        xe = 0.005,
        Ee = 0.01,
        Te = (function () {
            function t(e) {
                if ((u(this, t), (this.ctx = e.ctx), (this.x = e.x), (this.y = e.y), (this.r = e.r), (this.randomPosition = !!e.randomPosition), this.randomPosition)) {
                    var i = this.getRandomPosition(),
                        n = i.x,
                        s = i.y;
                    (this.x = n), (this.y = s);
                }
                this.stops = JSON.parse(JSON.stringify(e.stops));
                var o,
                    r = A(this.stops);
                try {
                    for (r.s(); !(o = r.n()).done; ) {
                        var a = o.value;
                        a.originalColor = JSON.parse(JSON.stringify(a.color));
                    }
                } catch (t) {
                    r.e(t);
                } finally {
                    r.f();
                }
                (this.scale = 1),
                    (this.expandScale = 1),
                    (this.isNeutralColors = !1),
                    (this.lerpFactors = { position: e.lerpPosition ? e.lerpPosition : xe, scale: e.lerpScale ? e.lerpScale : Ee }),
                    (this.drawPosition = { x: parseInt(this.x), y: parseInt(this.y), scale: parseFloat(this.scale) });
            }
            return (
                f(t, [
                    {
                        key: "getRandomPosition",
                        value: function () {
                            return { x: 0.2 * this.ctx.width + Math.random() * (this.ctx.width - 0.2 * this.ctx.width * 2), y: 0.2 * this.ctx.height + Math.random() * (this.ctx.height - 0.2 * this.ctx.height * 2) };
                        },
                    },
                    {
                        key: "animate",
                        value: function () {
                            var t = this;
                            (this.animating = !0),
                                (this.tw = gsap.to(
                                    this,
                                    m(
                                        m({}, this.getRandomPosition()),
                                        {},
                                        {
                                            scale: 0.1 * Math.random() + 0.95,
                                            duration: 6,
                                            ease: "linear",
                                            onComplete: function () {
                                                t.animate();
                                            },
                                        }
                                    )
                                ));
                        },
                    },
                    {
                        key: "setPosition",
                        value: function (t, e) {
                            (this.animating = !0), (this.x = t), (this.y = e);
                        },
                    },
                    {
                        key: "setGUI",
                        value: function (t) {
                            var e = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : "Gradient";
                            this.debug = !0;
                            var i = t.addFolder(e);
                            i.add(this, "debug"), i.add(this, "x", 0, this.ctx.width), i.add(this, "y", 0, this.ctx.width), i.add(this, "r", 0, this.ctx.width);
                            for (var n = 0; n < this.stops.length; n++) {
                                var s = i.addFolder("ColorStop " + n);
                                s.add(this.stops[n], "progress", 0, 1, 0.01), s.addColor(this.stops[n], "color"), s.add(this.stops[n], "alpha", 0, 1, 0.01);
                            }
                        },
                    },
                    {
                        key: "draw",
                        value: function () {
                            try {
                                var t = this.x,
                                    e = this.y,
                                    i = this.scale;
                                this.animating &&
                                    ((this.drawPosition.x = Ft(this.drawPosition.x, this.x, this.lerpFactors.position)),
                                    (this.drawPosition.y = Ft(this.drawPosition.y, this.y, this.lerpFactors.position)),
                                    (this.drawPosition.scale = Ft(this.drawPosition.scale, this.scale, this.lerpFactors.scale)),
                                    (t = this.drawPosition.x),
                                    (e = this.drawPosition.y),
                                    (i = this.drawPosition.scale));
                                var n,
                                    s = this.ctx.createRadialGradient(t, e, 0, t, e, this.r * i * this.expandScale),
                                    o = A(this.stops);
                                try {
                                    for (o.s(); !(n = o.n()).done; ) {
                                        var r = n.value;
                                        s.addColorStop(r.progress, "rgba(".concat(r.color.r, ", ").concat(r.color.g, ", ").concat(r.color.b, ", ").concat(r.alpha, ")"));
                                    }
                                } catch (t) {
                                    o.e(t);
                                } finally {
                                    o.f();
                                }
                                (this.ctx.fillStyle = s),
                                    this.ctx.fillRect(0, 0, this.ctx.width, this.ctx.height),
                                    this.debug &&
                                        ((this.ctx.fillStyle = "red"),
                                        this.ctx.fillRect(this.x - 1, this.y - 1, 2, 2),
                                        (this.ctx.strokeStyle = "red"),
                                        this.ctx.beginPath(),
                                        this.ctx.arc(this.x, this.y, this.r * this.scale * this.expandScale, 0, 2 * Math.PI),
                                        this.ctx.stroke());
                            } catch (t) {
                                console.error(t);
                            }
                        },
                    },
                    {
                        key: "toggleDefaultColors",
                        value: function (t) {
                            (this.isDefaultColors = t || !this.isDefaultColors), this.colorsTw && this.colorsTw.kill && this.colorsTw.kill(), (this.colorsTw = gsap.timeline({}));
                            var e,
                                i = A(this.stops);
                            try {
                                for (i.s(); !(e = i.n()).done; ) {
                                    var n = e.value,
                                        s = this.isDefaultColors ? n.defaultColor : n.originalColor;
                                    this.colorsTw.to(n.color, { r: s.r, g: s.g, b: s.b, duration: 1, ease: "power2.Out" }, 0);
                                }
                            } catch (t) {
                                i.e(t);
                            } finally {
                                i.f();
                            }
                        },
                    },
                    {
                        key: "expand",
                        value: function () {
                            this.expandTw && this.expandTw.kill && this.expandTw.kill(), (this.expandTw = gsap.to(this, { expandScale: 3, duration: 2, ease: "power2.InOut" })), this.toggleDefaultColors(!0);
                        },
                    },
                    {
                        key: "shrink",
                        value: function () {
                            this.expandTw && this.expandTw.kill && this.expandTw.kill(), (this.expandTw = gsap.to(this, { expandScale: 1, duration: 2, ease: "power2.InOut" })), this.toggleDefaultColors(!1);
                        },
                    },
                    {
                        key: "destroy",
                        value: function () {
                            this.expandTw && this.expandTw.kill && this.expandTw.kill(), this.tw && this.tw.kill && this.tw.kill();
                        },
                    },
                ]),
                t
            );
        })(),
        Se = (function () {
            function t(e) {
                u(this, t);
                (this.aliasedLimit = e.aliasedLimit), (this.log = e.log), (this.canvas = document.createElement("canvas")), (this.canvas.width = 1), (this.canvas.height = 1), document.body.appendChild(this.canvas);
                var i = this.canvas.getContext("webgl", { stencil: !0 });
                i || (i = this.canvas.getContext("experimental-webgl", { stencil: !0 }));
                var n = !!i;
                (this.gl = i),
                    (this.report = {
                        contextName: n,
                        glVersion: i.getParameter(i.VERSION),
                        shadingLanguageVersion: i.getParameter(i.SHADING_LANGUAGE_VERSION),
                        vendor: i.getParameter(i.VENDOR),
                        renderer: i.getParameter(i.RENDERER),
                        unMaskedVendor: this.getUnmaskedInfo(i).vendor,
                        unMaskedRenderer: this.getUnmaskedInfo(i).renderer,
                        antialias: i.getContextAttributes().antialias ? "Available" : "Not available",
                        angle: this.getAngle(i),
                        majorPerformanceCaveat: this.getMajorPerformanceCaveat(n),
                        maxColorBuffers: this.getMaxColorBuffers(i),
                        redBits: i.getParameter(i.RED_BITS),
                        greenBits: i.getParameter(i.GREEN_BITS),
                        blueBits: i.getParameter(i.BLUE_BITS),
                        alphaBits: i.getParameter(i.ALPHA_BITS),
                        depthBits: i.getParameter(i.DEPTH_BITS),
                        stencilBits: i.getParameter(i.STENCIL_BITS),
                        maxRenderBufferSize: i.getParameter(i.MAX_RENDERBUFFER_SIZE),
                        maxCombinedTextureImageUnits: i.getParameter(i.MAX_COMBINED_TEXTURE_IMAGE_UNITS),
                        maxCubeMapTextureSize: i.getParameter(i.MAX_CUBE_MAP_TEXTURE_SIZE),
                        maxFragmentUniformVectors: i.getParameter(i.MAX_FRAGMENT_UNIFORM_VECTORS),
                        maxTextureImageUnits: i.getParameter(i.MAX_TEXTURE_IMAGE_UNITS),
                        maxTextureSize: i.getParameter(i.MAX_TEXTURE_SIZE),
                        maxVaryingVectors: i.getParameter(i.MAX_VARYING_VECTORS),
                        maxVertexAttributes: i.getParameter(i.MAX_VERTEX_ATTRIBS),
                        maxVertexTextureImageUnits: i.getParameter(i.MAX_VERTEX_TEXTURE_IMAGE_UNITS),
                        maxVertexUniformVectors: i.getParameter(i.MAX_VERTEX_UNIFORM_VECTORS),
                        aliasedLineWidthRange: i.getParameter(i.ALIASED_LINE_WIDTH_RANGE),
                        aliasedPointSizeRange: i.getParameter(i.ALIASED_POINT_SIZE_RANGE),
                        maxViewportDimensions: i.getParameter(i.MAX_VIEWPORT_DIMS),
                        maxAnisotropy: this.getMaxAnisotropy(),
                        vertexShaderBestPrecision: this.getBestFloatPrecision(i.VERTEX_SHADER),
                        fragmentShaderBestPrecision: this.getBestFloatPrecision(i.FRAGMENT_SHADER),
                        fragmentShaderFloatIntPrecision: this.getFloatIntPrecision(i),
                        extensions: i.getSupportedExtensions(),
                        draftExtensionsInstructions: this.getDraftExtensionsInstructions(),
                    }),
                    (this.report.platform = navigator.platform),
                    (this.report.userAgent = navigator.userAgent),
                    (this.report.webglVersion = 1),
                    this.canvas.parentNode.removeChild(this.canvas),
                    (this.canvas = null);
                var s = !0;
                this.report.aliasedPointSizeRange[1] > this.aliasedLimit || (s = !1), (window.hasGoodGPU = s), this.log && this.logReport();
            }
            return (
                f(t, [
                    {
                        key: "logReport",
                        value: function () {
                            console.log("%cGPU report", "background-color:#ff0000; padding:0px 10px; color:#fff;"),
                                console.log(this.report),
                                window.hasGoodGPU ? console.log("%cGPU status: GOOD ðŸ¤˜", "color:#fff;background-color: #64c845") : console.log("%cGPU status: BAD ðŸ˜…", "color:#fff;background-color: #ff0000");
                        },
                    },
                    {
                        key: "describeRange",
                        value: function (t) {
                            return "[" + t[0] + ", " + t[1] + "]";
                        },
                    },
                    {
                        key: "getMaxAnisotropy",
                        value: function () {
                            var t = this.gl.getExtension("EXT_texture_filter_anisotropic") || this.gl.getExtension("WEBKIT_EXT_texture_filter_anisotropic") || this.gl.getExtension("MOZ_EXT_texture_filter_anisotropic");
                            if (t) {
                                var e = this.gl.getParameter(t.MAX_TEXTURE_MAX_ANISOTROPY_EXT);
                                return 0 === e && (e = 2), e;
                            }
                            return "n/a";
                        },
                    },
                    {
                        key: "formatPower",
                        value: function (t, e) {
                            return e ? "" + Math.pow(2, t) : "2<sup>" + t + "</sup>";
                        },
                    },
                    {
                        key: "getPrecisionDescription",
                        value: function (t, e) {
                            var i = e ? " bit mantissa" : "";
                            return "[-" + this.formatPower(t.rangeMin, e) + ", " + this.formatPower(t.rangeMax, e) + "] (" + t.precision + i + ")";
                        },
                    },
                    {
                        key: "getBestFloatPrecision",
                        value: function (t) {
                            var e = this.gl.getShaderPrecisionFormat(t, this.gl.HIGH_FLOAT),
                                i = this.gl.getShaderPrecisionFormat(t, this.gl.MEDIUM_FLOAT),
                                n = this.gl.getShaderPrecisionFormat(t, this.gl.LOW_FLOAT),
                                s = e;
                            return (
                                0 === e.precision && (s = i),
                                "High: " + this.getPrecisionDescription(e, !0) + " | Medium: " + this.getPrecisionDescription(i, !0) + " | Low: " + this.getPrecisionDescription(n, !0) + '">' + this.getPrecisionDescription(s, !1)
                            );
                        },
                    },
                    {
                        key: "getFloatIntPrecision",
                        value: function (t) {
                            var e = this.gl.getShaderPrecisionFormat(this.gl.FRAGMENT_SHADER, this.gl.HIGH_FLOAT),
                                i = 0 !== e.precision ? "highp/" : "mediump/";
                            return (i += 0 !== (e = this.gl.getShaderPrecisionFormat(this.gl.FRAGMENT_SHADER, this.gl.HIGH_INT)).rangeMax ? "highp" : "lowp");
                        },
                    },
                    {
                        key: "isPowerOfTwo",
                        value: function (t) {
                            return 0 !== t && 0 == (t & (t - 1));
                        },
                    },
                    {
                        key: "getAngle",
                        value: function (t) {
                            var e = this.describeRange(this.gl.getParameter(this.gl.ALIASED_LINE_WIDTH_RANGE));
                            return ("Win32" === navigator.platform || "Win64" === navigator.platform) &&
                                "Internet Explorer" !== this.gl.getParameter(this.gl.RENDERER) &&
                                "Microsoft Edge" !== this.gl.getParameter(this.gl.RENDERER) &&
                                e === this.describeRange([1, 1])
                                ? this.isPowerOfTwo(this.gl.getParameter(this.gl.MAX_VERTEX_UNIFORM_VECTORS)) && this.isPowerOfTwo(this.gl.getParameter(this.gl.MAX_FRAGMENT_UNIFORM_VECTORS))
                                    ? "Yes, D3D11"
                                    : "Yes, D3D9"
                                : "No";
                        },
                    },
                    {
                        key: "getMajorPerformanceCaveat",
                        value: function (t) {
                            return (
                                this.canvas.parentNode || document.body.append(this.canavas),
                                this.canvas.getContext(t, { failIfMajorPerformanceCaveat: !0 }) ? (void 0 === this.gl.getContextAttributes().failIfMajorPerformanceCaveat ? "Not implemented" : "No") : "Yes"
                            );
                        },
                    },
                    {
                        key: "getDraftExtensionsInstructions",
                        value: function () {
                            return -1 !== navigator.userAgent.indexOf("Chrome")
                                ? 'To see draft extensions in Chrome, browse to about:flags, enable the "Enable WebGL Draft Extensions" option, and relaunch.'
                                : -1 !== navigator.userAgent.indexOf("Firefox")
                                ? "To see draft extensions in Firefox, browse to about:config and set web.gl.enable-draft-extensions to true."
                                : "";
                        },
                    },
                    {
                        key: "getMaxColorBuffers",
                        value: function (t) {
                            var e = 1,
                                i = this.gl.getExtension("WEBGL_draw_buffers");
                            return null != i && (e = this.gl.getParameter(i.MAX_DRAW_BUFFERS_WEBGL)), e;
                        },
                    },
                    {
                        key: "getUnmaskedInfo",
                        value: function (t) {
                            var e = { renderer: "", vendor: "" },
                                i = this.gl.getExtension("WEBGL_debug_renderer_info");
                            return null != i && ((e.renderer = this.gl.getParameter(i.UNMASKED_RENDERER_WEBGL)), (e.vendor = this.gl.getParameter(i.UNMASKED_VENDOR_WEBGL))), e;
                        },
                    },
                    {
                        key: "showNull",
                        value: function (t) {
                            return null === t ? "n/a" : t;
                        },
                    },
                ]),
                t
            );
        })(),
        Ae = (function () {
            function t(e) {
                u(this, t),
                    (this.limit = e.limit),
                    (this.log = e.log),
                    (this.callback = e.callback),
                    (window.hasGoodCPU = !1),
                    (this.time = 1e4),
                    window.Worker && void 0 !== window.Worker
                        ? ((this.bindOnMessage = this.onMessage.bind(this)),
                          (this.bindOnError = this.onError.bind(this)),
                          (this.worker = new Worker("/assets/scripts/worker.js")),
                          this.worker.addEventListener("message", this.bindOnMessage),
                          this.worker.addEventListener("error", this.bindOnError))
                        : console.warn("CPUTest: Web workers are not available.");
            }
            return (
                f(t, [
                    {
                        key: "run",
                        value: function () {
                            this.worker && this.worker.postMessage({ id: Date.now(), message: { fn: "calculate" } });
                        },
                    },
                    {
                        key: "onError",
                        value: function (t) {
                            console.error("CPUTest :: Error:", t), this.callback && this.callback();
                        },
                    },
                    {
                        key: "onMessage",
                        value: function (t) {
                            t && (this.time = t.data.message.time), (window.hasGoodCPU = this.time < this.limit), this.log && this.logReport(), this.callback && this.callback(this.time), this.destroy();
                        },
                    },
                    {
                        key: "logReport",
                        value: function () {
                            console.log("%cCPU report", "background-color:#ff0000; padding:0px 10px; color:#fff;"),
                                window.hasGoodCPU
                                    ? console.log("%cCPU status: GOOD ðŸ¤˜(".concat(this.time, "ms)"), "color:#fff;background-color: #64c845")
                                    : console.log("%cCPU status: BAD ðŸ˜…(".concat(this.time, "ms)"), "color:#fff;background-color: #ff0000");
                        },
                    },
                    {
                        key: "destroy",
                        value: function () {
                            this.worker.removeEventListener("message", this.bindOnMessage),
                                this.worker.removeEventListener("error", this.bindOnError),
                                this.worker.terminate(),
                                (this.worker = null),
                                (this.callback = null),
                                (this.onMessage = null),
                                (this.onError = null);
                        },
                    },
                ]),
                t
            );
        })(),
        Ce = {
            cyan: [
                { r: 172, g: 252, b: 192 },
                { r: 153, g: 254, b: 255 },
                { r: 255, g: 235, b: 204 },
            ],
            blue: [
                { r: 147, g: 222, b: 255 },
                { r: 194, g: 159, b: 245 },
                { r: 132, g: 150, b: 255 },
            ],
            orange: [
                { r: 252, g: 202, b: 145 },
                { r: 255, g: 161, b: 222 },
                { r: 252, g: 229, b: 241 },
            ],
            green: [
                { r: 254, g: 255, b: 174 },
                { r: 164, g: 245, b: 185 },
                { r: 230, g: 255, b: 238 },
            ],
            rose: [
                { r: 255, g: 166, b: 219 },
                { r: 179, g: 202, b: 252 },
                { r: 246, g: 232, b: 255 },
            ],
        },
        Me = (function (t) {
            g(i, t);
            var e = k(i);
            function i(t) {
                return u(this, i), e.call(this, t);
            }
            return (
                f(i, [
                    {
                        key: "init",
                        value: function () {
                            var t = this;
                            new Se({ aliasedLimit: 250, log: !1 });
                            new Ae({
                                limit: 8,
                                log: !1,
                                callback: function () {
                                    t.ready();
                                },
                            }).run(),
                                (this.scheme = Ce[document.documentElement.getAttribute("data-theme")]),
                                (this.ctx = this.el.getContext("2d"));
                        },
                    },
                    {
                        key: "ready",
                        value: function () {
                            (this.isMobile = window.isMobile),
                                (this.hasGoodGPU = window.hasGoodGPU),
                                (this.hasGoodCPU = window.hasGoodCPU),
                                (this.hasGoodGPU && this.hasGoodCPU) || M.classList.add("has-low-performance"),
                                this.hasGoodCPU && this.hasGoodGPU && M.classList.remove("has-low-performance"),
                                this.compute(),
                                this.isMobile || ((this.checkResizeBind = this.checkResize.bind(this)), window.addEventListener("resize", this.checkResizeBind)),
                                this.isMobile || this.manageMouse(),
                                this.populate();
                        },
                    },
                    {
                        key: "checkResize",
                        value: function () {
                            var t = this;
                            this.resizeTick ||
                                ((this.resizeTick = !0),
                                requestAnimationFrame(function () {
                                    var e = t.width;
                                    t.compute(), e != t.width && t.populate(), (t.resizeTick = !1);
                                }));
                        },
                    },
                    {
                        key: "compute",
                        value: function () {
                            (this.width = window.innerWidth), (this.height = window.innerHeight), (this.el.width = this.width), (this.el.height = this.height), (this.ctx.width = this.width), (this.ctx.height = this.height);
                        },
                    },
                    {
                        key: "manageMouse",
                        value: function () {
                            var t = this;
                            this.isMobile ||
                                ((this.mouseUsed = !1),
                                (this.mouse = { x: 0, y: 0 }),
                                (this.bindMousemove = function (e) {
                                    (t.mouse.x = e.clientX),
                                        (t.mouse.y = e.clientY),
                                        t.mouseUsed ||
                                            ((t.mouseUsed = !0),
                                            (t.mouseGradient = new Te({
                                                ctx: t.ctx,
                                                x: t.mouse.x,
                                                y: t.mouse.y,
                                                r: t.ctx.width / t.ctx.height > 1 ? 0.1 * t.ctx.width : 0.1 * t.ctx.height,
                                                lerpPosition: 0.1,
                                                stops: [
                                                    { progress: 0.15, color: t.scheme[0], defaultColor: Ce.rose[0], alpha: 0.5 },
                                                    { progress: 1, color: t.scheme[0], defaultColor: Ce.rose[0], alpha: 0 },
                                                ],
                                            })));
                                }),
                                window.addEventListener("mousemove", this.bindMousemove));
                        },
                    },
                    {
                        key: "populate",
                        value: function () {
                            if ((void 0 !== this.raf && cancelAnimationFrame(this.raf), this.gradients && this.gradients.length)) {
                                var t,
                                    e = A(this.gradients);
                                try {
                                    for (e.s(); !(t = e.n()).done; ) {
                                        t.value.destroy();
                                    }
                                } catch (t) {
                                    e.e(t);
                                } finally {
                                    e.f();
                                }
                            }
                            this.gradients = [];
                            var i = new Te({
                                ctx: this.ctx,
                                randomPosition: !0,
                                r: this.ctx.width / this.ctx.height > 1 ? 0.4 * this.ctx.width : 0.4 * this.ctx.height,
                                stops: [
                                    { progress: 0, color: this.scheme[1], defaultColor: Ce.rose[1], alpha: 0.66 },
                                    { progress: 0.8, color: this.scheme[0], defaultColor: Ce.rose[0], alpha: 0.66 },
                                    { progress: 1, color: this.scheme[0], defaultColor: Ce.rose[0], alpha: 0 },
                                ],
                            });
                            i.animate();
                            var n = new Te({
                                ctx: this.ctx,
                                randomPosition: !0,
                                r: this.ctx.width / this.ctx.height > 1 ? 0.25 * this.ctx.width : 0.25 * this.ctx.height,
                                stops: [
                                    { progress: 0, color: this.scheme[0], defaultColor: Ce.rose[0], alpha: 0.66 },
                                    { progress: 0.8, color: this.scheme[1], defaultColor: Ce.rose[1], alpha: 0.66 },
                                    { progress: 1, color: this.scheme[1], defaultColor: Ce.rose[1], alpha: 0 },
                                ],
                            });
                            n.animate();
                            var s = new Te({
                                ctx: this.ctx,
                                randomPosition: !0,
                                r: this.ctx.width / this.ctx.height > 1 ? 0.25 * this.ctx.width : 0.25 * this.ctx.height,
                                stops: [
                                    { progress: 0.12, color: this.scheme[2], defaultColor: Ce.rose[2], alpha: 0.66 },
                                    { progress: 1, color: this.scheme[0], defaultColor: Ce.rose[0], alpha: 0 },
                                ],
                            });
                            s.animate(), this.gradients.push(i), this.gradients.push(n), this.gradients.push(s), this.draw();
                        },
                    },
                    {
                        key: "expand",
                        value: function () {
                            var t,
                                e = A(this.gradients);
                            try {
                                for (e.s(); !(t = e.n()).done; ) {
                                    t.value.expand();
                                }
                            } catch (t) {
                                e.e(t);
                            } finally {
                                e.f();
                            }
                        },
                    },
                    {
                        key: "shrink",
                        value: function () {
                            var t,
                                e = A(this.gradients);
                            try {
                                for (e.s(); !(t = e.n()).done; ) {
                                    t.value.shrink();
                                }
                            } catch (t) {
                                e.e(t);
                            } finally {
                                e.f();
                            }
                        },
                    },
                    {
                        key: "draw",
                        value: function () {
                            (this.ctx.fillStyle = "#F9F9F9"), this.ctx.fillRect(0, 0, this.ctx.width, this.ctx.height);
                            var t,
                                e = A(this.gradients);
                            try {
                                for (e.s(); !(t = e.n()).done; ) {
                                    t.value.draw();
                                }
                            } catch (t) {
                                e.e(t);
                            } finally {
                                e.f();
                            }
                            this.mouseUsed && !this.isMobile && (this.mouseGradient.setPosition(this.mouse.x, this.mouse.y), this.mouseGradient.draw()),
                                this.hasGoodCPU && this.hasGoodCPU && (this.raf = requestAnimationFrame(this.draw.bind(this)));
                        },
                    },
                    {
                        key: "destroy",
                        value: function () {
                            cancelAnimationFrame(this.raf), window.removeEventListener("resize", this.onResizeBind), window.removeEventListener("mousemove", this.bindMousemove), this.gui && this.gui.destroy && this.gui.destroy();
                        },
                    },
                ]),
                i
            );
        })(c),
        Le = Object.freeze({
            __proto__: null,
            Load: L,
            Scroll: Ut,
            WidgetUser: Gt,
            WidgetUserButton: Vt,
            Menu: Yt,
            MenuButton: qt,
            MenuTools: Kt,
            MenuToolsButton: Jt,
            Escape: Qt,
            Carousel: Zt,
            CarouselQuotes: te,
            Accordion: ee,
            Rail: ne,
            CtaContact: se,
            Split: oe,
            Ui: re,
            TriggerPopup: ae,
            Popup: le,
            MenuLang: he,
            MenuLangButton: ue,
            Lang: de,
            Test: fe,
            Results: me,
            SendResults: ge,
            History: ye,
            HistoryButton: we,
            QuickChoice: be,
            Langswitcher: ke,
            Background: Me,
        }),
        Oe = "undefined" != typeof globalThis ? globalThis : "undefined" != typeof window ? window : "undefined" != typeof global ? global : "undefined" != typeof self ? self : {};
    var Pe = (function (t, e) {
        return t((e = { exports: {} }), e.exports), e.exports;
    })(function (t) {
        var e, i;
        (e = Oe),
            (i = function () {
                function t(t, e, i) {
                    if (i) {
                        var n = document.createDocumentFragment(),
                            s = !e.hasAttribute("viewBox") && i.getAttribute("viewBox");
                        s && e.setAttribute("viewBox", s);
                        for (var o = i.cloneNode(!0); o.childNodes.length; ) n.appendChild(o.firstChild);
                        t.appendChild(n);
                    }
                }
                function e(e) {
                    (e.onreadystatechange = function () {
                        if (4 === e.readyState) {
                            var i = e._cachedDocument;
                            i || (((i = e._cachedDocument = document.implementation.createHTMLDocument("")).body.innerHTML = e.responseText), (e._cachedTarget = {})),
                                e._embeds.splice(0).map(function (n) {
                                    var s = e._cachedTarget[n.id];
                                    s || (s = e._cachedTarget[n.id] = i.getElementById(n.id)), t(n.parent, n.svg, s);
                                });
                        }
                    }),
                        e.onreadystatechange();
                }
                function i(t) {
                    for (var e = t; "svg" !== e.nodeName.toLowerCase() && (e = e.parentNode); );
                    return e;
                }
                return function (n) {
                    var s,
                        o = Object(n),
                        r = window.top !== window.self;
                    s =
                        "polyfill" in o
                            ? o.polyfill
                            : /\bTrident\/[567]\b|\bMSIE (?:9|10)\.0\b/.test(navigator.userAgent) ||
                              (navigator.userAgent.match(/\bEdge\/12\.(\d+)\b/) || [])[1] < 10547 ||
                              (navigator.userAgent.match(/\bAppleWebKit\/(\d+)\b/) || [])[1] < 537 ||
                              (/\bEdge\/.(\d+)\b/.test(navigator.userAgent) && r);
                    var a = {},
                        l = window.requestAnimationFrame || setTimeout,
                        c = document.getElementsByTagName("use"),
                        h = 0;
                    s &&
                        (function n() {
                            for (var r = 0; r < c.length; ) {
                                var u = c[r],
                                    d = u.parentNode,
                                    f = i(d),
                                    p = u.getAttribute("xlink:href") || u.getAttribute("href");
                                if ((!p && o.attributeName && (p = u.getAttribute(o.attributeName)), f && p)) {
                                    if (s)
                                        if (!o.validate || o.validate(p, f, u)) {
                                            d.removeChild(u);
                                            var v = p.split("#"),
                                                m = v.shift(),
                                                g = v.join("#");
                                            if (m.length) {
                                                var y = a[m];
                                                y || ((y = a[m] = new XMLHttpRequest()).open("GET", m), y.send(), (y._embeds = [])), y._embeds.push({ parent: d, svg: f, id: g }), e(y);
                                            } else t(d, f, document.getElementById(g));
                                        } else ++r, ++h;
                                } else ++r;
                            }
                            (!c.length || c.length - h > 0) && l(n, 67);
                        })();
                };
            }),
            t.exports ? (t.exports = i()) : (e.svg4everybody = i());
    });
    var _e = new h({ modules: Le });
    function Re() {
        !(function () {
            Pe();
            var t = bowser.getParser(window.navigator.userAgent).getBrowser();
            "Internet Explorer" === t.name ? M.classList.add("is-ie") : "Firefox" === t.name ? M.classList.add("is-firefox") : "Safari" === t.name && M.classList.add("is-safari");
        })(),
            _e.init(_e),
            setTimeout(function () {
                M.classList.add("is-loaded"),
                    M.classList.add("is-ready"),
                    M.classList.add("is-first-load"),
                    setTimeout(function () {
                        M.classList.add("is-animated"), M.classList.add("has-app-first-load");
                    }, 1200);
            }, 800);
    }
    window.onload = function (t) {
        var e = document.getElementById("stylesheet"),
            i = /Android|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) || ("MacIntel" === navigator.platform && navigator.maxTouchPoints > 1);
        (window.isMobile = i),
            window.isMobile && M.classList.add("is-mobile"),
            e.isLoaded
                ? Re()
                : e.addEventListener("load", function (t) {
                      Re();
                  });
    };
})();
