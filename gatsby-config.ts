import type { GatsbyConfig } from "gatsby";
import { GATSBY_PATH_PREFIX } from "./static/global-constants.js";

const config: GatsbyConfig = {
    siteMetadata: {
        title: `Jamun Verse: a space odyssey`,
        description: `Dessert Roz: An Ode to South Asian Desserts by 3 Sided Coin. Engage in Jamunverse: Trivia, Games, and Heavenly Delights!`,
        twitterUsername: `@3_sided_coin`,
        image: `/images/jamunverse-twitter.png`,
        siteUrl: `https://dessertroz.com`,
        domain: `dessertroz.com`
    },
    pathPrefix: GATSBY_PATH_PREFIX,
    // More easily incorporate content into your pages through automatic TypeScript type generation and better GraphQL IntelliSense.
    // If you use VSCode you can also use the GraphQL plugin
    // Learn more at: https://gatsby.dev/graphql-typegen
    graphqlTypegen: true,
    plugins: [
        //     {
        //   resolve: 'gatsby-source-sanity',
        //   options: {
        //     "projectId": "ula07i2m",
        //     "dataset": "production"
        //   }
        // },
        "gatsby-plugin-image",
        "gatsby-plugin-sharp",
        "gatsby-transformer-sharp",
        "gatsby-plugin-emotion",
        "gatsby-plugin-sitemap",
        "gatsby-transformer-remark",
        `gatsby-plugin-glslify`,
        {
            resolve: "gatsby-source-filesystem",
            options: {
                name: "images",
                path: "./src/images/"
            },
            __key: "images"
        },
        {
            resolve: "gatsby-source-filesystem",
            options: {
                name: "pages",
                path: "./src/pages/"
            },
            __key: "pages"
        },
        {
            resolve: "gatsby-plugin-manifest",
            options: {
                icon: "src/images/favicon.png"
            }
        },
        `gatsby-plugin-transition-link`
    ]
};

export default config;
